<?php
namespace Application\api\rest\operation\hook\OrgChart\companies;
class Fetch {
    
    public function before(\Application\api\rest\operation\hook\IService $serviceLocator) {
        return true;
    }
    
    public function after(\Application\api\rest\operation\hook\IService $serviceLocator, $result) {

        if ($this->countSeconds($result) > 5) { // <--- min update period
            
            $this->updateDepartments($serviceLocator, $result);
            
        }
        
        return $result;
    }
    
    private function countSeconds(array $company) {
        
        $lastUpdateTime = new \DateTime($company['ldapUpdate']);

        $now = new \DateTime(date('Y-m-d H:i:s', time()));
        
        $interval = $now->diff($lastUpdateTime);

        $seconds = $interval->format('%a')*24*60*60 + $interval->format('%h')*60*60 +  $interval->format('%m')*60 +  $interval->format('%s');
        
        return $seconds;
        
    }
    
    private function updateDepartments(
        \Application\api\rest\operation\hook\IService $serviceLocator, 
        array $company
    ) {

        $distinguishedName = $company['ldapDistinguishedName'];

        $companyTable = $serviceLocator->table();

        foreach($companyTable->getDetailTables() as $detailTable) {
            if ($detailTable->getName() == 'divisions') {
                $departmentTable = $detailTable;
                break;
            }
        }

        foreach ($departmentTable->getDetailTables() as $detailTable) {
            if ($detailTable->getName() == 'persons') {
                $personTable = $detailTable;
                break;
            }
        }

        if (!empty( $distinguishedName)) {

            foreach ($company['divisions'] as $department) {

                $this->walkDepartments(
                    $serviceLocator, 
                    $department, 
                    $distinguishedName,
                    $companyTable,
                    $departmentTable,
                    $personTable
                );

            }

        }

        $serviceLocator->query()->update()->byId(
            $companyTable, 
            $company['id'], 
            $serviceLocator->value()->rowToValues($companyTable, array('ldapUpdate' => date('Y-m-d H:i:s')))
        )->query();
        
    }
    
    private function walkDepartments(
        \Application\api\rest\operation\hook\IService $serviceLocator, 
        array $department, 
        $distinguishedName,
        \Application\api\rest\operation\table\ITable $companyTable,
        \Application\api\rest\operation\table\ITable $departmentTable,
        \Application\api\rest\operation\table\ITable $personTable
    ) {

        $departmentId = $department['id'];
        
        $serviceLocator->query()->delete()->byMasterId($personTable, $departmentId)->query();
        
        $persons = $this->fetchDepartmentPersons($serviceLocator, $department['ldap'], $distinguishedName);

        foreach ($persons as $person) {
            $personId = $serviceLocator->query()->insert()->row($personTable)->query();
            $serviceLocator->query()->insert()->glue($personTable, $departmentId, $personId)->query();
            $serviceLocator->query()->update()->byId(
                $personTable, 
                $personId, 
                $serviceLocator->value()->rowToValues($personTable, $person)
            )->query();
        }
        
        if (isset($department['children'])) {
            
            foreach ($department['children'] as $childDepartment) {
                
                $this->walkDepartments(
                    $serviceLocator, 
                    $childDepartment, 
                    $distinguishedName, 
                    $companyTable, 
                    $departmentTable, 
                    $personTable
                );
                
            }
            
        }

    }

    private function fetchDepartmentPersons(
        \Application\api\rest\operation\hook\IService $serviceLocator, 
        $departmentCode, 
        $distinguishedName
    ) {
        
        $host = "ppdc1.tp-local.ru"; 
        $port = 389;
        $user = 'UX_User@tp-local.ru';
        $password = 'Kjgfnf1389';
        
        $ldap = new Ldap($host, $port, $user, $password);

        $query = '(department='.$departmentCode.')';
        $map = array(
            'extensionAttribute6' => 'name',
            'displayName' => 'displayName',
            'mail' => 'mail',
            'mobile' => 'mobile',
            'extensionAttribute5' => 'phoneExtension'
        );
        $attributes = array_keys($map);
        
        $entries = $ldap->search($distinguishedName, $query, $attributes);
        
        $rows = array();
        foreach ($entries as $index => $entry) {
            if (!is_numeric($index)) {
                continue;
            }
            $row = array();
            foreach ($attributes as $attribute) {
                $entryAttribute = strtolower($attribute);
                if (array_key_exists($entryAttribute, $entry) && array_key_exists(0, $entry[$entryAttribute])) {
                    $row[$map[$attribute]] = iconv('cp1251', 'utf8', $entry[$entryAttribute][0]);
                } else {
                    $row[$map[$attribute]] = null;
                }
            }
            $rows[] = $row;
        }
        
        return $rows;
        
    }

}