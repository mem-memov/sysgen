<?php
namespace Application\api\rest\operation\hook\OrgChart\companies;
class Ldap {
    
    private $host; 
    private $port; 
    private $user; 
    private $password;
    private $connection;
    
    public function __construct($host, $port, $user, $password) {
        
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        $this->connection = null;
        
    }
    
    public function __destruct() {
        
        if (!is_null($this->connection)) {
            ldap_close($this->connection);
        }
        
    }
    
    public function search($distinguishedName, $query, $attributes) {
        
        $connection = $this->connection();

        $searchResult = ldap_search($connection, $distinguishedName, $query, $attributes); 

        if ($searchResult === false) {
            throw new \Exception('Неверный запрос к серверу LDAP '.$this->host);
        }
        
        $entries = ldap_get_entries($connection, $searchResult);

        return $entries;
        
    }
    
    private function connection() {
        
        if (is_null($this->connection)) {
            
            $this->connection = ldap_connect($this->host, $this->port);
            
            if ($this->connection === false) {
                throw new \Exception('Невозможно подключиться к серверу LDAP '.$this->host);
            }
            
            $bindResult = ldap_bind($this->connection, $this->user, $this->password);
            
            if ($bindResult === false) {
                throw new \Exception('Невозможно подключить пользователя  "'.$this->user.'" к серверу LDAP '.$this->host);
            }
            
        }
        
        return $this->connection;
        
    }
    
}