Ext.define('System.view.orgchart.View', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.orgchart',
    requires: [
        'System.view.orgchart.Controller'
    ],
    controller: 'orgchart',

    title: 'Оргструктура',
    
    html: '<div id="orgchart"></div>',
    
    bodyStyle: {
        backgroundColor: '#ddd'
    },
    
    listeners: {
        afterrender: 'afterRender'
    }
    
});