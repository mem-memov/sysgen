Ext.define('System.view.orgchart.menu.View', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.orgchartMenu',
    requires: [
        'System.view.orgchart.menu.Controller'
    ],
    controller: 'orgchartMenu',
    items: [
        {
            text: 'Переименовать',
            menu: [
                {
                    reference: 'categoryField',
                    xtype: 'textfield'
                }
            ]

        },
        {
            text: 'Добавить позицию'
        }
    ],
    listeners: {
        
    }
});