Ext.define('System.view.orgchart.menu.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.orgchartMenu',
    
    config: {
        listen: {
            controller: {
                '*': {
                    requestDivisionMenu: 'onRequestDivisionMenu'
                }
            }
        }
    },
    
    onRequestDivisionMenu: function(x, y, category) {
        console.log(x, y);
        //var categoryField = this.lookupReference('categoryField');
        //categoryField.setValue(category.get('category'));
        
        this.getView().showAt(x, y, false);
        
    }
    
});