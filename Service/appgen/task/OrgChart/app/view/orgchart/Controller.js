Ext.define('System.view.orgchart.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.orgchart',
    config: {
        rendered: false
    },
    afterRender: function () {

        if (this.getRendered()) { // event is fired twice
            return;
        }
        this.setRendered(true);

        getOrgChart.OPEN_SVG = '<svg id="extOrgChart" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" version="1.1" viewBox="[viewBox]"><defs>[defs]</defs><g>';

        Ext.get('orgchart').setWidth(this.getView().getWidth()); // orgchart library modifies this attribute
        Ext.get('orgchart').setHeight(this.getView().getHeight()); // orgchart library modifies this attribute


        $("#orgchart").getOrgChart({
            theme: 'annabel',
            editable: false,
            zoomable: true,
            orientation: getOrgChart.RO_LEFT,
            clickEvent: function (sender, args) {
                return false; // prevent showing the card
            },
            renderBoxContentEvent: function( sender, args ) {
                var gPosition = args.boxContentElements.length - 3;
                var pathPosition = gPosition + 1;
                var gString = args.boxContentElements[gPosition];
                var pathString = args.boxContentElements[pathPosition];
                if (gString) {
                    args.boxContentElements[gPosition] = gString.replace('<g', '<g id="extorgchart-'+args.id+'"');
                }
            }
        });

        Ext.get('orgchart').setWidth('100%'); // orgchart library modifies this attribute
        Ext.get('orgchart').setHeight('100%'); // orgchart library modifies this attribute

        var store = Ext.data.StoreManager.lookup('companies');
        store.getModel().load(1, {
            scope: this,
            params: {
                cluster: true
            },
            success: function(company) {
                $("#orgchart").getOrgChart('createPerson', 0, null, {Name: 'Генеральный директор'});
                company.divisions().each(function(division) {
                    this.walk(division);

                }, this);
                $("#orgchart").getOrgChart('draw');
                company.divisions().each(function(division) {
                    this.addHandlers(division);
                    division.persons().each(function(person) {
                        $("#orgchart").getOrgChart('createPerson', person.get('id')*1000, division.get('id'), {Name: person.get('name')});
                    });
                }, this);
            }
        });
        
/**
        var store = Ext.data.StoreManager.lookup('structure');

        store.setRoot({
            id: 1
        });

        store.load({
            params: {
                cluster: true
            },
            callback: function () {
                var root = store.getRoot();
                this.walk(root);
                $("#orgchart").getOrgChart('draw');
                this.addHandlers(root);
            },
            scope: this
        });
*/
    },

    
    walk: function (node) {

        //if (node.getDepth() > 3) {
        //    return;
        //}

        var id = node.get('id');
        var parentId = node.get('parentId');
        var name = node.get('name');
        
        if (id !== 1) {
            $("#orgchart").getOrgChart('createPerson', id, parentId === 1 ? null : parentId, {Name: name});
            var element = Ext.get('extorgchart-'+id);
            if (element) {
                element.mon(element, 'contextmenu', function(event) {
                    event.stopEvent();
                    this.fireEvent('requestDivisionMenu', event.getX(), event.getY() - this.getView().getY());
                }, this);
            }
        }

        node.eachChild(function (child) {

            this.walk(child);

        }, this);
    },
    
    addHandlers: function(node) {
        
        var id = node.get('id');
        
        if (id !== 1) {
            var element = Ext.get('extorgchart-'+id);
            if (element) {
                //element.mon(element, 'contextmenu', function(event) {}, this);
            }
        }
        
        node.eachChild(function (child) {

            this.walk(child);

        }, this);
        
    }

});