Ext.define('System.view.Start', {
    extend: 'Ext.container.Container',
    alias: 'widget.start',
    requires: [
        'System.view.orgchart.View',
        'System.view.orgchart.menu.View'
    ],
    layout: 'fit',
    items: [
        {
           xtype: 'orgchart'
        },
        {
            xtype: 'orgchartMenu'
        }
    ]
    
});