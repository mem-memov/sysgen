Ext.define('System.view.budget.catalog.View', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.budgetCatalog',
    requires: [
        'System.view.budget.catalog.Controller',
        'System.view.budget.catalog.category.View'
    ],
    controller: 'budgetCatalog',
    
    title: 'Категории',
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    items: [
        {
            reference: 'exchangeRateLabel',
            xtype: 'displayfield',
            value: 'Курс USD:',
            fieldStyle: 'text-align: right; padding-right: 5px;',
            padding: '3 0 0 0',
            style: {
                backgroundColor: '#eee'
            },
        }
    ],
    
    listeners: {
        beforeremove: 'beforeRemove'
    }

});