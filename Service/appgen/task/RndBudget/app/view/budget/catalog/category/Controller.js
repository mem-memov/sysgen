Ext.define('System.view.budget.catalog.category.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetCatalogCategory',
    
    config: {
        category: null
    },
    
    onSelectionChange: function() {
        
        var plan = this.getView().getSelectionModel().getSelection()[0];
        
        this.fireEvent('planSelected', this.getCategory(), plan);
        
    },
    
    onCollapse: function() {
        
        Ext.suspendLayouts();
        this.fireEvent('categoryCollapse', this.getCategory());
        Ext.resumeLayouts(true);
    },
    
    onExpand: function() {
        
        Ext.suspendLayouts();
        this.fireEvent('categoryExpand', this.getCategory());
        Ext.resumeLayouts(true);
        
    },
    
    onHeaderContextMenu: function(x, y) {
        
        var catalog = this.getView().up('budgetCatalog');

        y = y - catalog.getY(); // fixing strange behaviour
        
        this.fireEvent('requestCategoryMenu', x, y, this.getCategory());
        
    }
    
});