Ext.define('System.view.budget.catalog.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetCatalog',

    build: function(budget) {
        
        this.getView().removeAll(true);
        
        budget.categories().each(function(category) {

            var categoryComponent = Ext.widget({
                xtype:'budgetCatalogCategory',
                title: category.get('category'),
                store: category.plans()
            });
            
            categoryComponent.getController().setCategory(category);
            
            this.getView().add(categoryComponent);

        }, this);
        
    },
    
    beforeRemove: function(containerComponent, removedComponent) {

        return !removedComponent.getReference();
        
    }


});