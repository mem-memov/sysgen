Ext.define('System.view.budget.catalog.category.View', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.budgetCatalogCategory',
    requires: [
        'System.view.budget.catalog.category.Controller'
    ],
    controller: 'budgetCatalogCategory',
    
    collapsible: true,
    animCollapse: false,
    collapsed: true,
    
    hideHeaders: true,
    
    selModel: {
        type: 'cellmodel',
        allowDeselect: true
    },

    plugins: [
        {
            pluginId: 'editing',
            ptype: 'cellediting',
            clicksToEdit: 2
        }
    ],

    columns: [
        {
            dataIndex: 'description',
            flex: 1,
            editor: {
                xtype: 'textfield'
            }
        },
        {
            dataIndex: 'budgeting.code',
            width: 70
        }
    ],
    
    listeners: {
        selectionchange: 'onSelectionChange',
        collapse: 'onCollapse',
        expand: 'onExpand',
        headerContextMenu: 'onHeaderContextMenu',
        afterrender: function(grid) {
            grid.mon(grid.getHeader().getEl(), 'contextmenu', function(event) { 
                event.stopEvent();
                grid.fireEvent('headerContextMenu', event.getX(), event.getY()); 
            });
        }
    }

});