Ext.define('System.view.budget.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budget',
    
    config: {
        budget: {},
        isReady: false,
        listen: {
            global: {
                calendarScroll: 'onCalendarScroll'
            }
        }
    },

    init: function() {
        
        var projectId = 1;
        var projectField = this.lookupReference('projectField');

        projectField.setValue(projectId);
        
    },

    onProjectSelected: function(projectField, projectId) {

        var budgetField = this.lookupReference('budgetField');

        budgetField.setFilters([{
                property: 'projects',
                operator: 'eq',
                value: projectId
        }]);
        
    },
    
    onBudgetSelected: function(budgetField, budgetId) {
        
        var budget = Ext.create('System.model.gridEditor.Budgets', {
            id: budgetId
        });
        
        budget.phantom = true; // send id but avoid id checking
        
        this.setIsReady(false);
        
        var startDateField = this.lookupReference('startDateField');
        startDateField.disable();
        
        var finishDateField = this.lookupReference('finishDateField');
        finishDateField.disable();
        
        var currencyButton = this.lookupReference('currencyButton');
        currencyButton.disable();
        
        var timeButton = this.lookupReference('timeButton');
        timeButton.disable();
        
        budget.load({
            params: {
                cluster: true
            },
            success: function(budget) {

                this.setBudget(budget);

                var catalog = this.lookupReference('catalog');
                catalog.getController().build(budget);
                catalog.show();
                
                var calendar = this.lookupReference('calendar');
                calendar.getController().build(budget);

                startDateField.setValue(budget.get('startDate'));
                startDateField.enable();

                finishDateField.setValue(budget.get('finishDate'));
                finishDateField.enable();
                
                currencyButton.enable();
                timeButton.enable();
                
                this.setIsReady(true);

            },
            scope: this
        });
        
    },
    
    onStartDateChange: function(startDateField, startDate) {
        
        if (!this.getIsReady()) {
            return;
        }

        var budget = this.getBudget();
        budget.set('startDate', startDate);
        budget.save();
        
        var calendar = this.lookupReference('calendar');
        calendar.getController().build(budget);
        
    },
    
    onFinishDateChange: function(finishDateField, finishDate) {
        
        if (!this.getIsReady()) {
            return;
        }
        
        var budget = this.getBudget();
        budget.set('finishDate', finishDate);
        budget.save();
        
        var calendar = this.lookupReference('calendar');
        calendar.getController().build(budget);
        
    },
    
    onCurrencyButtonToggle: function() {
        
        var rurButton = this.lookupReference('rurButton');
        var usdButton = this.lookupReference('usdButton');
        
        Ext.suspendLayouts();
        
        if (rurButton.pressed) {
            this.fireEvent('currencyChange', 'rur')
        } else {
            this.fireEvent('currencyChange', 'usd')
        }
        
        Ext.resumeLayouts(true);
        
    },
    
    onCalendarScroll: function(x, y) {
        
        var catalog = this.lookupReference('catalog');
        
        catalog.setLocalY(-y);
        
    }
    
});