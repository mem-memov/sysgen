Ext.define('System.view.budget.View', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.budget',
    requires: [
        'System.view.budget.Controller',
        'System.view.budget.calendar.View',
        'System.view.budget.catalog.View',
        'System.view.budget.project.View',
        'System.view.budget.menu.plan.View',
        'System.view.budget.menu.category.View'
    ],
    controller: 'budget',

    title: 'Бюджет',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'panel',
            //xtype: 'budgetProject',
            height: 1
        },
        {
            xtype: 'container',
            flex: 1,
            scrollable: false,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    reference: 'catalogContainer',
                    xtype: 'container',
                    scrollable: false,
                    width: 400,
                    items: [
                        {
                            reference: 'catalog',
                            xtype: 'budgetCatalog',
                            hidden: true
                        }
                    ]

                },
                {
                    xtype: 'splitter'
                },
                {
                    reference: 'calendarContainer',
                    xtype: 'container',
                    flex: 1,
                    layout: 'vbox',
                    scrollable: {
                        x: true,
                        y: true,
                        listeners: {
                            scroll: function(scroller, x, y) {
                                Ext.GlobalEvents.fireEvent('calendarScroll', x, y); // usuall procedure didn't work in ExtJs 5.1.0
                            }
                        }
                    },
                    items: {
                        reference: 'calendar',
                        xtype: 'budgetCalendar'
                    }
                }
            ]
        },
        {
            xtype: 'budgetMenuCategory'
        },
        {
            xtype: 'budgetMenuPlan'
        }
    ],
    
    tbar: [
        {
            xtype: 'combobox',
            reference: 'projectField',
            store: 'projects',
            displayField: 'name',
            valueField: 'id',
            matchFieldWidth: false,
            queryMode: 'remote',
            editable: false,
            listeners: {
                change: 'onProjectSelected'
            }
        },
        {
            xtype: 'combobox',
            reference: 'budgetField',
            store: 'budgets',
            displayField: 'name',
            valueField: 'id',
            matchFieldWidth: false,
            queryMode: 'remote',
            editable: false,
            listeners: {
                change: 'onBudgetSelected',
                beforequery: function(qe){ // forcing a requery
                    delete qe.combo.lastQuery;
                }
            }
        },
        {
            xtype: 'datefield',
            reference: 'startDateField',
            format: 'd.m.Y',
            width: 120,
            disabled: true,
            listeners: {
                change: 'onStartDateChange'
            }
        },
        {
            xtype: 'datefield',
            reference: 'finishDateField',
            format: 'd.m.Y',
            width: 120,
            disabled: true,
            listeners: {
                change: 'onFinishDateChange'
            }
        },
        {
            xtype: 'segmentedbutton',
            reference: 'currencyButton',
            disabled: true,
            items: [
                {
                    text: 'USD',
                    reference: 'usdButton',
                    pressed: true
                }, {
                    text: 'RUR',
                    reference: 'rurButton'
                }
            ],
            listeners: {
                toggle: 'onCurrencyButtonToggle'
            }
        },
        {
            xtype: 'segmentedbutton',
            reference: 'timeButton',
            allowMultiple: true,
            disabled: true,
            items: [
                {
                    text: 'Месяц',
                    reference: 'monthButton',
                    pressed: true
                }, {
                    text: 'Квартал',
                    reference: 'quarterButton',
                    pressed: true
                }, {
                    text: 'Год',
                    reference: 'yearButton',
                    pressed: true
                }
            ],
            listeners: {
                //toggle: 'onCurrencyButtonToggle'
            }
        }
    ]
    
});