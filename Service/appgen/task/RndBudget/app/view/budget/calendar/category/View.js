Ext.define('System.view.budget.calendar.category.View', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.budgetCalendarCategory',
    requires: [
        'System.view.budget.calendar.category.Controller'
    ],
    controller: 'budgetCalendarCategory',
    
    collapsible: false,
    animCollapse: false,
    collapsed: true,
    
    hideHeaders: true,
    
    selModel: {
        type: 'cellmodel',
        allowDeselect: true
    },

    plugins: [
        {
            pluginId: 'editing',
            ptype: 'cellediting',
            clicksToEdit: 2
        }
    ],
    
    columns: [
        {
            xtype: 'numbercolumn',
            dataIndex: 'usd',
            flex: 1,
            renderer: function(value, mtaData, record, rowIndex, colIndex, store, view) {
                var thisMonth = view.ownerCt.getController().checkMonth(record);
                return (thisMonth ? '$' + value : '');
            },
            editor: {
                xtype: 'numberfield',
                step: 1000,
                minValue: 0
            }
        },
        {
            xtype: 'numbercolumn',
            dataIndex: 'rub',
            hidden: true,
            flex: 1,
            renderer: function(value, mtaData, record, rowIndex, colIndex, store, view) {
                var thisMonth = view.ownerCt.getController().checkMonth(record);
                return (thisMonth ? value + 'р.' : '');
            },
            editor: {
                xtype: 'numberfield',
                step: 1000,
                minValue: 0
            }
        }
    ],

    listeners: {
        beforerender: 'beforeRender',
        afterrender: 'afterRender',
        selectionchange: 'onSelectionChange',
        edit: 'onGridEdit'
    }

});