Ext.define('System.view.budget.calendar.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetCalendar',

    build: function(budget) {
        
        Ext.suspendLayouts();
        
        this.getView().removeAll(true);
        
        var minDate = budget.get('startDate');
        var maxDate = budget.get('finishDate');
        
        minDate = Ext.Date.clearTime(minDate);
        maxDate = Ext.Date.clearTime(maxDate);
        
        var monthCount = Ext.Date.diff(minDate, maxDate, Ext.Date.MONTH);

        var monthComponents = [];
        
        for (var i = 0; i <= monthCount; i++) {
            
            var currentDate = Ext.Date.add(minDate, Ext.Date.MONTH, i);
            var yearMonth = Ext.Date.format(currentDate, 'Y-m');

            var monthComponent = Ext.widget({
                xtype: 'budgetCalendarMonth',
                title: Ext.Date.format(currentDate, 'm Y')//,
                //hidden: i > 5
            });
            
            var categoryComponents = [];
            
            budget.categories().each(function(category) {

                var categoryComponent = Ext.widget({
                    xtype:'budgetCalendarCategory',
                    title: '&nbsp;',
                    store: category.plans(),
                    flex: 1
                });
                
                categoryComponent.getController().setCategory(category);
                categoryComponent.getController().setDate(currentDate);

                categoryComponents.push(categoryComponent);
                

            }, this);
            
            monthComponent.add(categoryComponents);
            
            
            monthComponents.push(monthComponent);

        }
        
        this.getView().add(monthComponents);
        
        Ext.resumeLayouts(true);
        
    }


});