Ext.define('System.view.budget.calendar.category.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetCalendarCategory',

    config: {
        category: null,
        date: null,
        month: null,
        year: null,
        sum: {
            rur: 0,
            usd: 0
        },
        listen: {
            controller: {
                '*': {
                    beforeRender: 'beforeRender',
                    afterRender: 'afterRender',
                    currencyChange: 'onCurrencyChange',
                    categoryCollapse: 'onCategoryCollapse',
                    categoryExpand: 'onCategoryExpand',
                    planSelected: 'onPlanSelected'
                }
            }
        }
    },

    applyDate: function(date) {

        var month = Ext.Date.format(date, 'm');
        var year = Ext.Date.format(date, 'Y');

        this.setMonth(month);
        this.setYear(year);
        
        return date;
        
    },
    
    beforeRender: function() {
        
        this.sum();
        
    },
    
    afterRender: function() {
        
        this.updateTitle('usd');
        
    },
    
    updateTitle: function(currency) {
        
        this.sum();
        
        switch (currency) {
            case 'usd':
                this.getView().setTitle(this.getSum().usd == 0 ? '&nbsp;' : '$' + this.getSum().usd);
                break;
            case 'rur':
                this.getView().setTitle(this.getSum().rur == 0 ? '&nbsp;' : this.getSum().rur + 'р.');
                break;
        }

    },
    
    sum: function() {
        
        var sum = {
            rur: 0,
            usd: 0
        };
        
        var yearMonth = this.getYear() + '-' + this.getMonth();

        this.getCategory().plans().each(function(plan) {
            if (Ext.Date.format(plan.get('date'), 'Y-m') == yearMonth) {
                sum.rur += plan.get('rub');
                sum.usd += plan.get('usd');
            }
        });    

        this.setSum(sum);
        
    },
    
    onCurrencyChange: function(currency) {
        
        var rurColumn = this.getView().down('[dataIndex="rub"]');
        var usdColumn = this.getView().down('[dataIndex="usd"]');
        
        switch(currency) {
            case 'rur':
                rurColumn.show();
                usdColumn.hide();
                break;
            case 'usd':
                usdColumn.show();
                rurColumn.hide();
                break;
        }
        
        this.updateTitle(currency);
        
    },
    
    checkMonth: function(plan) {
        
        var planDate = plan.get('date');
        var planMonth = Ext.Date.format(planDate, 'm');
        var planYear = Ext.Date.format(planDate, 'Y');
        return planYear === this.getYear() && planMonth === this.getMonth();
        
    },
    
    onGridEdit: function(editor, context, eOpts ) {

        context.record.set('date', this.getDate());
        context.record.save();
    
    },
    
    onCategoryCollapse: function(collapsedCategory) {
        
        if (collapsedCategory === this.getCategory()) {
            this.getView().collapse();
        }
        
    },
    
    onCategoryExpand: function(expandedCategory) {
        
        if (expandedCategory === this.getCategory()) {
            this.getView().expand();
        }
        
    },
    
    onSelectionChange: function() {
        
        var grid = this.getView();
        var cellModel = grid.getSelectionModel();
        var plan = cellModel.getSelection()[0];
        var position = cellModel.getPosition();
        var category = this.getCategory();
        
        this.fireEvent('planSelected', grid, category, plan, position);
        
    },
    
    onPlanSelected: function(grid, category, plan, position) {

        var cellModel = this.getView().getSelectionModel();

        if (grid !== this.getView()) {
            if (this.getCategory() === category) {
                cellModel.select(plan, false, true);
            } else {
                cellModel.deselectAll(true);
            }
        }
        
    }
    
});