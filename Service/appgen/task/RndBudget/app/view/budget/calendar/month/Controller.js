Ext.define('System.view.budget.calendar.month.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetCalendarMonth',


    build: function(budget) {
        
        
        
    },
    
    onDisplayFieldDoubleClick: function() {
        
        var currencyDisplay = this.lookupReference('currencyDisplay');
        var currencyField = this.lookupReference('currencyField');
        
        currencyDisplay.hide();
        currencyField.show();
        
    }


});