Ext.define('System.view.budget.calendar.month.View', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.budgetCalendarMonth',
    requires: [
        'System.view.budget.calendar.month.Controller'
    ],
    controller: 'budgetCalendarMonth',
    width: 200,
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    items: [
        {
            reference: 'currencyDisplay',
            flex: 1,
            padding: '3 0 0 0',
            style: {
                backgroundColor: '#eee'
            },
            xtype: 'displayfield',
            listeners: {
                dblclick: 'onDisplayFieldDoubleClick',
                afterrender: function(field) {
                    field.mon(field.getEl(), 'dblclick', function() { field.fireEvent('dblclick'); })
                }
            }
        },
        {
            reference: 'currencyField',
            flex: 1,
            padding: '3 0 0 0',
            xtype: 'numberfield',
            hidden: true
        }

    ]
});