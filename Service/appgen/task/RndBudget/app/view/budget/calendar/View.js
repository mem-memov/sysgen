Ext.define('System.view.budget.calendar.View', {
    extend: 'Ext.container.Container',
    alias: 'widget.budgetCalendar',
    requires: [
        'System.view.budget.calendar.Controller',
        'System.view.budget.calendar.month.View',
        'System.view.budget.calendar.category.View'
    ],
    controller: 'budgetCalendar',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    scrollable: false
});