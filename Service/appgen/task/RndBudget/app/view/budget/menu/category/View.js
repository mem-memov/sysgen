Ext.define('System.view.budget.menu.category.View', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.budgetMenuCategory',
    requires: [
        'System.view.budget.menu.category.Controller'
    ],
    controller: 'budgetMenuCategory',
    items: [
        {
            text: 'Переименовать',
            menu: [
                {
                    reference: 'categoryField',
                    xtype: 'textfield'
                }
            ]

        },
        {
            text: 'Добавить позицию'
        }
    ],
    listeners: {
        
    }
});