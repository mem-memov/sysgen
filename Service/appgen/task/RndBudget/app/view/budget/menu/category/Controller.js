Ext.define('System.view.budget.menu.category.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetMenuCategory',
    
    config: {
        listen: {
            controller: {
                '*': {
                    requestCategoryMenu: 'onRequestCategoryMenu'
                }
            }
        }
    },
    
    onRequestCategoryMenu: function(x, y, category) {
        
        var categoryField = this.lookupReference('categoryField');
        categoryField.setValue(category.get('category'));
        
        this.getView().showAt(x, y, false);
        
    }
    
});