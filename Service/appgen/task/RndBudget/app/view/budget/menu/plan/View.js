Ext.define('System.view.budget.menu.plan.View', {
    extend: 'Ext.menu.Menu',
    alias: 'widget.budgetMenuPlan',
    requires: [
        'System.view.budget.menu.plan.Controller'
    ],
    controller: 'budgetMenuPlan',
    items: [{
        text: 'regular item 1'
    },{
        text: 'regular item 2'
    },{
        text: 'regular item 3'
    }]
});