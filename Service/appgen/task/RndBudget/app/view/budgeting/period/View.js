Ext.define('System.view.budgeting.period.View', {
    extend: 'Ext.Component',
    alias: 'widget.budgetingPeriod',
    requires: [
        'System.view.budgeting.period.Controller'
    ],
    controller: 'budgetingPeriod',
    tpl: [
        '<div class="budgetingPeriod">',
            '<table>',
                '<tr>',
                    '<tpl for="calendar">',
                        '<td>',
                            '<div class="period title">{title}</div>',
                        '</td>',
                    '</tpl>',
                '</tr>',
            '</table>',
        '</div>',
        {

        }
    ]
});