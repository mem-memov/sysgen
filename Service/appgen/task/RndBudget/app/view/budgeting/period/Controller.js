Ext.define('System.view.budgeting.period.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetingPeriod',
    
    build: function(data){
        
        this.getView().update(data);
        
    }
    
});