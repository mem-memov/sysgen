Ext.define('System.view.budgeting.View', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.budgeting',
    requires: [
        'System.view.budgeting.Controller',
        'System.view.budgeting.category.View',
        'System.view.budgeting.calendar.View',
        'System.view.budgeting.period.View',
        'System.view.budgeting.total.View'
    ],
    controller: 'budgeting',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    cls: 'budgeting',
    items: [
        {
            xtype: 'panel',
            title: 'Project',
            height: 100
        },
        {
            xtype: 'panel',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'panel',
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            reference: 'budgetingTotal',
                            xtype: 'budgetingTotal',
                            height: 100
                        },
                        {
                            reference: 'budgetingCategoryContainer',
                            title: 'Category',
                            xtype: 'panel',
                            flex: 1,
                            items: {
                                reference: 'budgetingCategory',
                                xtype: 'budgetingCategory',
                            }
                        }

                    ]
                },
                {
                    reference: 'splitter',
                    xtype: 'splitter',
                    listeners: {
                        move: 'onSplitterMove'
                    }
                },
                {
                    xtype: 'panel',
                    flex: 1,
                    layout: {
                        type: 'vbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            reference: 'budgetingPeriodContainer',
                            title: 'Period',
                            xtype: 'panel',
                            height: 100,
                            items: {
                                reference: 'budgetingPeriod',
                                xtype: 'budgetingPeriod',
                            }
                        },
                        {
                            reference: 'budgetingCalendarContainer',
                            title: 'Calendar',
                            xtype: 'panel',
                            flex: 1,
                            items: {
                                reference: 'budgetingCalendar',
                                xtype: 'budgetingCalendar',
                            },
                            scrollable: {
                                x: true,
                                y: true
                            },
                            listeners: {
                                scroll: {
                                    fn: 'onBudgetingCalendarContainerScroll',
                                    element: 'body'
                                }
                            }
                        }
                    ]
                }
            ]
        }

    ],
    
    listeners: {
        afterRender: 'afterRender'
    }
});