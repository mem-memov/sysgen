Ext.define('System.view.budgeting.category.View', {
    extend: 'Ext.Component',
    alias: 'widget.budgetingCategory',
    requires: [
        'System.view.budgeting.category.Controller'
    ],
    controller: 'budgetingCategory',
    tpl: [
        '<div class="budgetingCategory">',
            '<table>',
                '<tpl for="categories">',
                    '<tr>',
                        '<td>',
                            '<div class="category code"></div>',
                        '</td>',
                        '<td>',
                            '<div class="category name">{category}</div>',
                        '</td>',
                        '<td>',
                            '<div class="category money cell">',
                                '<div class="currency rur"></div>',
                                '<div class="currency usd"></div>',
                            '</div>',
                        '</td>',
                    '</tr>',
                    '<tpl for="plans">',
                        '<tr>',
                            '<td>',
                                '<div class="plan code">{[this.getBudgetCode(values)]}</div>',
                            '</td>',
                            '<td>',
                                '<div class="plan description">{description}</div>',
                            '</td>',
                            '<td>',
                                '<div class="plan money cell">',
                                    '<div class="currency rur"></div>',
                                    '<div class="currency usd"></div>',
                                '</div>',
                            '</td>',
                          '</tr>',
                    '</tpl>',
                '</tpl>',
            '</table>',
        '</div>',
        {
            getBudgetCode: function(values) {
                return values['budgeting.code']
            }
        }
    ],
    
    data: {
        categories: [],
        plans: []
    },

    afterRender: function() {
        this.callParent(arguments);

    } 
});