Ext.define('System.view.budgeting.category.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetingCategory',
    
    build: function(data){
        
        this.getView().update(data);
        
    },
    
    resizeHorizontal: function(x) {
        
        var viewElement = this.getView().getEl();

        if (!viewElement.down('table')) {
            return;
        }

        var delta = x - viewElement.down('table').getWidth();
        
        if (!delta) {
            return;
        }
        
        var elements = viewElement.query('.category.name, .plan.description', false);
        
        var newWidth;
        Ext.Array.each(elements, function(element, index) {
            if (index === 0) {
                newWidth = element.getWidth() + delta;
                if (newWidth < 0) {
                    return false;
                }
            }
            element.setWidth(newWidth);
        });
        
        return newWidth;
        
    }
    
});