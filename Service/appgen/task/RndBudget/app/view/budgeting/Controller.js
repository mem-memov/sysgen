Ext.define('System.view.budgeting.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgeting',
    
    afterRender: function() {
        
        var budgetingCategory = this.lookupReference('budgetingCategory');
        var budgetingCalendar = this.lookupReference('budgetingCalendar');
        var budgetingPeriod = this.lookupReference('budgetingPeriod');
        var budgetingTotal = this.lookupReference('budgetingTotal');
        
        var budgetId = 2;
        
        var budget = Ext.create('System.model.gridEditor.Budgets', {
            id: budgetId
        });
        
        budget.phantom = true; // send id but avoid id checking
        
        budget.load({
            params: {
                cluster: true
            },
            success: function(budget) {

                var data = budget.getData(true);
                data.calendar = this.buildCalendar(data);
                
                budgetingCategory.getController().build(data);
                budgetingCalendar.getController().build(data);
                budgetingPeriod.getController().build(data);

            },
            scope: this
        });
        
    },
    
    onBudgetingCalendarContainerScroll: function(event, target) {

        var budgetingCategoryContainer = this.lookupReference('budgetingCategoryContainer');
        var budgetingPeriodContainer = this.lookupReference('budgetingPeriodContainer');
        
        budgetingCategoryContainer.body.scrollTo('top', target.scrollTop, false);
        budgetingPeriodContainer.body.scrollTo('left', target.scrollLeft, false);

    },
    
    onSplitterMove: function(splitter, x, y) {

        var budgetingCategory = this.lookupReference('budgetingCategory');
        var budgetingCalendar = this.lookupReference('budgetingCalendar');

        /* Synchronize row hights of catalog and calendar tables */
        var newWidth = budgetingCategory.getController().resizeHorizontal(x);
        budgetingCalendar.getController().resizeHorizontal(newWidth);
        
    },
    
    buildCalendar: function(data) {
        
        var minDate = data.startDate;
        var maxDate = data.finishDate;

        minDate = Ext.Date.clearTime(minDate);
        maxDate = Ext.Date.clearTime(maxDate);

        var monthCount = Ext.Date.diff(minDate, maxDate, Ext.Date.MONTH);
        
        var calendar = [];
        
        for (var i = 0; i <= monthCount; i++) {
            
            var currentDate = Ext.Date.add(minDate, Ext.Date.MONTH, i);
            var yearMonth = Ext.Date.format(currentDate, 'Y-m');
            
            calendar.push({
                title: Ext.Date.format(currentDate, 'm Y'),
                month: Ext.Date.format(currentDate, 'n'),
                year: Ext.Date.format(currentDate, 'Y')
            });
            
        }
        
        return calendar;
        
    }
    
});