Ext.define('System.view.budgeting.total.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetingTotal',
    
    build: function(data){
        
        this.getView().update(data);
        
    }
    
});