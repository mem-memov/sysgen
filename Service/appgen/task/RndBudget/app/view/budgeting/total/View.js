Ext.define('System.view.budgeting.total.View', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.budgetingTotal',
    requires: [
        'System.view.budgeting.total.Controller'
    ],
    controller: 'budgetingTotal',
    title: 'Total',
    tpl: [
        '<div class="budgetingCPeriod">',
            '<table>',
                '<tr>',
                    '<td>',
                        '<div></div>',
                    '</td>',
                '</tr>',
            '</table>',
        '</div>',
        {

        }
    ]
});