Ext.define('System.view.budgeting.calendar.View', {
    extend: 'Ext.Component',
    alias: 'widget.budgetingCalendar',
    requires: [
        'System.view.budgeting.calendar.Controller'
    ],
    controller: 'budgetingCalendar',
    tpl: [
        '{[this.setCalendar(values.calendar)]}',
        '<div class="budgetingCalendar">',
            '<table>',
                '<tpl for="categories">',
                    '<tr>',
                        '<td>',
                            '<div class="category name">{category}</div>',
                        '</td>',
                        '<tpl for="parent.calendar">',
                            '<td>',
                                '<div class="category money cell">',
                                    '<div class="currency rur"></div>',
                                    '<div class="currency usd"></div>',
                                '</div>',
                            '</td>',
                        '</tpl>',
                    '</tr>',
                    '<tpl for="plans">',
                        '{[this.setCurrentPlan(values)]}',
                        '<tr>',
                            '<td>',
                                '<div class="plan description">{description}</div>',
                            '</td>',
                            '<tpl for="this.getCalendar()">',
                                '<td>',
                                    '<div class="plan money cell">',
                                        '<div class="currency rur">{[this.getRubles(values)]}</div>',
                                        '<div class="currency usd">{[this.getDollars(values)]}</div>',
                                    '</div>',
                                '</td>',
                            '</tpl>',
                        '</tr>',
                    '</tpl>',
                '</tpl>',
            '</table>',
        '</div>',
        {
            calendar: null,
            currentPlan: null,
            currentPlanMonth: null,
            currentPlanYear: null,
            setCalendar: function(calendar) {
                this.calendar = calendar;
            },
            getCalendar: function() {
                return this.calendar;
            },
            setCurrentPlan: function(currentPlan) {
                this.currentPlan = currentPlan;
                this.currentPlanMonth = Ext.Date.format(this.currentPlan.date, 'n');
                this.currentPlanYear = Ext.Date.format(this.currentPlan.date, 'Y');
            },
            getRubles: function(values) {
                if (this.currentPlanYear === values.year && this.currentPlanMonth === values.month) {
                    return this.currentPlan.rub + 'р.';
                } else {
                    return '&nbsp;';
                }
            },
            getDollars: function(values) {
                if (this.currentPlanYear === values.year && this.currentPlanMonth === values.month) {
                    return '$' + this.currentPlan.usd;
                } else {
                    return '&nbsp;';
                }
            }
        }
    ]
});