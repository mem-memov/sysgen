Ext.define('System.view.budgeting.calendar.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.budgetingCalendar',
    
    build: function(data){

        this.getView().update(data);

    },
    
    resizeHorizontal: function(newWidth) {

        var elements = this.getView().getEl().query('.category.name, .plan.description', false);

        Ext.Array.each(elements, function(element, index) {
            element.setWidth(newWidth);
            element.setStyle('margin-left', '-'+newWidth+'px');
        });
        
    }
    
});