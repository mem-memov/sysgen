Ext.define('System.view.Start', {
    extend: 'Ext.container.Container',
    alias: 'widget.start',
    requires: [
        //'System.view.budget.View'
        'System.view.budgeting.View'
    ],
    layout: 'fit',
    items: [
        {
           //xtype: 'budget'
           xtype: 'budgeting'
        }
    ]
    
});