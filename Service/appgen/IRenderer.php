<?php
namespace Service\appgen;
interface IRenderer {
    
    public function render($template, array $data);
    
}