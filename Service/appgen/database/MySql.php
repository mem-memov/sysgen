<?php
namespace Service\appgen\database;
class MySql {
    
    private $configuration;
    private $database;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
        $this->database = new mySql\Database($configuration);

    }
    
    public function generate() {
        
        $this->database->generate();
        
    }

}