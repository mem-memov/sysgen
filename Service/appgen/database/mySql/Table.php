<?php
namespace Service\appgen\database\mySql;
class Table {
    
    private $configuration;
    private $columns;
    private $input;
    private $masterTable;
    private $detailTables;
    private $aspectTables;
    
    public function __construct(array $configuration) {

        $this->configuration = $configuration;
        
        $this->columns = array();

        foreach ($configuration['columns'] as $columnKey => $columnConfiguration) {
            $this->columns[$columnKey] = new Column($columnConfiguration);
        }
        
        $this->masterTable = null;
        if (array_key_exists('masterTable', $configuration)) {
            $this->masterTable = new Association(array(
                'tablePrefix' => $this->configuration['tablePrefix'],
                'masterTable' => $configuration['masterTable'],
                'detailTable' => $this->configuration['name']
            )); 
        }

        $this->detailTables = array();
        if (array_key_exists('detailTables', $configuration)) {
            foreach ($configuration['detailTables'] as $detailTableKey) {
                $this->detailTables[$detailTableKey] = new Association(array(
                    'tablePrefix' => $this->configuration['tablePrefix'],
                    'masterTable' => $this->configuration['name'],
                    'detailTable' => $detailTableKey
                ));
            }
        }
        
        $this->aspectTables = array();
        foreach ($configuration['columns'] as $columnKey => $columnConfiguration) {
                if ($columnConfiguration['type'] == 'aspect') {
                    foreach ($columnConfiguration['tables'] as $aspectTable) {
                        $this->aspectTables[$aspectTable] = new Aspect(array(
                            'tablePrefix' => $this->configuration['tablePrefix'],
                            'masterTable' => $this->configuration['name'],
                            'masterColumn' => $columnKey,
                            'detailTable' => $aspectTable
                        ));
                    }
                }
        }

        if (!isset($configuration['input'])) {
            $this->input = null;
        } else {
            $inputClass = __NAMESPACE__.'\input\\'.$this->configuration['input']['type'];
            $this->configuration['input']['applicationName'] = $this->configuration['applicationName'];
            $this->input = new $inputClass($this->configuration['input']);
        }
        
        $this->configuration = $configuration;

    }
    
    public function generate($connection) {
        
        $this->generateTable($connection);
        $this->generateAccosiations($connection);
        $this->generateAspects($connection);
        
    }
    
    private function generateAspects($connection) {
        
        foreach ($this->aspectTables as $aspectTable) {
            
            $aspectTable->generate($connection);
            
        }
        
    }
    
    private function generateAccosiations($connection) {

        foreach ($this->detailTables as $detailTable) {
            
            $detailTable->generate($connection);
            
        }
        
    }
    
    private function generateTable($connection) {
        
        $table = $this->configuration['tablePrefix'].$this->configuration['name'];
        
        // drop
        $dropStatement = 'DROP TABLE IF EXISTS `'.$table.'`;';
        $connection->request($dropStatement);
        
        $columnStatements = array();
        $columnNames = array();
        foreach ($this->columns as $columnKey => $column) {
            $columnResult = $column->generate();
            $columnStatements[] = $columnResult['sqlStatement'];
            $columnNames[] = $columnResult['name'];
        }

        // create
        $createStatement = '
            CREATE TABLE 
                `'.$table.'` (
                    '.implode(', ', $columnStatements).'
                )
            ;
        ';
        $connection->request($createStatement);

        // fill
        $insertStatement = '';
        if (!is_null($this->input)) {
            $data = $this->input->generate();
            $valueStatements = array();
            foreach ($data as $rowIndex => $row) {

                if (!array_key_exists('id', $row)) {
                    $row['id'] = $rowIndex + 1;
                }
                if (!array_key_exists('orderNumber', $row)) {
                    $row['orderNumber'] = $rowIndex + 1;
                }
                
                if (!is_null($this->masterTable)) {
                    $masterKey = $this->configuration['masterTable'];
                    $masterId = $row[$masterKey];
                    $this->masterTable->addRow($connection, $masterId, $row['id']);
                    unset($row[$masterKey]);
                }
                
                if ($rowIndex == 0) {
                    $insertStatement = '
                        INSERT INTO 
                            '.$table.'(
                                '.implode(',', array_keys($row)).'
                            ) 
                        VALUES ';
                }
                
                $values = array();
                foreach ($row as $columnKey => $value) { // $this->columns must contain id and orderNumber columns
                    if (array_key_exists($columnKey, $this->columns)) {
                        $values[] = $this->columns[$columnKey]->wrapValue($connection, $value);
                    }
                }
                $valueStatements[] = '('.implode(',', $values).')';
                
            }
            $insertStatement .= implode(','."\n", $valueStatements).';'."\n\n";
        }

        $connection->request($insertStatement);
        
    }
    
}