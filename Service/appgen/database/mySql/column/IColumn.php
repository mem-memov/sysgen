<?php
namespace Service\appgen\database\mySql\column;
interface IColumn {
    
    public function generate();
    public function wrapValue($connection, $value);
    
}