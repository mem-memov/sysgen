<?php
namespace Service\appgen\database\mySql\column;
class Id implements IColumn {
    
    private $configuration;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
    }
    
    public function generate() {
        
        return '`id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT';
        
    }
    
    public function wrapValue($connection, $value) {
        
        return $connection->renderInteger($value);
        
    }
    
}