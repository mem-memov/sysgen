<?php
namespace Service\appgen\database\mySql\column;
class OrderNumber implements IColumn {
    
    private $configuration;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
    }
    
    public function generate() {
        
        return '`orderNumber` INT NULL';
        
    }
    
    public function wrapValue($connection, $value) {
        
        return $connection->renderInteger($value);
        
    }
    
}