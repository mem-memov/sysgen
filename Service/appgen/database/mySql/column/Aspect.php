<?php
namespace Service\appgen\database\mySql\column;
class Aspect implements IColumn {
    
    private $configuration;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
    }
    
    public function generate() {
        
        return '`' . $this->configuration['name'] . '` VARCHAR(512) NULL ';
        
    }
    
    public function wrapValue($connection, $value) {
        
        return $connection->renderInteger($value);
        
    }
    
}