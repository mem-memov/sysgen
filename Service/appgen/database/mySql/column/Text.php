<?php
namespace Service\appgen\database\mySql\column;
class Text implements IColumn {
    
    private $configuration;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
    }
    
    public function generate() {
        
        return '`' . $this->configuration['name'] . '` TEXT NULL';
        
    }
    
    public function wrapValue($connection, $value) {
        
        return $connection->renderString($value);
        
    }
    
}