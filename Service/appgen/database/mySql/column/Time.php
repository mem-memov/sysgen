<?php
namespace Service\appgen\database\mySql\column;
class Time implements IColumn {
    
    private $configuration;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
    }
    
    public function generate() {
        
        return '`' . $this->configuration['name'] . '` timestamp NULL DEFAULT "0000-00-00 00:00:00" ';
        
    }
    
    public function wrapValue($connection, $value) {
        
        return $connection->renderString($value);
        
    }
    
}