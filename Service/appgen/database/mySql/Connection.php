<?php
namespace Service\appgen\database\mySql;
class Connection {
    
    private $configuration;
    private $connection;
    
    public function __construct(array $configuration) {
        $this->configuration = $configuration;
    }
    
    public function generate() {
        
        $this->connect();
        
    }
    
    public function request($query) {

        $this->connection->real_query($query);
        
    }
    
    public function renderString($value) {
        return '"'.$this->connection->real_escape_string($value).'"';
    }
    
    public function renderBoolean($value) {
        return (bool)$value ? '1' : '0';
    }
    
    public function renderInteger($value) {
        return (int)$value;
    }
    
    private function connect() {
       
        $this->connection = new \mysqli(
            $this->configuration['server'], 
            $this->configuration['user'], 
            $this->configuration['password'], 
            $this->configuration['database'], 
            $this->configuration['port']
        );
        
        if ($this->connection->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
        
    }
    
}