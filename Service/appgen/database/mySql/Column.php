<?php
namespace Service\appgen\database\mySql;
class Column {
    
    private $configuration;
    private $type;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
        $class = __NAMESPACE__ . '\column\\' . ucfirst($this->configuration['type']);
        $this->type = new $class($configuration);
        
    }
    
    public function generate() {
        
        return array(
            'sqlStatement' => $this->type->generate(),
            'name' => $this->configuration['name']
        );
        
    }
    
    public function wrapValue($connection, $value) {
        
        return $this->type->wrapValue($connection, $value);

    }
    
}