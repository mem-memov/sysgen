<?php
namespace Service\appgen\database\mySql\input;
class Csv {
    
    private $configuration;
    
    public function __construct($configuration) {
        
        $this->configuration = array_merge(array(
            'delimiter' => ','
        ), $configuration);
        
    }
    
    public function generate() {

        $file = realpath(__DIR__.'/../../../task/'.$this->configuration['applicationName'].'/'.$this->configuration['file']);
        
        $data = $this->readFile($file, $this->configuration['delimiter'], $this->configuration['map']);

        return $data;
        
    }
    
    private function readFile($file, $delimiter, array $map) {

        $data = array();
        
        if (($handle = fopen($file, 'r')) !== FALSE) {
            while (($set = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                $mapedSet = array();
                foreach ($set as $index => $value) {
                    $mapedSet[$map[$index]] = $value;
                }
                $data[] = $mapedSet;
            }
            fclose($handle);
        }

        return $data;
        
    }
    
}