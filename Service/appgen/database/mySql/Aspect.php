<?php
namespace Service\appgen\database\mySql;
class Aspect {
    
    private $configuration;
    private $table;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        $this->table = $this->configuration['tablePrefix'].$this->configuration['masterTable'].'_'.$this->configuration['masterColumn'].'_'.$this->configuration['detailTable'];
        
    }
    
    public function generate($connection) {

        $this->deleteTable($connection);
        $this->createTable($connection);
        
    }
    
    public function addRow($connection, $masterId, $detailId) {
   
        $query = '
            INSERT INTO 
                '.$this->table.'(
                    '.$this->configuration['masterTable'].',
                    '.$this->configuration['detailTable'].'
                ) 
            VALUES (
                '.(int)$masterId.', 
                '.(int)$detailId.' 
            );
        ';

        $connection->request($query);
        
    }
    
    
    
    private function deleteTable($connection) {
        
        $dropStatement = 'DROP TABLE IF EXISTS `'.$this->table.'`;';

        $connection->request($dropStatement);
        
    }
    
    private function createTable($connection) {
        
        $query = '
            CREATE TABLE 
                `'.$this->table.'`(
                    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
                    `'.$this->configuration['masterTable'].'` INT,
                    `'.$this->configuration['detailTable'].'` INT
                )
            ;
        ';
        
        $connection->request($query);
        
    }
    
}