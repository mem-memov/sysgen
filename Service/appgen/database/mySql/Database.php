<?php
namespace Service\appgen\database\mySql;
class Database {
    
    private $configuration;
    private $connection;
    private $tables;

    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
        $this->connection = new Connection($this->configuration['database']['connection']);
        
        $this->tables = array();
        foreach ($configuration['tables'] as $tableKey => $tableConfiguration) {
            $tableConfiguration['applicationName'] = $this->configuration['name'];
            $this->tables[$tableKey] = new Table($tableConfiguration);
        }
        
    }
    
    public function generate() {

        $this->connection->generate();
        
        $tableStatements = array();
        foreach ($this->tables as $tableKey => $table) {
            $tableStatements[] = $table->generate($this->connection);
        }
        
        foreach ($tableStatements as $tableStatement) {
            $this->connection->request($tableStatement['dropStatement']);
            $this->connection->request($tableStatement['createStatement']);
        }
        
    }

}