<?php
namespace Service\appgen;
interface IFileSystem {
    
    public function makeDirectory($directory);
    public function deleteDirectory($directory);
    public function copyDirectory($source, $desination);
    public function writeFile($file, $contents);
    public function copyFile($source, $desination);
    
}