<?php
namespace Service\appgen\server\php;
class Generator {
    
    private $configuration;
    private $applicationDirectory;
    private $fileSystem;
    
    private $typeMap;
    
    public function __construct(
        array $configuration, 
        $applicationDirectory, 
        \Service\appgen\IFileSystem $fileSystem
    ) {
        
        $this->configuration = $configuration;
        $this->applicationDirectory = $applicationDirectory;
        $this->fileSystem = $fileSystem;
        
        $this->typeMap = array(
            'id' => 'integer',
            'orderNumber' => 'integer',
            'masterId' => 'integer',
            'parentId' => 'integer',
            'string' => 'string',
            'integer' => 'integer',
            'boolean' => 'boolean',
            'text' => 'string',
            'time' => 'date',
            'reference' => 'integer',
            'aspect' => 'string'
        );
        
    }
    
    public function generate() {
        
        $this->generateWebServerConfig();
        $this->generateConfiguration();
        $this->copyMainFile();
        $this->copyClasses();
        $this->copyHooks();
        
    }
    
    private function copyClasses() {

        $source = __DIR__.'/template/php';
        $destination = $this->applicationDirectory.'/php';
        $this->fileSystem->copyDirectory($source, $destination);
        
    }
    
    private function generateConfiguration() {
        
        $configuration = array(
            'application' => $this->configuration['name'],
            'aspectMap' => $this->generateAspectMap(),
            'associationMap' => $this->generateAssociationMap(),
            'referenceMap' => $this->generateReferenceMap(),
            'api' => $this->generateApi(),
            'database' => $this->configuration['database'],
            'cache' => array_key_exists('cache', $this->configuration) ? $this->configuration['cache'] : null
        );
        
        $php = '<?php return ' . var_export($configuration, true) . ';';

        $this->fileSystem->writeFile($this->applicationDirectory.'/configuration.php', $php);
        
    }
    
    private function generateAspectMap() {
        
        $data = array();

        foreach ($this->configuration['tables'] as $tableKey => $tableConfiguration) {
            
            if (array_key_exists('aspectMasterTables', $tableConfiguration)) {
                foreach ($tableConfiguration['aspectMasterTables'] as $aspectMasterTable) {
                    $data[$tableKey]['aspectMasterTables'][$aspectMasterTable['table']][] = $aspectMasterTable['column'];
                }
            }
            
            foreach ($tableConfiguration['columns'] as $fieldKey => $fieldConfiguration) {
                if ($fieldConfiguration['type'] == 'aspect') {
                    foreach ($fieldConfiguration['aspectDetailTables'] as $aspectDetailTable) {
                        $data[$tableKey]['aspectDetailTables'][$fieldKey][] = $aspectDetailTable['name'];
                    }
                }
            }
            
        }

        return $data;
        
    }
    
    private function generateReferenceMap() {
        
        $data = array();

        foreach ($this->configuration['tables'] as $tableKey => $tableConfiguration) {
            foreach ($tableConfiguration['columns'] as $fieldKey => $fieldConfiguration) {
                if ($fieldConfiguration['type'] == 'reference') {
                    $data[$tableKey][$fieldKey]['table'] = $fieldConfiguration['table'];
                    foreach ($fieldConfiguration['columns'] as $column) {
                        $data[$tableKey][$fieldKey]['fields'][$column] = $this->configuration['tables'][$fieldConfiguration['table']]['columns'][$column]['type'];
                    }
                }
            }
        }

        return $data;
        
    }
    
    private function generateAssociationMap() {
        
        $data = array();

        foreach ($this->configuration['tables'] as $tableKey => $tableConfiguration) {
            if (array_key_exists('masterTable', $tableConfiguration)) {
                $data[$tableKey]['masterTable'] = $tableConfiguration['masterTable'];
            }
            if (array_key_exists('detailTables', $tableConfiguration)) {
                $data[$tableKey]['detailTables'] = $tableConfiguration['detailTables'];
            }
        }

        return $data;
        
    }
    
    private function generateApi() {
        
        $data = array();

        foreach ($this->configuration['tables'] as $tableKey => $tableConfiguration) {
            $data[$tableKey] = array(
                'fields' => array(
                    'id' => 'integer', 
                    'orderNumber' => 'integer'
                )
            );
            if ($this->configuration['tables'][$tableKey]['tree']) {
                $data[$tableKey]['fields']['parentId'] = 'integer';
            }
            foreach ($tableConfiguration['columns'] as $columnName => $columnConfiguration) {
                $data[$tableKey]['fields'][$columnName] = $this->typeMap[$columnConfiguration['type']];
            }
            if (array_key_exists('masterTable', $tableConfiguration)) {
                $masterTable = $tableConfiguration['masterTable'];
                $data[$tableKey]['fields'][$masterTable] = 'integer';
            }
            if (array_key_exists('aspectMasterTables', $tableConfiguration)) {
                foreach ($tableConfiguration['aspectMasterTables']  as $aspect) {
                    $aspectField = $aspect['table'].'.'.$aspect['column'];
                    $data[$tableKey]['fields'][$aspectField] = 'integer';
                }
            }
        }
        
        return $data;
        
    }
    
    private function copyMainFile() {

        $this->fileSystem->copyFile(__DIR__.'/template/main.php', $this->applicationDirectory.'/main.php');
        
    }
    
    private function generateWebServerConfig() {
        
        if ($this->configuration['webServer']['serverType'] == 'apache') {
            $this->fileSystem->copyFile(__DIR__.'/template/.htaccess', $this->applicationDirectory.'/.htaccess');
        }
        
    }
    
    private function copyHooks() {
        
        $this->fileSystem->copyDirectory(
            __DIR__.'/../../task/'.$this->configuration['name'].'/hook', 
            $this->applicationDirectory.'/php/Application/api/rest/operation/hook/'.$this->configuration['name']
        );
        
    }
    
}