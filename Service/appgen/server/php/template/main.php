<?php

date_default_timezone_set('Europe/Moscow');

$configuration = require_once 'configuration.php';

function __autoload($class) {

    $path = __DIR__.'/php/'.str_replace('\\', '/', $class).'.php';

    if (!file_exists($path)) {
        throw new Exception('Class file not found: '.$path);
    }
    
    require $path;
    
}

new Application\Facade($configuration);
