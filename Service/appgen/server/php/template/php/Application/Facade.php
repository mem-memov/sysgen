<?php
namespace Application;
class Facade {
    
    private $database;
    private $api;
    private $associationMap;
    private $referenceMap;
    private $aspectMap;
    private $application;
    private $cache;
            
    public function __construct(array $configuration) {

        $this->database = $configuration['database'];
        $this->api = $configuration['api'];
        $this->associationMap = $configuration['associationMap'];
        $this->referenceMap = $configuration['referenceMap'];
        $this->aspectMap = $configuration['aspectMap'];
        $this->application = $configuration['application'];
        $this->cache = $configuration['cache'];
        
        $this->response();
        
    }
    
    private function response() {
        
        
        
        $cacheFactory = new cache\Factory($this->application, $this->cache);
        $cache = $cacheFactory->make();
        
        $databaseFactory = new database\Factory($this->database);
        $database = $databaseFactory->make();

        $valueWrapper = new api\rest\ValueWrapper($database);
        
        $queryFactory = new api\rest\operation\query\Factory(
            $database, 
            $valueWrapper, 
            $this->api
        );
        
        $request = new api\rest\Request();
        $response = new api\rest\Response();
        

        
        $tableFactory = new api\rest\operation\table\Factory(
            $this->aspectMap, 
            $this->associationMap, 
            $this->referenceMap, 
            $this->api, 
            $this->database['tablePrefix']
        );
        
        $table = $tableFactory->requestToTable($request);
        
        $valueFactory = new api\rest\operation\value\Factory($this->api);

        $hookServiceLocator = new api\rest\operation\hook\Service(
            $request,
            $response,
            $table,
            $database,
            $valueWrapper,
            $cache,
            $queryFactory,
            $valueFactory
        );
        
        $hookFactory = new api\rest\operation\hook\Factory($this->application, $hookServiceLocator);
        
        $operationFactory = new api\rest\operation\Factory(
            $queryFactory, 
            $table, 
            $valueFactory, 
            $hookFactory, 
            $cache
        );
        
        $httpFactory = new api\rest\Factory($operationFactory);

        $httpFactory->make($request, $response)->response();
        
    }

    
}