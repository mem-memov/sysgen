<?php
namespace Application\cache;
class Factory implements IFactory {
    
    private $configuration;
    private $application;
    
    public function __construct($application, array $configuration = null) {
        
        $this->application = $application;
        $this->configuration = $configuration;
        
    }
    
    public function make() {
        
        if (is_null($this->configuration)) {
            return $this->phantom();
        }
        
        $method = $this->configuration['type'];
        
        return $this->$method();
        
    }

    private function file() {
        
        return new File($this->application, $this->configuration['directory']);
        
    }
    
    private function phantom() {
        
        return new Phantom();
        
    }
    
}