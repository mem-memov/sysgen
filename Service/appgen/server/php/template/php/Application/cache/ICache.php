<?php
namespace Application\cache;
interface ICache {
    
    public function set($key, $value);
    public function get($key);
    public function drop($key);
    
}