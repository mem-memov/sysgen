<?php
namespace Application\cache;
interface IFactory {
    
    public function make();
    
}