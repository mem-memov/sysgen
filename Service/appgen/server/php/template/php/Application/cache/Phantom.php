<?php
namespace Application\cache;
/**
 * Null Object - http://www.oodesign.com/null-object-pattern.html
 */
class Phantom implements ICache {

    public function set($key, $value) {}
    
    public function get($key) { return null; }
    
    public function drop($key) {}
    
}


