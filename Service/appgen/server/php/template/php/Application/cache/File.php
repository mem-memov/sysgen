<?php
namespace Application\cache;
/**
 * "cache": {
 *     "type": "file",
 *     "directory": "tmp/cache"
 * }
 */
class File implements ICache {
    
    private $application;
    private $directory;
    
    public function __construct($application, $directory) {
        
        $this->application = $application;
        
        $this->directory = __DIR__.'/../../../../../'.$directory.'/'.$application;

        if (!file_exists($this->directory)) {
            mkdir($this->directory, 0777, true);
        }
        
    }
    
    public function set($key, $value) {
        
        file_put_contents(
            $this->file($key), 
            '<?php return ' . var_export($value, true) . ';'
        );
        
    }
    
    public function get($key) {
        
        $file = $this->file($key);
        
        if (!is_file($file)) {
            return null;
        }
        
        return require $file;
        
    }
    
    public function drop($key) {
        
        unlink($this->file($key));
        
    }
    
    private function file($key) {
        
        return $this->directory.'/'.$key.'.php';
        
    }
    
}