<?php
namespace Application\database;
interface IDatabase {
    
    public function insert($query);
    public function delete($query);
    public function update($query);
    public function select($query, array $types = array());
    
    public function startTransaction();
    public function commitTransaction();
    public function rollbackTransaction();
    
    public function renderString($value);
    public function renderDate($value);
    public function renderBoolean($value);
    public function renderInteger($value);
    
}