<?php
namespace Application\database;
class Factory implements IFactory {
    
    private $configuration;
    
    public function __construct(array $configuration) {
        
        $this->configuration = $configuration;
        
    }
    
    public function make() {

        $method = lcfirst($this->configuration['type']);
        
        return $this->$method();
        
    }
    
    public function mySql() {
        
        return new mySql\Database(
            $this->configuration['connection']['server'], 
            $this->configuration['connection']['port'], 
            $this->configuration['connection']['database'], 
            $this->configuration['connection']['user'], 
            $this->configuration['connection']['password']
        );
        
    }
    
}