<?php
namespace Application\database;
interface IFactory {
    
    public function make();
    
}