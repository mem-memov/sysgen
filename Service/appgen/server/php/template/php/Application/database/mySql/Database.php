<?php
namespace Application\database\mySql;
/**
 * Adapter: http://www.oodesign.com/adapter-pattern.html
 * 
 * "database": {
 *     "type": "MySql",
 *     "tablePrefix": "OrgChartTest_",
 *     "connection": {
 *         "user": "backend",
 *         "password": "hjEBDVlv",
 *         "server": "192.168.4.124",
 *         "port": "3306",
 *         "database": "backend"
 *     }
 * }
 */
class Database implements \Application\database\IDatabase {
    
    private $host;
    private $port;
    private $database;
    private $user;
    private $password;
    /**
     *
     * @var mysqli
     */
    private $connection;
    
    public function __construct($host, $port, $database, $user, $password) {
        
        $this->host = $host;
        $this->port = $port;
        $this->database = $database;
        $this->user = $user;
        $this->password = $password;
        $this->connection = null;
        
    }
    
    public function __destruct() {
        
        if (!is_null($this->connection)) {
            $this->connection->close();
        }
        
    }
    
    public function query($query) {
        
        $this->request($query);
        
    }
    
    public function insert($query) {
        
        $this->request($query);
        return $this->connection->insert_id;
        
    }
    
    public function delete($query) {
        
        $this->request($query);
        return $this->connection->affected_rows;
        
    }
    
    public function update($query) {
        
        $this->request($query);
        return $this->connection->affected_rows;
        
    }
    
    public function select($query, array $types = array()) {
        
        $rows = array();

        $this->request($query);
        
        $result = $this->connection->use_result();
        
        if ($result !== false) {
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            $result->close();
        }

        if (!empty($types)) {
            foreach ($rows as $index => $row) {
                foreach ($row as $field => $value) {
                    if (array_key_exists($field, $types)) {
                        switch ($types[$field]) {
                            case 'integer':
                                $rows[$index][$field] = (int)$value;
                                break;
                            case 'boolean':
                                $rows[$index][$field] = (bool)$value;
                                break;
                        }
                    }
                }
            }
        }

        return $rows;
        
    }
    
    public function startTransaction() {
        
        $this->request('START TRANSACTION;');
        
    }
    
    public function commitTransaction() {
        
        $this->request('COMMIT;');
        
    }
    
    public function rollbackTransaction() {
        
        $this->request('ROLLBACK;');
        
    }
    
    private function request($query) {

        if (is_null($this->connection)) {
            $this->connect();
        }
        
        $this->connection->real_query($query);
        
    }
    
    public function renderString($value) {
        
        if (is_null($this->connection)) {
            $this->connect();
        }
        
        return '"'.$this->connection->real_escape_string($value).'"';
    }
    
    public function renderDate($value) {
        
        if (is_null($this->connection)) {
            $this->connect();
        }

        return '"'.$this->connection->real_escape_string($value).'"';
    }
    
    public function renderBoolean($value) {
        return (bool)$value ? '1' : '0';
    }
    
    public function renderInteger($value) {
        return (int)$value;
    }
    
    private function connect() {
        
        $this->connection = new \mysqli($this->host, $this->user, $this->password, $this->database, $this->port);
        
        if ($this->connection->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
        
        $this->request('USE `'.$this->database.'` ;');
        
    }
    
}