<?php
namespace Application\api\rest\operation\table;
class Reference implements IReference {
    
    private $containerTable;
    private $containerField; 
    private $contentTable; 
    private $contentFields;
    
    public function __construct(
        ITable $containerTable, 
        IField $containerField, 
        ITable $contentTable, 
        array $contentFields
    ) {
        
        $this->containerTable = $containerTable;
        $this->containerField = $containerField;
        $this->contentTable = $contentTable;
        $this->contentFields = $contentFields;
        
    }

    public function getContainerField() {
        return $this->containerField;
    }

    public function getContentFeilds() {
        return $this->contentFields;
    }

    public function getContentTable() {
        return $this->contentTable;
    }
    
}