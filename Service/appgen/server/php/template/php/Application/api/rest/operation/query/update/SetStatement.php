<?php
namespace Application\api\rest\operation\query\update;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class SetStatement implements IState {
    
    private $valueWrapper;
    private $table;
    private $values;
    
    public function __construct(
        $valueWrapper, 
        \Application\api\rest\operation\table\ITable $table, 
        array $values
    ) {

        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->values = $values;
 
    }
    
    public function state() {

        $statement = '';
        
        $parts = array();
        
        foreach ($this->values as $value) {
            $parts[] = ' `'.$this->table->name().'`.`'.$value->getField().'` = '.$this->valueWrapper->wrapValue($value->getType(), $value->getValue()).' ';
        }
        
        $statement .= implode(', ', $parts);
        
        return $statement;
        
    }
    
}