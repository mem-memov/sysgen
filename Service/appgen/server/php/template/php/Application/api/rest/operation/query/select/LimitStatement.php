<?php
namespace Application\api\rest\operation\query\select;
class LimitStatement implements IState {
    
    private $valueWrapper;
    private $table;
    
    public function __construct($valueWrapper, \Application\api\rest\operation\table\ITable $table) {

        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
 
    }
    
    public function state() {

        $statement = '';
        
        if (!is_null($this->table->getPage())) {
            $statement = ' LIMIT '.$this->table->getPage()->getLimit().' OFFSET '.$this->table->getPage()->getStart().' ';
        }

        return $statement;
        
    }
    
}