<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class HasChildren {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $parentId;
    
    public function __construct(
        $database, 
        $valueWrapper,
        $table, 
        $parentId
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->parentId = $parentId;

    }
    
    public function query() {

        $query = '
            SELECT 
                `id`
            FROM 
                `'.$this->table.'` 
            WHERE
                `'.$this->table.'`.`parentId` = '.$this->valueWrapper->wrapValue('integer', $this->parentId).'
            LIMIT
                1
            ;
        ';

        $rows = $this->database->select($query);
        
        return empty($rows);
        
    }
    
}