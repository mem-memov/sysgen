<?php
namespace Application\api\rest;
interface IResponse {
    
    public function sendError($message);
    public function sendData(array $output);
    public function confirmSuccess();
    
}