<?php
namespace Application\api\rest\operation\query\select;
class GroupStatement implements IState {
    
    private $valueWrapper;
    private $table;
    
    public function __construct($valueWrapper, \Application\api\rest\operation\table\ITable $table) {

        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
 
    }
    
    public function state() {

        $statement = '';
        
        if (!is_null($this->table->getGroup())) {
            $group = $this->table->getGroup();
            $direction = $group->isAscending() ? 'ASC' : 'DESC';
            $statement = ' GROUP BY `'.$this->table->name().'`.`'.$group->getField().'` '.$direction.' ';
        }
        
        return $statement;
        
    }
    
}