<?php
namespace Application\api\rest;
interface IRequest {
    
    public function getHttpMethod();
    public function getApiPathParts();
    public function checkGet($key);
    public function readGet($key);
    public function checkPost($key);
    public function readPost($key);
    public function readRaw();
    
}