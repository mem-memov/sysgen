<?php
namespace Application\api\rest\operation\query\select;
class JoinStatement implements IState {
    
    private $table;
    
    public function __construct(\Application\api\rest\operation\table\ITable $table) {

        $this->table = $table;
 
    }
    
    public function state() {

        $statement = '';

        $masterGlue = $this->table->getMasterGlue();
        
        if (!is_null($masterGlue)) { // master table object may be just NULL

            $statement .= ' LEFT JOIN `'.$masterGlue->table().'` ON (`'.$masterGlue->table().'`.`'.$masterGlue->detail().'` = `'.$this->table->name().'`.`id`) ';
        
        }
        
        foreach ($this->table->getReferences() as $reference) {
            
            $contentTable = $reference->getContentTable();
            $containerField = $reference->getContainerField();
            
            $statement .= ' LEFT JOIN `'.$contentTable->name().'` ON (`'.$contentTable->name().'`.`id` = `'.$this->table->name().'`.`'.$containerField->getName().'`) ';
 
        }

        foreach ($this->table->getMasterAspects() as $aspect) {

            $statement .= ' LEFT JOIN `'.$aspect->table().'` ON (`'.$aspect->table().'`.`'.$aspect->detail().'` = `'.$this->table->name().'`.`id`) ';
            
        }

        return $statement;
        
    }
    
}