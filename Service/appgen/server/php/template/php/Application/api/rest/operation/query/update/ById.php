<?php
namespace Application\api\rest\operation\query\update;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ById implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $id;
    private $setStatement;
    
    public function __construct($database, $valueWrapper, $table, $id, $setStatement) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->id = $id;
        $this->setStatement = $setStatement;

    }
    
    public function query() {
        
        $query = '
            UPDATE  
                `'.$this->table.'` 
            SET 
                '.$this->setStatement->state().' 
            WHERE 
                `'.$this->table.'`.`id` = '.$this->valueWrapper->wrapValue('integer', $this->id).' 
            ;
        ';

        $affectedRows = $this->database->update($query);
        
        return  $affectedRows;

        
    }

    
}