<?php
namespace Application\api\rest\operation\query;
interface IFactory {
    
    /**
     * @return delete\IFactory
     */
    public function delete();
    
    /**
     * @return update\IFactory
     */
    public function update();
    
    /**
     * @return insert\IFactory
     */
    public function insert();
    
    /**
     * @return select\IFactory
     */
    public function select();

}