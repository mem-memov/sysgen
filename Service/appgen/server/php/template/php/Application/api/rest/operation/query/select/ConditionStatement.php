<?php
namespace Application\api\rest\operation\query\select;
class ConditionStatement implements IState {
    
    private $valueWrapper;
    private $table;
    
    public function __construct(
        $valueWrapper, 
        \Application\api\rest\operation\table\ITable $table
    ) {

        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
 
    }
    
    public function state() {

        $statement = '';
        
        $parts = array();
        
        foreach ($this->table->getFilters() as $filter) {

            $operator = $filter->getOperator();
            $value = $filter->getValue();
            $type = $filter->getType();
            
            $masterGlue = $this->table->getMasterGlue();
            
            $property = '`'.$this->table->name().'`.`'.$filter->getField().'`'; // intentionally overwriting property
            
            if (!is_null($masterGlue) && $masterGlue->isMasterField($filter->getField())) {
                $property = '`'.$masterGlue->table().'`.`'.$masterGlue->master().'`'; // join needed here
            }
            
            foreach ($this->table->getMasterAspects() as $aspect) {
                $aspectField = $aspect->master().'.'.$aspect->field();
                if ($aspectField == $filter->getField()) {
                    $property = '`'.$aspect->table().'`.`'.$aspect->master().'`'; // join needed here
                }
            }
            
            if (in_array($type, array('string', 'text'))) {
                $value = '%'.$value.'%';
            }

            if ($type == 'boolean') {
                $value = $value ? '1' : '0';
            }
            
            $parts[] = ' AND '.$property.' '.$operator.' '.$this->valueWrapper->wrapValue($type, $value).' ';
        }

        if (count($parts) > 0) {
            $statement = ' WHERE TRUE '.implode(' ', $parts);
        }
        
        return $statement;
        
    }
    
}