<?php
namespace Application\api\rest\operation\query\update;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {
    
    private $database;
    private $valueWrapper;
    
    public function __construct($database, $valueWrapper) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        
    }
    
    public function byId(\Application\api\rest\operation\table\ITable $table, $id, array $values) {
        
        return new ById(
            $this->database,
            $this->valueWrapper,
            $table->name(), 
            $id,
            new SetStatement($this->valueWrapper, $table, $values)
        );
        
    }

    public function shiftUp(\Application\api\rest\operation\table\ITable $table, $orderNumber) {
        
        return new ShiftUp(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $orderNumber
        );

    }
    
    public function shiftDown(\Application\api\rest\operation\table\ITable $table, $orderNumber) {
        
        return new ShiftUp(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $orderNumber
        );

    }
    
    public function reorderAfterDown(\Application\api\rest\operation\table\ITable $table, $id, $from, $to) {
        
        return new ReorderAfterDown(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $id, 
            $from, 
            $to
        );
        
    }
    
    public function reorderAfterUp(\Application\api\rest\operation\table\ITable $table, $id, $from, $to) {
        
        return new ReorderAfterUp(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $id, 
            $from, 
            $to
        );
        
    }
   
    public function reorderBeforeDown(\Application\api\rest\operation\table\ITable $table, $id, $from, $to) {
        
        return new ReorderBeforeDown(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $id, 
            $from, 
            $to
        );
        
    }
    
    public function reorderBeforeUp(\Application\api\rest\operation\table\ITable $table, $id, $from, $to) {
        
        return new ReorderBeforeUp(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $id, 
            $from, 
            $to
        );
        
    }
    
    public function orderNumber(\Application\api\rest\operation\table\ITable $table, $orderNumber, $id) {
        
        return new OrderNumber(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $orderNumber, 
            $id
        );
        
    }
    
    
}