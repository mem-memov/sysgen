<?php
namespace Application\api\rest\operation\hook;
/**
 * Service Locator - http://en.wikipedia.org/wiki/Service_locator_pattern
 */
class Service implements IService {
    
    private $request;
    private $response;
    private $table;
    private $database;
    private $valueWrapper;
    private $cache;
    private $queryFactory;
    private $valueFactory;
    
    public function __construct(
        \Application\api\rest\IRequest $request,
        \Application\api\rest\IResponse $response,
        \Application\api\rest\operation\table\ITable $table,
        \Application\database\IDatabase  $database,
        \Application\api\rest\IValueWrapper $valueWrapper,
        \Application\cache\ICache $cache,
        \Application\api\rest\operation\query\IFactory $queryFactory,
        \Application\api\rest\operation\value\IFactory $valueFactory
    ) {
        
        $this->request = $request;
        $this->response = $response;
        $this->table = $table;
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->cache = $cache;
        $this->queryFactory = $queryFactory;
        $this->valueFactory = $valueFactory;
        
    }

    public function request() { 
        return $this->request; 
    }

    public function response() { 
        return $this->response; }
    
    public function table() { 
        return $this->table; 
    }
    
    public function database() { 
        return $this->database; 
    }
    
    public function valueWrapper() { 
        return $this->valueWrapper; 
    }
    
    public function cache() { 
        return $this->cache; 
    }
    
    public function query() {
        return $this->queryFactory;
    }
    
    public function value() {
        return $this->valueFactory;
    }
    
}