<?php
namespace Application\api\rest;
class Put implements IHttpMethod {
    
    private $operationFactory;
    private $request;
    private $response;
    
    public function __construct($operationFactory, IRequest $request, IResponse $response) {
        
        $this->operationFactory = $operationFactory;
        $this->request = $request;
        $this->response = $response;
        
    }
    
    public  function response() {

        $pathParts = $this->request->getApiPathParts();
        
        if (count($pathParts) < 2) {
            $this->response->sendError('Api entity id unknown');
        }
        
        $id = $pathParts[1];

        $oldValues = $this->operationFactory->fetch($this->request)->run();
        
        if (is_null($oldValues)) {
            $this->response->sendError('Row has been deleted');
        }
        
        $values = json_decode($this->request->readRaw(), true, 100, JSON_UNESCAPED_UNICODE);

        if (array_key_exists('orderNumber', $values) && $values['orderNumber'] !== $oldValues['orderNumber']) {
            $this->reorder($id, $oldValues['orderNumber'], $values['orderNumber']);
        } else {
            $this->update($id);
        }

    }
    
    private function update($id) {

        $affectedRows = $this->operationFactory->change($this->request, $id)->run();

        if ($affectedRows == 1) {
            $this->response->confirmSuccess();
        } else {
            $this->response->sendError('Error when updating row');
        }
        
    }

    private function reorder($id, $from, $to) {
        
        try {
            
            $this->operationFactory->reorder($this->request, $id, $from, $to)->run();

        } catch (\Exception $ex) {
            $this->response->sendError('Error when reordering rows');
        }
        
        $this->response->confirmSuccess();
        
    }

}