<?php
namespace Application\api\rest\operation\query\delete;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ById {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $id;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $table, 
        $id
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->id = $id;
        
    }
    
    public function query() {
        
        $query = '
            DELETE FROM  
                `'.$this->table.'` 
            WHERE 
                `'.$this->table.'`.`id` = '.$this->valueWrapper->wrapValue('integer', $this->id).' 
            ;
        ';

        $affectedRows = $this->database->delete($query);
        
        return $affectedRows;
        
    }

}