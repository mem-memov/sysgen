<?php
namespace Application\api\rest\operation\query\update;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ReorderBeforeDown implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $from;
    private $to;
    
    public function __construct($database, $valueWrapper, $table, $from, $to) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->from = $from;
        $this->to = $to;

    }
    
    public function query() {
        
        $query = '
            UPDATE 
                `'.$this->table.'` 
            SET 
                `orderNumber` = `orderNumber` - 1 
            WHERE 
                `orderNumber` < '.$this->valueWrapper->wrapValue('integer', $this->to).' 
                AND `orderNumber` > '.$this->valueWrapper->wrapValue('integer', $this->from).' 
            ;
        ';
        
        $affectedRows = $this->database->update($query);
        
        return  $affectedRows;

        
    }

    
}