<?php
namespace Application\api\rest\operation;
class Copy implements IRun {
    
    private $queryFactory;
    private $valueFactory;
    private $table;
    private $from;
    private $to;
    
    public function __construct(
        query\IFactory $queryFactory,
        value\Factory $valueFactory,
        table\ITable $table,
        $from, 
        $to
    ) {
        
        $this->queryFactory = $queryFactory;
        $this->valueFactory = $valueFactory;
        $this->table = $table;
        $this->from = $from;
        $this->to = $to; // meaningful when reodering

    }
    
    public function run() {

        if ($this->table->isTree()) { // top node
            
            $row = $this->queryFactory
                ->select()
                ->byId($this->table, $this->from)
                ->query()
            ;
            
            $to = $this->queryFactory
                ->insert()
                ->row($this->table)
                ->query()
            ;
        
            $this->copyTreeRow($this->table, $this->from, $to, $row['parentId']);
            
            $this->copy($this->table, $this->from, $this->to);
            
        } else {
            
            $this->copy($this->table, $this->from, $this->to);
            
        }
        
        $row = $this->queryFactory
            ->select()
            ->byId($this->table, $this->to)
            ->query()
        ;
        
        return $row;
        
    }
    
    private function copy($table, $from, $to) { // next table may be a tree or a plain one, so cascading calls this method again and again

        if ($table->isTree()) {
            
            $this->cascade($table, $from, $to);

            $this->cascadeAspects($table, $from, $to);
            
            $this->tree($table, $from, $to);
            
        } else {
            
            $this->copyRow($table, $from, $to);

            $this->cascade($table, $from, $to);

            $this->cascadeAspects($table, $from, $to);
            
        }
        
    }
    
    private function tree(table\ITable $table, $fromParentId, $toParentId) {

        $childRows = $this->queryFactory // TODO: doubled query
            ->select()
            ->children($table, $fromParentId)
            ->query()
        ;

        foreach ($childRows as $index => $childRow) {
            
            $fromChildId = $childRow['id'];

            $toChildId = $this->queryFactory
                ->insert()
                ->row($table)
                ->query()
            ;
            
            $this->copyTreeRow($table, $fromChildId, $toChildId, $toParentId);

            $this->copy($table, $fromChildId, $toChildId);
            
        }
        
    }
    
    private function cascadeAspects(table\ITable $masterTable, $fromMasterId, $toMasterId) {

        foreach ($masterTable->getDetailAspects() as $detailAspect) {
            
            $rows = $this->queryFactory
                ->select()
                ->aspect($detailAspect, $fromMasterId)
                ->query()
            ;
            
            $detailTable = $detailAspect->getDetailTable();

            foreach ($rows as $row) {
                
                $fromDetailId = $row['id'];
                
                $toDetailId = $this->queryFactory
                    ->insert()
                    ->row($detailTable)
                    ->query()
                ;
                
                $this->copyRow($detailTable, $fromDetailId, $toDetailId);
                
                $this->copyAspectGlueRow($detailAspect, $toDetailId, $toMasterId);
                
                $this->copy($detailTable, $fromDetailId, $toDetailId);
                
            }
            
        }

    }
    
    private function copyAspectGlueRow(table\IAspect $aspect, $detailId, $masterId) {

        $id = $this->queryFactory
            ->insert()
            ->aspect($aspect, $masterId, $detailId)
            ->query()
        ;
        
        if (empty($id)) {
            throw new \Exception('Copying glue row failed');
        }
        
    }
    
    private function cascade(table\ITable $masterTable, $fromMasterId, $toMasterId) {

        foreach ($masterTable->getDetailTables() as $detailTable) {
            
            $rows = $this->queryFactory
                ->select()
                ->byMasterId($detailTable, $fromMasterId)
                ->query()
            ;

            foreach ($rows as $row) {

                $fromDetailId = $row['id'];
                
                $toDetailId = $this->queryFactory
                    ->insert()
                    ->row($detailTable)
                    ->query()
                ;

                $this->copyRow($detailTable, $fromDetailId, $toDetailId);
                
                $this->copyGlueRow($detailTable, $toDetailId, $toMasterId);

                $this->copy($detailTable, $fromDetailId, $toDetailId);

            }
            
        }
        
    }
    
    private function copyGlueRow(table\ITable $detailTable, $detailId, $masterId) {

        $id = $this->queryFactory
            ->insert()
            ->glue($detailTable, $masterId, $detailId)
            ->query()
        ;
        
        if (empty($id)) {
            throw new \Exception('Copying glue row failed');
        }
        
    }
    
    private function copyRow(table\ITable $table, $from, $to) {

        $row = $this->queryFactory
            ->select()
            ->byId($table, $from)
            ->query()
        ;

        $values = $this->valueFactory->rowToValues($table, $row);

        $id = $this->queryFactory
            ->update()
            ->byId($table, $to, $values)
            ->query()
        ;
        
        if (empty($id)) {
            throw new \Exception('Copying row failed');
        }
        
        $this->reorder($table, $to);

    }
    
    private function copyTreeRow(table\ITable $table, $from, $to, $parentId) {

        $row = $this->queryFactory
            ->select()
            ->byId($table, $from)
            ->query()
        ;
        
        $row['parentId'] = $parentId;

        $values = $this->valueFactory->rowToValues($table, $row);

        $id = $this->queryFactory
            ->update()
            ->byId($table, $to, $values)
            ->query()
        ;
        
        if (empty($id)) {
            throw new \Exception('Copying row failed');
        }
        
        $this->reorder($table, $to);

    }
    
    private function reorder(table\ITable $table, $id) {
        
        $maximum = $this->queryFactory
            ->select()
            ->maximumOrderNumber($table)
            ->query()
        ;
        
        $affectedRows = $this->queryFactory
            ->update()
            ->orderNumber($table, $maximum+1, $id)
            ->query()
        ;
        
        if ($affectedRows !== 1) {
            throw new \Exception('Reodering failed when copiyng');
        }
        
    }
    
}