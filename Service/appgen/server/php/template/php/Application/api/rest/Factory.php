<?php
namespace Application\api\rest;
class Factory implements IFactory {
    
    private $operationFactory;
    
    public function __construct($operationFactory) {
        
        $this->operationFactory = $operationFactory;
        
    }
    
    public  function make(IRequest $request, IResponse $response) {

        $httpMethod = $request->getHttpMethod();
        $httpClass = __NAMESPACE__.'\\'.ucfirst(strtolower($httpMethod));
        $http = new $httpClass($this->operationFactory, $request, $response);
        
        return $http;

    }
    
}