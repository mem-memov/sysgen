<?php
namespace Application\api\rest\operation\query\select;
interface IFactory {
    
    public function byOrderNumber(\Application\api\rest\operation\table\ITable $table, $orderNumber);
    public function maximumOrderNumber(\Application\api\rest\operation\table\ITable $table);
    public function count(\Application\api\rest\operation\table\ITable $table);
    public function range(\Application\api\rest\operation\table\ITable $table);
    public function byId(\Application\api\rest\operation\table\ITable $table, $id);
    public function byMasterId(\Application\api\rest\operation\table\ITable $detailTable, $masterId);
    public function children(\Application\api\rest\operation\table\ITable $table, $parentId);
    public function countChildren(\Application\api\rest\operation\table\ITable $table, $parentId);
    public function hasChildren(\Application\api\rest\operation\table\ITable $table, $parentId);
    
}