<?php
namespace Application\api\rest\operation\table;
/**
 * Value Object: http://martinfowler.com/eaaCatalog/valueObject.html
 */
class Table implements ITable{
    
    private $tablePrefix;
    private $name;
    private $fields;
    private $isTree;
    private $rootId;
    private $page;
    private $filters;
    private $sorters;
    private $group;
    private $masterGlue;
    private $detailTables;
    private $references;
    private $masterAspects;
    private $detailAspects;
    
    public function __construct(
        $tablePrefix,
        $name, 
        array $fields,
        $isTree
    ) {

        $this->tablePrefix = $tablePrefix;
        $this->name = $name;
        $this->isTree = $isTree;
        $this->rootId = 0;
        
        $this->fields = array();
        foreach ($fields as $field) {
            if (!($field instanceof IField)) {
                throw new \Exception('Wrong field interface');
            }
            $this->fields[$field->getName()] = $field;
        }

        $this->filters = array();
        $this->sorters = array();
        $this->group = null;
        $this->masterGlue = null;
        $this->detailTables = array();
        $this->references = array();
        $this->masterAspects = array();
        $this->detailAspects = array();
        
    }
    
    public function setPage(IPage $page) {
        
        $this->page = $page;
        
    }
    
    public function setFilters(array $filters) {
        
        $this->filters = array();
        foreach ($filters as $filter) {
            if (!($filter instanceof IFilter)) {
                throw new \Exception('Wrong filter interface');
            }
            $this->filters[] = $filter;
        }
        
    }
    
    public function setSorters(array $sorters) {

        $this->sorters = array();
        foreach ($sorters as $sorter) {
            if (!($sorter instanceof ISorter)) {
                throw new \Exception('Wrong sorter interface');
            }
            $this->sorters[] = $sorter;
        }
        
    }
    
    public function setGroup(IGroup $group) {
        
        $this->group = $group;
        
    }

    public function setMasterGlue(IGlue $masterGlue) {
        
        $this->masterGlue = $masterGlue;
        
    }
    
    public function addDetailTable(ITable $detalTable) {
        
        $this->detailTables[] = $detalTable;
        
    }
    
    public function addReference(IReference $reference) {
        
        $this->references[] = $reference;
        
    }

    public function addDetailAspect(IAspect $aspect) {

        $this->detailAspects[] = $aspect;
        
    }
    
    public function addMasterAspect(IAspect $aspect) {

        $this->masterAspects[] = $aspect;
        
    }
    
    public function setRootId($rootId) {
        
        $this->rootId = $rootId;
        
    }

    public function getName() {
        return $this->name;
    }
    
    public function name() {
        return $this->tablePrefix.$this->name;
    }

    public function getPage() {
        return $this->page;
    }

    public function getFields() {
        return $this->fields;
    }
    
    public function isTree() {
        return $this->isTree;
    }

    public function getFilters() {
        return $this->filters;
    }

    public function getSorters() {
        return $this->sorters;
    }

    public function getGroup() {
        return $this->group;
    }
    
    public function getMasterGlue() {
        return $this->masterGlue;
    }
    
    public function getReferences() {
        return $this->references;
    }
    
    public function getDetailAspects() {
        return $this->detailAspects;
    }
    
    public function getMasterAspects() {
        return $this->masterAspects;
    }
    
    public function getDetailTables() {
        return $this->detailTables;
    }
    
    public function getRootId() {
        
        return $this->rootId;
        
    }
    
}