<?php
namespace Application\api\rest\operation\hook;
interface IFactory {
    
    public function fetch($table);

}