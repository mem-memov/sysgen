<?php
namespace Application\api\rest\operation\table;
interface IReference {
    
    /**
     * @return \Application\api\rest\operation\table\IField
     */
    public function getContainerField();
    /**
     * @return \Application\api\rest\operation\table\IField[]
     */
    public function getContentFeilds();
    /**
     * @return \Application\api\rest\operation\table\ITable
     */
    public function getContentTable();
    
}