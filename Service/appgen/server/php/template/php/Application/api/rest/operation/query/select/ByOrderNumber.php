<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ByOrderNumber implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $orderNumber;
    
    public function __construct($database, $valueWrapper, $table, $orderNumber) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->orderNumber = $orderNumber;
        
        
    }
    
    public function query() {
        
        $query = '
            SELECT 
                *
            FROM 
                `'.$this->table.'` 
            WHERE
                `orderNumber` = '.$this->valueWrapper->wrapValue('integer', $this->orderNumber).'
            ;
        ';
        
        $rows = $this->database->select($query);
        
        if (empty($rows)) {
            return null;
        }
        
        return $rows[0];
        
    }

    
}