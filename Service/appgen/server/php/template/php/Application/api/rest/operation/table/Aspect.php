<?php
namespace Application\api\rest\operation\table;
class Aspect implements IAspect {
    
    private $masterTable;
    private $masterField;
    private $detailTable;

    
    public function __construct( 
        ITable $masterTable,
        IField $masterField,
        ITable $detailTable
    ) {

        $this->masterTable = $masterTable;
        $this->masterField = $masterField;
        $this->detailTable = $detailTable;
        
    }
    
    public function table() {
        return $this->masterTable->name() . '_' . $this->masterField->getName() . '_' . $this->detailTable->getName();
    }
    
    public function detail() {
        return $this->detailTable->getName();
    }
    
    public function master() {
        return $this->masterTable->getName();
    }
    
    public function field() {
        return $this->masterField->getName();
    }
    
    public function getDetailTable() {
        return $this->detailTable;
    }

    
}