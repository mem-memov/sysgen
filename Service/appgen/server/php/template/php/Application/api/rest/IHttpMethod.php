<?php
namespace Application\api\rest;
interface IHttpMethod {
    
    public  function response();
    
}