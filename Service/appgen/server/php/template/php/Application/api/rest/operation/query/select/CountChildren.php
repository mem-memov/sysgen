<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class CountChildren {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $joinStatement;
    private $conditionStatement;
    private $parentId;
    
    public function __construct(
        $database, 
        $valueWrapper,
        $table, 
        $joinStatement, 
        $conditionStatement,
        $parentId
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->joinStatement = $joinStatement;
        $this->conditionStatement = $conditionStatement;
        $this->parentId = $parentId;

    }
    
    public function query() {
        
        $condition = $this->conditionStatement->state();
        $parentCondition = ' `'.$this->table.'`.`parentId` = '.$this->valueWrapper->wrapValue('integer', $this->parentId).' ';
        if (empty($condition)) {
            $condition = 'WHERE '.$parentCondition;
        } else {
            $condition = $condition .' AND '.$parentCondition;
        }
        
        $query = '
            SELECT 
                COUNT(*) AS `totalCount` 
            FROM 
                `'.$this->table.'` 
            '.$this->joinStatement->state().'
            '.$condition.' 
            ;
        ';

        $rows = $this->database->select($query);
        $count = $rows[0]['totalCount'];
        
        return $count;
        
    }
    
}