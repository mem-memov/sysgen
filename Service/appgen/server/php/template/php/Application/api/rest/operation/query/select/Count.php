<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class Count {
    
    private $database;
    private $table;
    private $joinStatement;
    private $conditionStatement;

    
    public function __construct(
        $database, 
        $table, 
        $joinStatement, 
        $conditionStatement
    ) {
        
        $this->database = $database;
        $this->table = $table;
        $this->joinStatement = $joinStatement;
        $this->conditionStatement = $conditionStatement;

    }
    
    public function query() {
        
        $query = '
            SELECT 
                COUNT(*) AS `totalCount` 
            FROM 
                `'.$this->table.'` 
            '.$this->joinStatement->state().'
            '.$this->conditionStatement->state().' 
            ;
        ';

        $rows = $this->database->select($query);
        $count = $rows[0]['totalCount'];
        
        return $count;
        
    }
    
}