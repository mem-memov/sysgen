<?php
namespace Application\api\rest;
class Request implements IRequest {
    
    private $rawContent;
    
    public function __construct() {
        
        $this->rawContent = null;
        
    }
    
    public function getHttpMethod() {
        
        return $_SERVER['REQUEST_METHOD'];
        
    }
    
    public function getApiPathParts() {
        
        $apiMarker = 'api';
        $uriPath = rtrim(parse_url($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
        $uriParts = explode('/', $uriPath);
        $pathParts = array();
        $markerFound = false;
        foreach ($uriParts as $uriPart) {
            if ($markerFound) {
                $pathParts[] = $uriPart;
            }
            if ($uriPart === $apiMarker) {
                $markerFound = true;
            }
        }
        
        return $pathParts;
        
    }
    
    public function checkGet($key) {
        
        return array_key_exists($key, $_GET);
        
    }
    
    public function readGet($key) {
        
        if (!$this->checkGet($key)) {
            throw new \Exception('No required GET parameter '.$key);
        }
        
        return $_GET[$key];
        
    }
    
    public function checkPost($key) {
        
        return array_key_exists($key, $_POST);
        
    }
    
    public function readPost($key) {
        
        if (!$this->checkPost($key)) {
            throw new \Exception('No required POST parameter '.$key);
        }
        
        return $_POST[$key];
        
    }
    
    public function readRaw() {
        
        if (is_null($this->rawContent)) {
            $this->rawContent = file_get_contents('php://input');
        }
        return $this->rawContent;
        
    }
    
}