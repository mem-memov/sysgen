<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class Children {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $fieldStatement;
    private $conditionStatement;
    private $groupStatement;
    private $orderStatement;
    private $joinStatement;
    private $limitStatement;
    private $parentId;
    private $types;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $table,
        $fieldStatement, 
        $conditionStatement, 
        $groupStatement, 
        $orderStatement, 
        $joinStatement, 
        $limitStatement,
        $parentId,
        array $types
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->fieldStatement = $fieldStatement;
        $this->conditionStatement = $conditionStatement;
        $this->groupStatement = $groupStatement;
        $this->orderStatement = $orderStatement;
        $this->joinStatement = $joinStatement;
        $this->limitStatement = $limitStatement;
        $this->parentId = $parentId;
        $this->types = $types;
        
    }
    
    public function query() {
        
        $condition = $this->conditionStatement->state();
        $parentCondition = ' `'.$this->table.'`.`parentId` = '.$this->valueWrapper->wrapValue('integer', $this->parentId).' ';
        if (empty($condition)) {
            $condition = 'WHERE '.$parentCondition;
        } else {
            $condition = $condition .' AND '.$parentCondition;
        }
        
        $query = '
            SELECT 
                '.$this->fieldStatement->state().'
            FROM  
                `'.$this->table.'` 
            '.$this->joinStatement->state().'
            '.$condition.'
            '.$this->groupStatement->state().'
            '.$this->orderStatement->state().' 
            '.$this->limitStatement->state().' 
            ;
        ';

        $rows = $this->database->select($query, $this->types);
        
        return  $rows;
        
    }
    
}