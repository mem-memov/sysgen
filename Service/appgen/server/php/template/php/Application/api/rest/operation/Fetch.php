<?php
namespace Application\api\rest\operation;
/**
 * This class fetches the whole data structure on an entity
 */
class Fetch implements IRun {
    
    private $queryFactory;
    private $hook;
    private $cache;
    private $table;
    private $id;
    
    public function __construct(
        query\IFactory $queryFactory, 
        hook\IHook $hook,
        \Application\cache\ICache $cache,
        table\ITable $table,
        $id
    ) {
        
        $this->queryFactory = $queryFactory;
        $this->hook = $hook;
        $this->cache = $cache;
        $this->table = $table;
        $this->id = $id;

    }
    
    public function run() {
        
        $this->hook->before();
        
        $key = $this->table->getName().'_'.$this->id;
        
        $result = $this->cache->get($key);
        
        if (is_null($result)) {
            
            $result = $this->result();
            
            $this->cache->set($key, $result);
            
        }

        return $this->hook->after($result);
 
    }
    
    private function result() {
        
        if ($this->table->isTree()) {

            $rootId = $this->table->getRootId();

            $root = $this->queryFactory
                ->select()
                ->byId($this->table, $rootId)
                ->query()
            ;

            if (is_null($root)) { // root id = 0
                $root = array();
            }

            $root = array_merge($root, $this->cascade($this->table, $rootId));
            $root = array_merge($root, $this->cascadeAspects($this->table, $rootId));

            //$root['root'] = true;
            $root['expanded'] = true;

            $result = array_merge($root, $this->fetch($this->table, $rootId));

        } else {

            $result = $this->fetch($this->table, $this->id);

        }

        return $result;
        
    }
    
    private function fetch(table\ITable $table, $id) {
        
        if ($table->isTree()) {

            return array('children' => $this->tree($table, $id));

        } else {
            
            return $this->plain($table, $id);
            
        }
        
    }
    
    private function plain(table\ITable $table, $id) {
   
        $row = $this->queryFactory
            ->select()
            ->byId($table, $id)
            ->query()
        ; 

        $row = array_merge($row, $this->cascade($table, $id));
        $row = array_merge($row, $this->cascadeAspects($table, $id));
        
        return $row;
        
    }
    
    private function tree(table\ITable $table, $parentId) {
        
        $rows = $this->queryFactory
            ->select()
            ->children($table, $parentId)
            ->query()
        ;
        
        foreach ($rows as $index => $row) {

            $details = $this->cascade($table, $row['id']);
            $row = array_merge($row, $details);

            $aspects = $this->cascadeAspects($table, $row['id']);
            $row = array_merge($row, $aspects);

            $children = $this->tree($table, $row['id']);

            if (empty($children)) {
                $row['leaf'] = true;
            } else {
                $row['leaf'] = false;
                $row['expanded'] = true;
                $row['children'] = $children;
            }
            
            $rows[$index] = $row;

        }
        
        return $rows;
        
    }
    
    private function cascade(table\ITable $masterTable, $masterId) {

        $result = array();
        
        foreach ($masterTable->getDetailTables() as $detailTable) {

            $rows = $this->queryFactory
                ->select()
                ->byMasterId($detailTable, $masterId)
                ->query()
            ;

            foreach ($rows as $row) { 

                $result[$detailTable->getName()][] = array_merge($row, $this->fetch($detailTable, $row['id']));

            }
            
        }

        return $result;

    }
    
    private function cascadeAspects(table\ITable $masterTable, $masterId) {

        $result = array();
        
        foreach ($masterTable->getDetailAspects() as $detailAspect) {
            
            $rows = $this->queryFactory
                ->select()
                ->aspect($detailAspect, $masterId)
                ->query()
            ;
            
            $detailTable = $detailAspect->getDetailTable();

            foreach ($rows as $row) {
                
                $column = $detailAspect->field().'.'.$detailAspect->detail();
                $result[$column][] = array_merge($row, $this->fetch($detailTable, $row['id']));
                
            }
            
        }

        return $result;

    }
    
}