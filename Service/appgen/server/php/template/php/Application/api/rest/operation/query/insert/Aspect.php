<?php
namespace Application\api\rest\operation\query\insert;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class Aspect implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $masterField;
    private $detailField;
    private $glueTable;
    private $masterId;
    private $detailId;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $masterField, 
        $detailField, 
        $glueTable, 
        $masterId, 
        $detailId
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->masterField = $masterField; 
        $this->detailField = $detailField; 
        $this->glueTable = $glueTable; 
        $this->masterId = $masterId; 
        $this->detailId = $detailId;
        
    }
    
    public function query() {
        
        $query = '
            INSERT INTO  
                `'.$this->glueTable.'` 
            SET 
                `'.$this->masterField.'` = '.$this->valueWrapper->wrapValue('integer', $this->masterId).',
                `'.$this->detailField.'` = '.$this->valueWrapper->wrapValue('integer', $this->detailId).'
            ;
        ';

        $id = $this->database->insert($query);

        return $id;
        
    }

    
}