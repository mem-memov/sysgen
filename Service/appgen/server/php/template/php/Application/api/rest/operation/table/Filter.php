<?php
namespace Application\api\rest\operation\table;
class Filter implements IFilter{
    
    private $table;
    private $field;
    private $operator;
    private $value;
    private $type;
    
    public function __construct($table, $field, $operator, $value, $type) {
      
        $this->table = $table;
        $this->field = $field;
        $this->operator = $operator;
        $this->value = $value;
        $this->type = $type;
        
    }
    
    public function getTable() {
        return $this->table;
    }
    
    public function getField() {
        return $this->field;
    }
    
    public function getOperator() {
        return $this->operator;
    }
    
    public function getValue() {
        return $this->value;
    }
    
    public function getType() {
        return $this->type;
    }
    
}