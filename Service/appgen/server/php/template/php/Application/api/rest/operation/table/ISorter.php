<?php
namespace Application\api\rest\operation\table;
interface ISorter {

    public function isAscending();
    public function isDescending();
    
}