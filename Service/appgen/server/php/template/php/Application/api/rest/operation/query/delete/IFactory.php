<?php
namespace Application\api\rest\operation\query\delete;
interface IFactory {
    
    public function byId(\Application\api\rest\operation\table\ITable $table, $id);
    public function children(\Application\api\rest\operation\table\ITable $treeTable, $parentId);
    public function aspect(\Application\api\rest\operation\table\IAspect $aspect, $masterId);
    public function byMasterId(\Application\api\rest\operation\table\ITable $detailTable, $masterId);

}