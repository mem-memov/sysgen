<?php
namespace Application\api\rest\operation\query\select;
class FieldStatement implements IState {

    private $table;
    
    public function __construct(\Application\api\rest\operation\table\ITable $table) {

        $this->table = $table;
 
    }
    
    public function state() {
        
        if (empty($this->table->getFields())) {
            return '*';
        }

        $statement = '';
        
        $parts = array();
        
        foreach ($this->table->getFields() as $field) { // below overriding with keys is intentional
            
            $parts[$field->getName()] = '`'.$this->table->name().'`.`'.$field->getName().'`';
            
        }

        $masterGlue = $this->table->getMasterGlue();
        
        if (!is_null($masterGlue)) {
            $parts[$masterGlue->master()] = '`'.$masterGlue->table().'`.`'.$masterGlue->master().'`'; // join needed here
        }
        
        $aspects = $this->table->getMasterAspects();

        foreach ($aspects as $aspect) {
            $aspectField = $aspect->master().'.'.$aspect->field();
            $parts[$aspectField] = '`'.$aspect->table().'`.`'.$aspect->master().'` AS `'.$aspectField.'`'; // join needed here
        }
        
        foreach ($this->table->getReferences() as $reference) {
            
            $contentTable = $reference->getContentTable();
            $containerField = $reference->getContainerField();
            foreach ($reference->getContentFeilds() as $contentField) {
                $parts[] = '`'.$contentTable->name().'`.`'.$contentField->getName().'` AS `'.$containerField->getName().'.'.$contentField->getName().'`'; // join needed here
            }
            
        }
        


        $statement .= implode(', ', array_values($parts));
        
        return $statement;
        
    }
    
}