<?php
namespace Application\api\rest\operation\query\delete;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class Aspect implements IQuery {
    
    private $byMasterIdInDetailTable;
    private $byMasterIdInGlueTable;
    
    public function __construct($byMasterIdInDetailTable, $byMasterIdInGlueTable) {
        
        $this->byMasterIdInDetailTable = $byMasterIdInDetailTable;
        $this->byMasterIdInGlueTable = $byMasterIdInGlueTable;
        
    }
    
    public function query() {
        
        $this->byMasterIdInDetailTable->query();
        $this->byMasterIdInGlueTable->query();
        
    }
    
}