<?php
namespace Application\api\rest\operation\value;
interface IFactory {
    
    public function requestToValues(\Application\api\rest\IRequest $request);
    public function rowToValues(\Application\api\rest\operation\table\ITable $table, array $row);

}