<?php
namespace Application\api\rest;
class Get implements IHttpMethod {
    
    private $operationFactory;
    private $request;
    private $response;
    
    public function __construct($operationFactory, IRequest $request, IResponse $response) {
        
        $this->operationFactory = $operationFactory;
        $this->request = $request;
        $this->response = $response;
        
    }
    
    public  function response() {

        $pathParts = $this->request->getApiPathParts();
        if (count($pathParts) == 2 && $this->request->checkGet('cluster')) {
            return $this->responseOne();
        }

        $result = $this->operationFactory->filter($this->request)->run();

        $this->response->sendData(array(
            'totalCount' => $result[0],
            'data' => $result[1]
        ));
       
    }

    private function responseOne() {
        
        $row = $this->operationFactory->fetch($this->request)->run();
        
        if (is_null($row)) {
            $this->response->sendError('Row has been deleted');
        }
        
        $this->response->sendData(array(
            'success' => true,
            'data' => $row
        ));
        
    }
    
}