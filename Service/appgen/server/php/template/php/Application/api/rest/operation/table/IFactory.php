<?php
namespace Application\api\rest\operation\table;
interface IFactory {
    
    /**
     * @return \Application\api\rest\operation\table\ITable
     */
    public function requestToTable(\Application\api\rest\IRequest $request);

}