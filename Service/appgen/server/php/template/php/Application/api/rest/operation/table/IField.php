<?php
namespace Application\api\rest\operation\table;
interface IField {

    public function getName();
    public function getType();
    
}