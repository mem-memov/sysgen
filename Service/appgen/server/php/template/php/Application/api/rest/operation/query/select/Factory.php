<?php
namespace Application\api\rest\operation\query\select;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {
    
    private $database;
    private $valueWrapper;
    private $api;
    
    public function __construct($database, $valueWrapper, array $api) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->api = $api;
        
    }
    
    public function byOrderNumber(\Application\api\rest\operation\table\ITable $table, $orderNumber) {
        
        return new ByOrderNumber(
            $this->database, 
            $this->valueWrapper,
            $table->name(), 
            $orderNumber
        );
        
    }
    
    public function maximumOrderNumber(\Application\api\rest\operation\table\ITable $table) {
        
        return new MaximumOrderNumber(
            $this->database, 
            $table->name()
        );
        
    }
    
    public function count(\Application\api\rest\operation\table\ITable $table) {
        
        return new Count(
            $this->database, 
            $table->name(), 
            new JoinStatement($table),
            new ConditionStatement($this->valueWrapper, $table)
        );
        
    }

    public function range(\Application\api\rest\operation\table\ITable $table) {

        return new Range(
            $this->database, 
            $table->name(), 
            new FieldStatement($table), 
            new ConditionStatement($this->valueWrapper, $table), 
            new GroupStatement($this->valueWrapper, $table), 
            new OrderStatement($table), 
            new JoinStatement($table), 
            new LimitStatement($this->valueWrapper, $table),
            $this->api[$table->getName()]['fields']
        );
        
    }

    public function byId(\Application\api\rest\operation\table\ITable $table, $id) {
        
        return new ById(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            new FieldStatement($table), 
            new JoinStatement($table),
            $id,
            $this->api[$table->getName()]['fields']
        );
        
    }

    public function byMasterId(\Application\api\rest\operation\table\ITable $detailTable, $masterId) {
        
        return new ByMasterId(
            $this->database, 
            $this->valueWrapper, 
            $detailTable->name(), 
            new FieldStatement($detailTable),
            new JoinStatement($detailTable),
            $detailTable->getMasterGlue()->table(), 
            $detailTable->getMasterGlue()->master(), 
            $masterId,
            $this->api[$detailTable->getName()]['fields']
        );

    }
    
    public function aspect(\Application\api\rest\operation\table\IAspect $aspect, $masterId) {
        
        $detailTable = $aspect->getDetailTable();
        
        return new Aspect(
            $this->database, 
            $this->valueWrapper, 
            $detailTable->name(), 
            new FieldStatement($detailTable),
            new JoinStatement($detailTable),
            $aspect->table(), 
            $aspect->master(), 
            $masterId,
            $this->api[$detailTable->getName()]['fields']
        );

    }
    
    public function children(\Application\api\rest\operation\table\ITable $table, $parentId) {
        
        return new Children(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            new FieldStatement($table), 
            new ConditionStatement($this->valueWrapper, $table), 
            new GroupStatement($this->valueWrapper, $table), 
            new OrderStatement($table), 
            new JoinStatement($table), 
            new LimitStatement($this->valueWrapper, $table),
            $parentId,
            $this->api[$table->getName()]['fields']
        );
        
    }
    
    public function countChildren(\Application\api\rest\operation\table\ITable $table, $parentId) {
        
        return new CountChildren(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            new JoinStatement($table),
            new ConditionStatement($this->valueWrapper, $table),
            $parentId
        );
        
    }

    public function hasChildren(\Application\api\rest\operation\table\ITable $table, $parentId) {
        
        return new HasChildren(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $parentId
        );
        
    }

    
}