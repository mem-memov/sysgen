<?php
namespace Application\api\rest\operation\table;
interface IAspect {

    public function table();
    public function detail();
    public function master();
    public function field();
    /**
     * @return \Application\api\rest\operation\table\ITable
     */
    public function getDetailTable();
    
}