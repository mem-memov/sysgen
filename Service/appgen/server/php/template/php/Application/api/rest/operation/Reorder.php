<?php
namespace Application\api\rest\operation;
class Reorder implements IRun {
    
    private $queryFactory;
    private $table;
    private $id;
    private $from;
    private $to;
    
    public function __construct(
        query\IFactory $queryFactory, 
        table\ITable $table,
        $id,
        $from,
        $to
    ) {
        
        $this->queryFactory = $queryFactory;
        $this->table = $table;
        $this->id = $id;
        $this->from = $from;
        $this->to = $to;

    }
    
    public function run() {
        
        if ($this->to > 0) {
            $ok = $this->reorderAfter();
        } else {
            $this->to = -$this->to;
            $ok = $this->reorderBefore();
        }
        
        // TODO: Copy from previeos version 
        $targetValues = $this->queryFactory->select()->byOrderNumber($this->table, $this->to)->query();
        
        if (is_null($targetValues)) {
            return;
        }
        
        $targetId = $targetValues['id'];
        
        $this->updateOrderNumber($this->id, $this->to);
        
        $this->updateOrderNumber($targetId, $this->from);
                
    }
    
    private function reorderAfter() {
        
        if ($this->from > $this->to) {
            return $this->queryFactory->update()->reorderAfterDown(
                $this->table, 
                $this->id, 
                $this->from, 
                $this->to
            )->query();
            $this->updateOrderNumber($this->id, $this->to+1);
        } else if ($this->from < $this->to) {
            return $this->queryFactory->update()->reorderAfterUp(
                $this->table, 
                $this->id, 
                $this->from, 
                $this->to
            )->query();
            $this->updateOrderNumber($this->id, $this->to+1);
        } else {
            throw new \Exception('Reordering record with itself');
        }
        
    }
    
    private function reorderBefore() {
        
        if ($this->from > $this->to) {
            return $this->queryFactory->update()->reorderBeforeDown(
                $this->table, 
                $this->id, 
                $this->from, 
                $this->to
            )->query();
            $this->updateOrderNumber($this->id, $this->to-1);
        } else if ($this->from < $this->to) {
            return $this->queryFactory->update()->reorderBeforeUp(
                $this->table, 
                $this->id, 
                $this->from, 
                $this->to
            )->query();
            $this->updateOrderNumber($this->id, $this->to-1);
        } else {
            throw new \Exception('Reordering record with itself');
        }
        
    }
    
    private function updateOrderNumber($id, $orderNumber) {
        
        $affectedRows = $this->queryFactory->update()->orderNumber(
            $this->table, 
            $orderNumber,
            $id
        )->query();
        
        if ($affectedRows !== 1) {
            throw new \Exception('Impossible to write orderNumber');
        }
        
    }
    
}