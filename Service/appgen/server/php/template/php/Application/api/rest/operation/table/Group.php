<?php
namespace Application\api\rest\operation\table;
class Group implements IGroup{
    
    private $table;
    private $field;
    private $isDescending;
    private $isAscending;
    
    public function __construct($table, $field, $direction = 'ASC') {
        
        $this->table = $table;
        $this->field = $field;
        
        $direction = strtoupper($direction);
        switch ($direction) {
            case 'ASC':
                $this->isDescending = false;
                $this->isAscending = true;
                break;
            case 'DESC':
                $this->isDescending = true;
                $this->isAscending = false;
                break;
            default:
                throw new \Exception('Unknown sort direction: '.$direction);
                break;
        }
        
    }
    
    public function getTable() {
        return $this->table;
    }
    
    public function getField() {
        return $this->field;
    }
    
    public function isAscending() {
        return $this->isAscending;
    }
    
    public function isDescending() {
        return $this->isDescending;
    }
    
}