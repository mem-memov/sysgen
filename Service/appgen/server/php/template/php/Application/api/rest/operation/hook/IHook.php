<?php
namespace Application\api\rest\operation\hook;
interface IHook {

    public function before();
    public function after($result);
    
}