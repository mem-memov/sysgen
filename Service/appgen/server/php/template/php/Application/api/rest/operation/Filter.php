<?php
namespace Application\api\rest\operation;
/**
 * The class provides a list of items whithout digging deep into the structure of connected tables
 */
class Filter implements IRun {
    
    private $queryFactory;
    private $table;
    
    public function __construct(
        query\IFactory $queryFactory, 
        table\ITable $table
    ) {
        
        $this->queryFactory = $queryFactory;
        $this->table = $table;

    }
    
    public function run() {
        
        if ($this->table->isTree()) {
            return $this->tree();
        } else {
            return $this->plain();
        }

    }
    
    private function plain() {
        
        $rows = $this->queryFactory
            ->select()
            ->range($this->table)
            ->query()
        ;
        
        $totalCount = $this->queryFactory
            ->select()
            ->count($this->table)
            ->query()
        ;
        
        return array($totalCount, $rows);
        
    }
    
    private function tree() {
        
        $rows = $this->queryFactory
            ->select()
            ->children($this->table, $this->table->getRootId())
            ->query()
        ;
        
        foreach ($rows as $index => $row) {
            $rows[$index]['leaf'] = $this->queryFactory
                ->select()
                ->hasChildren($this->table, $row['id'])
                ->query()
            ;
        }
        
        $totalCount = $this->queryFactory
            ->select()
            ->countChildren($this->table, $this->table->getRootId())
            ->query()
        ;
        
        return array($totalCount, array('children' => $rows));
        
    }
    
}