<?php
namespace Application\api\rest\operation\query\update;
interface IFactory {
    
    public function byId(\Application\api\rest\operation\table\ITable $table, $id, array $values);
    public function shiftUp(\Application\api\rest\operation\table\ITable $table, $orderNumber);
    public function shiftDown(\Application\api\rest\operation\table\ITable $table, $orderNumber);
    public function reorderAfterDown(\Application\api\rest\operation\table\ITable $table, $id, $from, $to);
    public function reorderAfterUp(\Application\api\rest\operation\table\ITable $table, $id, $from, $to);
    public function reorderBeforeDown(\Application\api\rest\operation\table\ITable $table, $id, $from, $to);
    public function reorderBeforeUp(\Application\api\rest\operation\table\ITable $table, $id, $from, $to);
    public function orderNumber(\Application\api\rest\operation\table\ITable $table, $orderNumber, $id);

}