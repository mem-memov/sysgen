<?php
namespace Application\api\rest\operation\query\insert;
interface IFactory {
    
    public function row(\Application\api\rest\operation\table\ITable $table);
    public function glue(\Application\api\rest\operation\table\ITable $table, $masterId, $detailId);

}