<?php
namespace Application\api\rest\operation;
class Create implements IRun {
    
    private $queryFactory;
    private $table;
    private $orderNumber;
    private $masterId;
    private $aspect;
    private $aspectId;
    
    public function __construct(
        query\IFactory $queryFactory, 
        table\ITable $table,
        $orderNumber,
        $masterId,
        table\IAspect $aspect = null,
        $aspectId = null
    ) {
        
        $this->queryFactory = $queryFactory;
        $this->table = $table;
        $this->orderNumber = $orderNumber;
        $this->masterId = $masterId;
        $this->aspect = $aspect;
        $this->aspectId = $aspectId;
        
    }
    
    public function run() {
        
        $id = $this->queryFactory
            ->insert()
            ->row($this->table)
            ->query()
        ;

        $this->updateOrderNumber($id);
        $this->glueToMasterTable($id);
        $this->attachToAspectMaster($id);
        
        $row = $this->queryFactory
            ->select()
            ->byId($this->table, $id)
            ->query()
        ;
        
        return $row;

        
    }
    
    private function updateOrderNumber($id) {
        
        if (is_null($this->orderNumber)) {
            
            $orderNumber = $this->queryFactory->select()->maximumOrderNumber($this->table)->query() + 1;
            
        } else {
            
            $targetRow = $this->queryFactory
                ->select()
                ->byOrderNumber($this->table, $this->orderNumber)
                ->query()
            ;
            
            if (is_null($targetRow)) {
                $orderNumber = $this->queryFactory->select()->maximumOrderNumber($this->table)->query() + 1;
            } else {
                $orderNumber = $targetRow['orderNumber'] + 1;
                $this->queryFactory->update()->shiftDown($this->table, $orderNumber);
            }
            
        }
        
        $affectedRows = $this->queryFactory->update()->orderNumber(
            $this->table, 
            $orderNumber,
            $id
        )->query();
        
        if ($affectedRows !== 1) {
            throw new \Exception('Impossible to write orderNumber');
        }
        
    }

    private function glueToMasterTable($detailId) {
        
        if (!is_null($this->table->getMasterGlue())) {
            $this->queryFactory
                ->insert()
                ->glue($this->table, $this->masterId, $detailId)
                ->query()
            ;
        }
        
    }

    private function attachToAspectMaster($detailId) {
        
        if (!is_null($this->aspect) && !is_null($this->aspectId)) {
            $this->queryFactory
                ->insert()
                ->aspect($this->aspect, $this->aspectId, $detailId)
                ->query()
            ;
        }
    }
    
}