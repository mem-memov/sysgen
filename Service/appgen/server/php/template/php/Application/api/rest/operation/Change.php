<?php
namespace Application\api\rest\operation;
class Change implements IRun {
    
    private $queryFactory;
    private $table;
    private $id;
    private $values;
    
    public function __construct(
        query\IFactory $queryFactory, 
        table\ITable $table,
        $id, 
        array $values
    ) {
        
        $this->queryFactory = $queryFactory;
        $this->table = $table;
        $this->id = $id;
        $this->values = $values;

    }
    
    public function run() {
        
        $result = $this->queryFactory
            ->update()
            ->byId(
                $this->table, 
                $this->id,
                $this->values
            )
            ->query()
        ;
        
        return $result;
        
    }
    
}