<?php
namespace Application\api\rest;
class Post implements IHttpMethod {
    
    private $operationFactory;
    private $request;
    private $response;
    
    public function __construct($operationFactory, IRequest $request, IResponse $response) {
        
        $this->operationFactory = $operationFactory;
        $this->request = $request;
        $this->response = $response;
        
    }
    
    public  function response() {
  
        $values = json_decode($this->request->readRaw(), true, 100, JSON_UNESCAPED_UNICODE);
       
        $row = $this->operationFactory->create($this->request)->run();
        
        if (is_null($row)) {
            $this->response->sendError('Row not created');
        }

        // copy
        if (isset($values['id']) && $values['id'] < 0) {
            $row = $this->operationFactory->copy($this->request, -$values['id'], $row['id'])->run();
        }

        $this->response->sendData(array(
            'success' => true,
            'data' => $row
        ));
        
    }

}