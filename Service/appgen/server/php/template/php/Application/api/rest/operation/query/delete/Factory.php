<?php
namespace Application\api\rest\operation\query\delete;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {
    
    private $database;
    private $valueWrapper;
    
    public function __construct($database, $valueWrapper) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        
    }
    
    public function byId(\Application\api\rest\operation\table\ITable $table, $id) {
        
        return new ById(
            $this->database, 
            $this->valueWrapper, 
            $table->name(), 
            $id
        );
        
    }
    
    public function children(\Application\api\rest\operation\table\ITable $treeTable, $parentId) {
        
        return new Children(
            $this->database, 
            $this->valueWrapper, 
            $treeTable->name(),
            $parentId
        );
        
    }
    
    public function aspect(\Application\api\rest\operation\table\IAspect $aspect, $masterId) {
        
        $byMasterIdInDetailTable = $this->byMasterIdInDetailTable(
            $aspect->table(),
            $aspect->getDetailTable()->name(),
            $aspect->detail(),
            $aspect->master(),
            $masterId
        );
        
        $byMasterIdInGlueTable = $this->byMasterIdInGlueTable(
            $aspect->table(),
            $aspect->master(),
            $masterId
        );
        
        return new Aspect($byMasterIdInDetailTable, $byMasterIdInGlueTable);
        
    }
    
    public function byMasterId(\Application\api\rest\operation\table\ITable $detailTable, $masterId) {
        
        $byMasterIdInDetailTable = $this->byMasterIdInDetailTable(
            $detailTable->getMasterGlue()->table(), 
            $detailTable->name(), 
            $detailTable->getMasterGlue()->detail(),
            $detailTable->getMasterGlue()->master(), 
            $masterId
        );
        
        $byMasterIdInGlueTable = $this->byMasterIdInGlueTable(
            $detailTable->getMasterGlue()->table(), 
            $detailTable->getMasterGlue()->master(), 
            $masterId
        );
        
        return new ByMasterId($byMasterIdInDetailTable, $byMasterIdInGlueTable);
        
    }
    
    private function byMasterIdInDetailTable($glueTable, $detailTable, $detailField, $masterField, $masterId) {
        
        return new ByMasterIdInDetailTable(
            $this->database, 
            $this->valueWrapper, 
            $glueTable, 
            $detailTable, 
            $detailField,
            $masterField, 
            $masterId
        );
        
    }
    
    private function byMasterIdInGlueTable($glueTable, $masterField, $masterId) {
        
        return new ByMasterIdInGlueTable(
            $this->database, 
            $this->valueWrapper, 
            $glueTable, 
            $masterField, 
            $masterId
        );
        
    }
    

    
}