<?php
namespace Application\api\rest\operation\query\delete;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class Children implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $parentId;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $table, 
        $parentId
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->parentId = $parentId;

    }
    
    public function query() {
        
        $query = '
            DELETE FROM  
                `'.$this->table.'` 
            WHERE 
                `'.$this->table.'`.`parentId` = '.$this->valueWrapper->wrapValue('integer', $this->parentId).' 
            ;
        ';

        $affectedRows = $this->database->delete($query);
        
    }
    
}