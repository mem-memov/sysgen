<?php
namespace Application\api\rest;
class Delete implements IHttpMethod {
    
    private $operationFactory;
    private $request;
    private $response;
    
    public function __construct($operationFactory, IRequest $request, IResponse $response) {
        
        $this->operationFactory = $operationFactory;
        $this->request = $request;
        $this->response = $response;
        
    }
    
    public  function response() {
        
        $pathParts = $this->request->getApiPathParts();
        
        if (count($pathParts) < 2) {
            $this->response->sendError('Api entity id unknown');
        }

        $this->operationFactory->remove($this->request)->run();

        $this->response->confirmSuccess();

        
    }
    
}