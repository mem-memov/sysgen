<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class MaximumOrderNumber {
    
    private $database;
    private $table;
    
    public function __construct($database, $table) {
        
        $this->database = $database;
        $this->table = $table;

    }
    
    public function query() {
        
        $query = '
            SELECT 
                MAX(`orderNumber`) AS `orderNumber` 
            FROM 
                `'.$this->table.'`
            ;
        ';

        $rows = $this->database->select($query);

        if (empty($rows)) {
            return 0;
        } else {
            return $rows[0]['orderNumber'];
        }
        
    }
    
}