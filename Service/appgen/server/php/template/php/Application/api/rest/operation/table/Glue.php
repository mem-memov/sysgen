<?php
namespace Application\api\rest\operation\table;
class Glue implements IGlue {
    
    private $masterTable;
    private $detailTable;
    
    public function __construct(ITable $masterTable, ITable $detailTable) {
        
        $this->masterTable = $masterTable;
        $this->detailTable = $detailTable;
        
    }
    
    public function table() {
        
        return $this->masterTable->name() . '_' . $this->detailTable->getName();
                
    }
    
    public function detail() {
        
        return $this->detailTable->getName();
        
    }
    
    public function master() {
        
        return $this->masterTable->getName();
        
    }
    
    public function isMasterField($field) {
        
        return $field == $this->masterTable->getName();
        
    }
    
}