<?php
namespace Application\api\rest;
class Response implements IResponse {
    
    public function __construct() {

    }
    
    public function sendError($message) {

        $output = array(
            'success' => false,
            'error' => $message
        );
        $json = json_encode($output);
        header('Content-Type: application/json');
        echo $json;
        exit();
        
    }
    
    public function sendData(array $output) {
        
        $output['success'] = true;
        $json = json_encode($output);
        header('Content-Type: application/json');
        echo $json;
        exit();
        
    }
    
    public function confirmSuccess() {

        $output = array(
            'success' => true
        );
        $json = json_encode($output);
        header('Content-Type: application/json');
        echo $json;
        exit();
        
    }
    
}