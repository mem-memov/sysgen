<?php
namespace Application\api\rest\operation\query\delete;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ByMasterIdInGlueTable implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $glueTable;
    private $masterField;
    private $masterId;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $glueTable, 
        $masterField, 
        $masterId
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->glueTable = $glueTable;
        $this->masterField = $masterField;
        $this->masterId = $masterId;

    }
    
    public function query() {
        
        $query = '
            DELETE FROM  
                `'.$this->glueTable.'` 
            WHERE 
                `'.$this->glueTable.'`.`'.$this->masterField.'` = '.$this->valueWrapper->wrapValue('integer', $this->masterId).' 
            ;
        ';

        $affectedRows = $this->database->delete($query);
        
        return $affectedRows;
        
    }
    
}