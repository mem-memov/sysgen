<?php
namespace Application\api\rest\operation\table;
interface IPage {

    public function getStart();
    public function getLimit();
    
}