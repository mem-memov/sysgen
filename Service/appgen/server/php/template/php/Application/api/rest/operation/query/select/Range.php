<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class Range {
    
    private $database;
    private $table;
    private $fieldStatement;
    private $conditionStatement;
    private $groupStatement;
    private $orderStatement;
    private $joinStatement;
    private $limitStatement;
    private $types;
    
    public function __construct(
        $database, 
        $table,
        $fieldStatement, 
        $conditionStatement, 
        $groupStatement, 
        $orderStatement, 
        $joinStatement, 
        $limitStatement,
        array $types
    ) {
        
        $this->database = $database;
        $this->table = $table;
        $this->fieldStatement = $fieldStatement;
        $this->conditionStatement = $conditionStatement;
        $this->groupStatement = $groupStatement;
        $this->orderStatement = $orderStatement;
        $this->joinStatement = $joinStatement;
        $this->limitStatement = $limitStatement;
        $this->types = $types;
        
    }
    
    public function query() {
        
        $query = '
            SELECT 
                '.$this->fieldStatement->state().'
            FROM 
                `'.$this->table.'` 
            '.$this->joinStatement->state().'
            '.$this->conditionStatement->state().' 
            '.$this->groupStatement->state().'
            '.$this->orderStatement->state().' 
            '.$this->limitStatement->state().' 
            ;
        ';

        $rows = $this->database->select($query, $this->types);

        return $rows;
        
    }
    
}