<?php
namespace Application\api\rest\operation\query\select;
class OrderStatement implements IState {

    private $table;
    
    public function __construct(\Application\api\rest\operation\table\ITable $table) {

        $this->table = $table;
 
    }
    
    public function state() {

        $statement = '';
        
        $parts = array();
        
        foreach ($this->table->getSorters() as $sorter) {
            $property = '`'.$this->table->name().'`.`'.$sorter->getField().'`';
            $direction = $sorter->isAscending() ? 'ASC' : 'DESC';
            $parts[] = ' '.$property.' '.$direction.' ';
        }

        if (count($parts) > 0) {
            $statement = ' ORDER BY '.implode(', ', $parts).' ';
        } else {
            $statement = ' ORDER BY `'.$this->table->name().'`.`orderNumber` ASC ';
        }
        
        return $statement;
        
    }
    
}