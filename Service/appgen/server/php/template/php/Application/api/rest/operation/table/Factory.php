<?php
namespace Application\api\rest\operation\table;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {
    
    private $aspectMap;
    private $associationMap;
    private $referenceMap;
    private $api;
    private $tablePrefix;
    private $tables;

    public function __construct(
        array $aspectMap, 
        array $associationMap, 
        array $referenceMap, 
        array $api, 
        $tablePrefix
    ) {

        $this->aspectMap = $aspectMap;
        $this->associationMap = $associationMap;
        $this->referenceMap = $referenceMap;
        $this->api = $api;
        $this->tablePrefix = $tablePrefix;
        
        $this->tables = array();
        
    }
    
    /**
     * @return \Application\api\rest\operation\table\ITable
     */
    public function requestToTable(\Application\api\rest\IRequest $request) {
        
        $pathParts = $request->getApiPathParts();
        
        if (count($pathParts) < 1) {
            throw new \Exception('Api entity unknown');
        }

        $table = $pathParts[0];
        
        $tables = $this->tables($table);

        if ($request->checkGet('start') && $request->checkGet('limit')) {
            $tables[$table]->setPage(
                $this->page(
                    $request->readGet('start'), 
                    $request->readGet('limit')
                )
            );
        }
        
        if ($request->checkGet('filter')) {
            $tables[$table]->setFilters(
                $this->filters(
                    $table, 
                    json_decode($request->readGet('filter'), true, 100, JSON_UNESCAPED_UNICODE)
                )
            );
        }

        if ($request->checkGet('sort')) {
            $tables[$table]->setSorters(
                $this->sorters(
                    $table, 
                    json_decode($request->readGet('sort'), true, 100, JSON_UNESCAPED_UNICODE)
                )
            );
        }
        
        if ($request->checkGet('group')) {
            $tables[$table]->setGroup(
                $this->group(
                    $table, 
                    json_decode($request->readGet('group'), true, 100, JSON_UNESCAPED_UNICODE)
                )
            );
        }

        if ($tables[$table]->isTree() && count($pathParts) == 2) {
            $tables[$table]->setRootId($pathParts[1]);
        }

        return $tables[$table];
        
    }
    
    /**
     * Creates tables that are significant to the request
     * @param type $startTableName
     * @return \Application\api\rest\operation\table\ITable[]
     */
    private function tables($startTableName) {
        
        $tables = array();

        foreach ($this->selectTables($startTableName) as $requiredTableName => $placeholder) {
            $tables[$requiredTableName] = $this->table($requiredTableName);
        }
        
        foreach ($tables as $name => $table) {
            $this->associate($name, $tables);
            $this->refer($name, $tables);
            $this->aspectuate($name, $tables);
        }
        
        return $tables;
    }
    
    /**
     * Selects only tables that are connected to the requested one
     * @param string $table
     * @param array $tables table names are array keys, values are always true
     * @return boolean[]
     */
    private function selectTables($table, array &$tables = array()) {
        
        if (array_key_exists($table, $tables)) {
            return;
        }

        $tables[$table] = true;
        
        if (array_key_exists($table, $this->associationMap)) {
            
            if (array_key_exists('masterTable', $this->associationMap[$table])) {
                $this->selectTables($this->associationMap[$table]['masterTable'], $tables);
            }
            
            if (array_key_exists('detailTables', $this->associationMap[$table])) {
                foreach ($this->associationMap[$table]['detailTables'] as $detailTable) {
                    $this->selectTables($detailTable, $tables);
                }
            }

        }

        if (array_key_exists($table, $this->referenceMap)) {
            
            foreach ($this->referenceMap[$table] as $field => $reference) {
                $this->selectTables($reference['table'], $tables);
            }
            
        }
        
        foreach ($this->aspectMap as $aspectTable => $aspectData) {
            $this->selectTables($aspectTable, $tables);
        }
        
        return $tables;
        
    }

    /**
     * Creates a standalone table instance
     * @param type $name
     * @return \Application\api\rest\operation\table\Table
     */
    private function table($name) {
        
        return new Table(
            $this->tablePrefix, 
            $name, 
            $this->fields($name),
            array_key_exists('parentId', $this->api[$name]['fields'])
        );
        
    }
    
    /**
     * Associates a table with detail tables and a master table
     * @param string $table
     * @param \Application\api\rest\operation\table\ITable[] $tables
     */
    private function associate($table, array  $tables) {
        
        if (array_key_exists($table, $this->associationMap)) {

            if (array_key_exists('masterTable', $this->associationMap[$table])) {

                $tables[$table]->setMasterGlue(
                    $this->glue(
                        $tables[$this->associationMap[$table]['masterTable']],
                        $tables[$table]
                    )
                );

            }

            if (array_key_exists('detailTables', $this->associationMap[$table])) {

                foreach ($this->associationMap[$table]['detailTables'] as $detailTable) {
                    $tables[$table]->addDetailTable($tables[$detailTable]);
                }

            }
            
        }
        
    }
    
    /**
     * 
     * @param string $table
     * @param \Application\api\rest\operation\table\ITable[] $tables
     */
    private function refer($table, array  $tables) {
        
        if (array_key_exists($table, $this->referenceMap)) {
        
            foreach ($this->referenceMap[$table] as $field => $reference) {
                $tables[$table]->addReference(
                    $this->reference(
                        $tables[$table], 
                        $this->field($field, $this->api[$table]['fields'][$field]), 
                        $tables[$reference['table']], 
                        $reference['fields']
                    )
                );
            }
        
        }
        
    }
    
    /**
     * 
     * @param string $table
     * @param \Application\api\rest\operation\table\ITable[] $tables
     */
    private function aspectuate($table, array  $tables) {

        if (array_key_exists($table, $this->aspectMap)) {
        
            if (array_key_exists('aspectMasterTables', $this->aspectMap[$table])) {
                
                foreach ($this->aspectMap[$table]['aspectMasterTables'] as $aspectMasterTable => $columns) {
                    
                    foreach ($columns as $column) {
                        
                        $aspect = $this->aspect(
                            $tables[$aspectMasterTable],
                            $this->field($column, $this->api[$aspectMasterTable]['fields'][$column]), 
                            $tables[$table]
                        );
                        
                        $tables[$table]->addMasterAspect($aspect);
                        
                        $tables[$aspectMasterTable]->addDetailAspect($aspect);
                        
                    }
                    
                }
                
            }
        
        }
        
    }
    
    private function aspect(ITable $masterTable, IField $masterField, ITable $detailTable) {
        return new Aspect($masterTable, $masterField, $detailTable);
    }

    private function group($table, $item = null) {
        
        $group = null;

        if (!empty($item) && isset($this->api[$table]['fields'][$item['property']])) {
            $direction = strtoupper($item['direction']);
            $allowedDirections = array('ASC', 'DESC');
            if (!in_array($direction, $allowedDirections)) {
                throw new \Exception('Unknown group direction '.$item['direction']);
            }
            $group = new Group($table, $item['property'], $direction);
        }

        return $group;
        
    }
    
    private function sorters($table, $items) {
        
        $sorters = array();

        foreach ($items as $item) {
            if (isset($this->api[$table]['fields'][$item['property']])) {
                $direction = strtoupper($item['direction']);
                $allowedDirections = array('ASC', 'DESC');
                if (!in_array($direction, $allowedDirections)) {
                    throw new \Exception('Unknown sort direction '.$item['direction']);
                }
                $sorters[] = new Sorter($table, $item['property'], $direction);
            }
        }

        return $sorters;
        
    }
    
    private function filters($table, $items = array()) {

        $filters = array();

        $allowedFilterOperators = array(
            'string' => array(
                'like' => 'LIKE',
                'exactMatch' => '='
            ),
            'integer' => array(
                'lt' => '<', 
                'gt' => '>', 
                'eq' => '=',
                'exactMatch' => '='
            ),
            'boolean' => array(
                '=' => '=',
                'exactMatch' => '='
            ),
            'text' => array(
                'like' => 'LIKE',
                'exactMatch' => '='
            ),
            'time' => array(
                'exactMatch' => '='
            )
        );

        foreach ($items as $item) {
            if (isset($this->api[$table]['fields'][$item['property']])) {
                $type = $this->api[$table]['fields'][$item['property']];
                if (array_key_exists('exactMatch', $item) && $item['exactMatch'] == true) {
                    $item['operator'] = 'exactMatch';
                }
                if (!array_key_exists($item['operator'], $allowedFilterOperators[$type])) {
                    throw new \Exception('Unknown filter operator '.$item['operator']);
                }
                $operator = $allowedFilterOperators[$type][$item['operator']];
                $filters[] = new Filter($table, $item['property'], $operator, $item['value'], $type);
            }
        }

        return  $filters;
        
    }

    private function page($start = null, $limit = null) {
        
        if (is_null($start) || is_null($limit)) {
            return null;
        }
        
        return new Page($start, $limit);

    }
    
    private function fields($table) {
        
        $fields = array();

        foreach ($this->api[$table]['fields'] as $name => $type) {
            $fields[] = $this->field($name, $type);
        }
        
        return $fields;
        
    }
    
    private function field($name, $type) {
        return new Field($name, $type);
    }

    private function reference($containerTable, $containerField, $contentTable, array $contentFields) {

        $referencedFields = array();
        foreach ($contentFields as $contentFieldName => $contentFieldType) {
            $referencedFields[$contentFieldName] = $this->field($contentFieldName, $contentFieldType);
        }
        
        return new Reference($containerTable, $containerField, $contentTable, $referencedFields);
    }
    
    private function glue($masterTable, $detailTable) {
        
        return new Glue($masterTable, $detailTable);
        
    }

}