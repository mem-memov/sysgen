<?php
namespace Application\api\rest\operation\table;
interface ITable {

    public function setPage(IPage $page);
    public function setFilters(array $filters);
    public function setSorters(array $sorters);
    public function setGroup(IGroup $group);
    public function setMasterGlue(IGlue $masterGlue);
    public function addDetailTable(ITable $detalTable);
    public function addReference(IReference $reference);
    public function setRootId($rootId);
    public function addDetailAspect(IAspect $aspect);
    public function addMasterAspect(IAspect $aspect);
    
    public function getName();
    public function name();
    public function isTree();
    /**
     * @return \Application\api\rest\operation\table\IPage
     */
    public function getPage();
    public function getFields();
    public function getFilters();
    public function getSorters();
    /**
     * @return \Application\api\rest\operation\table\IGroup
     */
    public function getGroup();
    /**
     * @return \Application\api\rest\operation\table\IGlue
     */
    public function getMasterGlue();
    /**
     * @return \Application\api\rest\operation\table\IReference[]
     */
    public function getReferences();
    /**
     * @return \Application\api\rest\operation\table\IAspect[]
     */
    public function getDetailAspects();
    /**
     * @return \Application\api\rest\operation\table\IAspect[]
     */
    public function getMasterAspects();
    /**
     * @return \Application\api\rest\operation\table\ITable[]
     */
    public function getDetailTables();
    public function getRootId();
    
}