<?php
namespace Application\api\rest;
interface IValueWrapper {

    public  function wrapValue($type, $value);
    
}