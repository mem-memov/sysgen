<?php
namespace Application\api\rest\operation\hook;
interface IService {
    
    /**
     * @return \Application\api\rest\IRequest
     */
    public function request();
    /**
     * @return \Application\api\rest\IResponse
     */
    public function response();
    /**
     * @return \Application\api\rest\operation\table\ITable
     */
    public function table();
    /**
     * @return \Application\database\IDatabase
     */
    public function database();
    /**
     * @return \Application\api\rest\IValueWrapper
     */
    public function valueWrapper();
    /**
     * @return \Application\cache\ICache
     */
    public function cache();
    /**
     * @return \Application\api\rest\operation\query\IFactory
     */
    public function query();
    /**
     * @return \Application\api\rest\operation\value\IFactory
     */
    public function value();
    

}