<?php
namespace Application\api\rest\operation\hook;
class Fetch implements IHook {
    
    private $serviceLocator;
    private $application;
    private $table;
    
    public function __construct(IService $serviceLocator, $application, $table) {
        
        $this->serviceLocator = $serviceLocator;
        $this->application = $application;
        $this->table = $table;
        
    }
    
    public function before() {
        
        $className = $this->className();

        if ($this->classExists($className)) {
            $hook = new $className();
            $operationAllowed = (bool)$hook->before($this->serviceLocator);
            if (!$operationAllowed) {
                $this->response->sendError('Operation not allowed');
            }
        }

    }
    
    public function after($result) {
        
        $className = $this->className();

        if ($this->classExists($className)) {
            $hook = new $className();
            return $hook->after($this->serviceLocator, $result);
        }
        
        return $result;
        
    }
    
    private function classExists($className) {
        
        try {
            $classExists = class_exists($className);
        } catch (\Exception $ex) {
            $classExists = false;
        }
        
        return $classExists;
        
    }
    
    private function className() {
        
        return '\Application\api\rest\operation\hook\\'.$this->application.'\\'.$this->table.'\\'.'Fetch';
        
    }
    
}