<?php
namespace Application\api\rest;
interface IFactory {
    
    public  function make(IRequest $request, IResponse $response);
    
}