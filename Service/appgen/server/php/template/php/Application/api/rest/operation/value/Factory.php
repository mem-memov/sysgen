<?php
namespace Application\api\rest\operation\value;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {

    private $api;

    public function __construct(array $api) {

        $this->api = $api;
        
    }
    
    public function requestToValues(\Application\api\rest\IRequest $request) {
        
        $pathParts = $request->getApiPathParts();
        
        if (count($pathParts) < 1) {
            throw new \Exception('Api entity unknown');
        }

        $table = $pathParts[0];
        
        $items = json_decode($request->readRaw(), true, 100, JSON_UNESCAPED_UNICODE);

        $values = array();
        
        foreach ($items as $property => $value) {
            if (
                isset($this->api[$table]['fields'][$property]) 
                && $property !== 'id' // id must be excluded
            ) {
                $type = $this->api[$table]['fields'][$property];
                $values[] = new Value($property, $type, $value);
            }
        }
        
        return $values;
        
    }
    
    public function rowToValues(\Application\api\rest\operation\table\ITable $table, array $row) {
        
        $values = array();
        
        $fields = $table->getFields();
        
        foreach ($fields as $field) {

            if (
                array_key_exists($field->getName(), $row) 
                && $field->getName() != 'id' // id must be excluded
            ) {
                
                $masterGlue = $table->getMasterGlue();
                if (!is_null($masterGlue) && $masterGlue->isMasterField($field->getName())) {
                    continue;
                }
                
                if (strpos($field->getName(), '.') !== false) { // skipping reference and aspect fields
                    continue;
                }
                
                $values[] = new Value(
                    $field->getName(), 
                    $field->getType(), 
                    $row[$field->getName()]
                );
                
            }
            
        }
        
        return $values;
        
    }

}