<?php
namespace Application\api\rest\operation\value;
interface IValue {

    public function getField();
    public function getType();
    public function getValue();
    
}