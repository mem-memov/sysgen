<?php
namespace Application\api\rest\operation\query\insert;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {
    
    private $database;
    private $valueWrapper;
    
    public function __construct($database, $valueWrapper) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        
    }
    
    public function row(\Application\api\rest\operation\table\ITable $table) {
        
        return new Row(
            $this->database, 
            $table->name()
        );
        
    }

    public function glue(\Application\api\rest\operation\table\ITable $table, $masterId, $detailId) {
        
        return new Glue(
            $this->database, 
            $this->valueWrapper, 
            $table->getMasterGlue()->master(), 
            $table->getMasterGlue()->detail(), 
            $table->getMasterGlue()->table(), 
            $masterId, 
            $detailId
        );
        
    }

    public function aspect(\Application\api\rest\operation\table\IAspect $aspect, $masterId, $detailId) {
        
        return new Aspect(
            $this->database, 
            $this->valueWrapper, 
            $aspect->master(), 
            $aspect->detail(), 
            $aspect->table(), 
            $masterId, 
            $detailId
        );
        
    }

}