<?php
namespace Application\api\rest\operation\query\update;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class OrderNumber implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $orderNumber;
    private $id;
    
    public function __construct($database, $valueWrapper, $table, $orderNumber, $id) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->orderNumber = $orderNumber;
        $this->id = $id;

    }
    
    public function query() {
        
        $query = '
            UPDATE  
                `'.$this->table.'` 
            SET 
                `orderNumber` = '.$this->valueWrapper->wrapValue('integer', $this->orderNumber).' 
            WHERE 
                `'.$this->table.'`.`id` = '.$this->valueWrapper->wrapValue('integer', $this->id).' 
            ;
        ';

        $affectedRows = $this->database->update($query);
        
        return  $affectedRows;

        
    }

    
}