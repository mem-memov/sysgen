<?php
namespace Application\api\rest\operation\query\insert;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class Row implements IQuery {
    
    private $database;
    private $table;
    
    public function __construct($database, $table) {
        
        $this->database = $database;
        $this->table = $table;
        
    }
    
    public function query() {
        
        $query = '
            INSERT INTO  
                `'.$this->table.'` 
            SET 
                `id` = DEFAULT
            ;
        ';

        $id = $this->database->insert($query);
        
        return $id;
        
    }

    
}