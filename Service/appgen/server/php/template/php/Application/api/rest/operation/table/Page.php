<?php
namespace Application\api\rest\operation\table;
class Page implements IPage{
    
    private $start;
    private $limit;
  
    public function __construct($start, $limit) {
        
        $this->start = $start;
        $this->limit = $limit;
        
    }
    
    public function getStart() {
        return $this->start;
    }
    
    public function getLimit() {
        return $this->limit;
    }
    
}