<?php
namespace Application\api\rest\operation\query\delete;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ByMasterIdInDetailTable implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $glueTable;
    private $detailTable;
    private $detailField;
    private $masterField;
    private $masterId;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $glueTable, 
        $detailTable, 
        $detailField,
        $masterField,
        $masterId
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->glueTable = $glueTable;
        $this->detailTable = $detailTable;
        $this->detailField = $detailField;
        $this->masterField = $masterField;
        $this->masterId = $masterId;

    }
    
    public function query() {
        
        $query = '
            DELETE 
                `'.$this->detailTable.'`
            FROM  
                `'.$this->detailTable.'` 
                LEFT JOIN `'.$this->glueTable.'` ON(`'.$this->glueTable.'`.`'.$this->detailField.'` = `'.$this->detailTable.'`.`id`)
            WHERE 
                `'.$this->glueTable.'`.`'.$this->masterField.'` = '.$this->valueWrapper->wrapValue('integer', $this->masterId).' 
            ;
        ';

        $affectedRows = $this->database->delete($query);
        
    }
    
}