<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ByMasterId {
    
    private $database;
    private $valueWrapper;
    private $detailTable;
    private $fieldStatement;
    private $joinStatement;
    private $glueTable;
    private $masterField;
    private $masterId;
    private $types;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $detailTable,
        $fieldStatement, 
        $joinStatement,
        $glueTable, 
        $masterField,
        $masterId,
        array $types
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->detailTable = $detailTable;
        $this->fieldStatement = $fieldStatement;
        $this->joinStatement = $joinStatement;
        $this->glueTable = $glueTable;
        $this->masterField = $masterField;
        $this->masterId = $masterId;
        $this->types = $types;
        
    }
    
    public function query() {
        
        $query = '
            SELECT 
                '.$this->fieldStatement->state().'
            FROM  
                `'.$this->detailTable.'` 
                '.$this->joinStatement->state().'
            WHERE 
                `'.$this->glueTable.'`.`'.$this->masterField.'` = '.$this->valueWrapper->wrapValue('integer', $this->masterId).' 
            ;
        ';

        $rows = $this->database->select($query, $this->types);
        
        return  $rows;
        
    }
    
}