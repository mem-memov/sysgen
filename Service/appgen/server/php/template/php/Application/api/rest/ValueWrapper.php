<?php
namespace Application\api\rest;
class ValueWrapper implements IValueWrapper{
    
    private $database;
    
    public function __construct($database) {
        
        $this->database = $database;
        
    }
    
    public  function wrapValue($type, $value) {
        
        switch ($type) {
            case 'integer':
                return $this->database->renderInteger($value);
                break;
            case 'string':
                return $this->database->renderString($value);
                break;
            case 'text':
                return $this->database->renderString($value);
                break;
            case 'boolean':
                return $this->database->renderBoolean($value);
                break;
            case 'date':
                return $this->database->renderDate($value);
                break;
            default:
                throw new \Exception('Unknown data type '.$type);
                break;
        }
        
    }
    
}