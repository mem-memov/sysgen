<?php
namespace Application\api\rest\operation\value;
/**
 * Value Object: http://martinfowler.com/eaaCatalog/valueObject.html
 */
class Value implements IValue{

    private $field;
    private $type;
    private $value;
    
    public function __construct($field, $type, $value) {

        $this->field = $field;
        $this->type = $type;
        $this->value = $value;
        
    }
    
    public function getField() {
        return $this->field;
    }
    
    public function getType() {
        return $this->type;
    }
    
    public function getValue() {
        return $this->value;
    }
    
}