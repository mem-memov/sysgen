<?php
namespace Application\api\rest\operation;
interface IFactory {
    
    public function create(\Application\api\rest\IRequest $request);
    public function remove(\Application\api\rest\IRequest $request);
    public function reorder(\Application\api\rest\IRequest $request, $id, $from, $to);
    public function change(\Application\api\rest\IRequest $request, $id);
    public function copy(\Application\api\rest\IRequest $request, $from, $to);
    public function fetch(\Application\api\rest\IRequest $request);
    public function filter(\Application\api\rest\IRequest $request);

}