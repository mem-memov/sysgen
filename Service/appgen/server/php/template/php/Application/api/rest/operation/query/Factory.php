<?php
namespace Application\api\rest\operation\query;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {
    
    private $database;
    private $valueWrapper;
    private $api;
    
    
    private $deleteFactory;
    private $updateFactory;
    private $insertFactory;
    private $selectFactory;
    
    public function __construct($database, $valueWrapper, array $api) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->api = $api;
        
        $this->deleteFactory = null;
        $this->updateFactory = null;
        $this->insertFactory = null;
        $this->selectFactory = null;
        
    }
    
    public function startTransaction() {
        //$this->database->startTransaction();
    }
    
    public function commitTransaction() {
        //$this->database->commitTransaction();
    }
    
    /**
     * 
     * @return delete\Factory
     */
    public function delete() {
        
        if (is_null($this->deleteFactory)) {
            $this->deleteFactory = new delete\Factory($this->database, $this->valueWrapper);
        }
        
        return $this->deleteFactory;
        
    }
    
    /**
     * 
     * @return update\Factory
     */
    public function update() {
        
        if (is_null($this->updateFactory)) {
            $this->updateFactory = new update\Factory($this->database, $this->valueWrapper);
        }
        
        return $this->updateFactory;
        
    }
    
    /**
     * 
     * @return insert\Factory
     */
    public function insert() {
        
        if (is_null($this->insertFactory)) {
            $this->insertFactory = new insert\Factory($this->database, $this->valueWrapper);
        }
        
        return $this->insertFactory;
        
    }
    
    /**
     * 
     * @return select\Factory
     */
    public function select() {
        
        if (is_null($this->selectFactory)) {
            $this->selectFactory = new select\Factory($this->database, $this->valueWrapper, $this->api);
        }
        
        return $this->selectFactory;
        
    }

}