<?php
namespace Application\api\rest\operation;
class Remove implements IRun {
    
    private $queryFactory;
    private $table;
    private $id;
    
    public function __construct(
        query\IFactory $queryFactory, 
        table\ITable $table,
        $id
    ) {
        
        $this->queryFactory = $queryFactory;
        $this->table = $table;
        $this->id = $id;

    }
    
    public function run() {
        
        $row = $this->queryFactory
            ->select()
            ->byId($this->table, $this->id)
            ->query()
        ;
        
        if (empty($row)) {
            return;
        }
        
        $this->remove($this->table, $row);

    }
    
    private function remove(table\ITable $table, array $row) { // next table may be a tree or a plain one, so cascading calls this method again and again

        $affectedRows = $this->queryFactory
            ->delete()
            ->byId($table, $row['id'])
            ->query()
        ;
        
        $this->queryFactory
            ->update()
            ->shiftUp($table, $row['orderNumber'])
            ->query()
        ;
        
        if ($table->isTree()) {
            $this->tree($table, $row['id']);
        }

        $this->cascade($table, $row['id']);
        $this->cascadeAspect($table, $row['id']);
        
    }
    
    private function cascade(table\ITable $masterTable, $masterId) {
        
        foreach ($masterTable->getDetailTables() as $detailTable) {
            
            $rows = $this->queryFactory
                ->select()
                ->byMasterId($detailTable, $masterId)
                ->query()
            ;
            
            $this->queryFactory
                ->delete()
                ->byMasterId($detailTable, $masterId)
                ->query()
            ;

            foreach ($rows as $row) {

                $this->queryFactory
                    ->update()
                    ->shiftUp($detailTable, $row['orderNumber'])
                    ->query()
                ;
                
                $this->remove($detailTable, $row);
                
            }

        }
        
    }
    
    private function cascadeAspect(table\ITable $masterTable, $masterId) {
        
        foreach ($masterTable->getDetailAspects() as $aspect) {
            
            $rows = $this->queryFactory
                ->select()
                ->aspect($aspect, $masterId)
                ->query()
            ;
            
            $this->queryFactory
                ->delete()
                ->aspect($aspect, $masterId)
                ->query()
            ;
            
            $detailTable = $aspect->getDetailTable();
            
            foreach ($rows as $row) {

                $this->queryFactory
                    ->update()
                    ->shiftUp($detailTable, $row['orderNumber'])
                    ->query()
                ;
                
                $this->remove($detailTable, $row);
                
            }
            
        }
        
    }
    
    private function tree(table\ITable $table, $parentId) {

        $rows = $this->queryFactory
            ->select()
            ->children($this->table, $parentId)
            ->query()
        ;
        
        $this->queryFactory
            ->delete()
            ->children($table, $parentId)
            ->query()
        ;
        
        foreach ($rows as $index => $row) {
            
            $this->remove($table, $row);
            
        }

        return $rows;
        
    }
    
}