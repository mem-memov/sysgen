<?php
namespace Application\api\rest\operation\hook;
class Factory implements IFactory {
    
    private $application;
    private $serviceLocator;
    
    public function __construct(
            $application,
            IService $serviceLocator
       ) {
        
        $this->application = $application;
        $this->serviceLocator = $serviceLocator;
        
    }
    
    public function fetch($table) {
        return new Fetch($this->serviceLocator, $this->application, $table);
    }
    
}