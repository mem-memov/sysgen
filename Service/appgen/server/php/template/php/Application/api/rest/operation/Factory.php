<?php
namespace Application\api\rest\operation;
/**
 * Factory: http://www.oodesign.com/factory-pattern.html
 */
class Factory implements IFactory {

    private $queryFactory;
    private $table;
    private $valueFactory;
    private $hookFactory;
    private $cache;
    
    public function __construct(
        query\IFactory $queryFactory, 
        table\ITable $table,
        value\IFactory $valueFactory,
        hook\IFactory $hookFactory,
        \Application\cache\ICache $cache
    ) {

        $this->queryFactory = $queryFactory;
        $this->table = $table;
        $this->valueFactory = $valueFactory;
        $this->hookFactory = $hookFactory;
        $this->cache = $cache;
        
        $this->queryFactory->startTransaction();
        
    }
    
    public function __destruct() {
        
        $this->queryFactory->commitTransaction();
        
    }
    
    public function create(\Application\api\rest\IRequest $request) {
        
        $values = json_decode($request->readRaw(), true, 100, JSON_UNESCAPED_UNICODE);

        $orderNumber = array_key_exists('orderNumber', $values) ? $values['orderNumber'] : null;
        
        $masterId = null;
        if (!is_null($this->table->getMasterGlue())) {
            $masterField = $this->table->getMasterGlue()->master();
            if (array_key_exists($masterField, $values)) {
                $masterId = array_key_exists($masterField, $values) ? $values[$masterField] : null;
            }
        }
       
        $aspect = null;
        $aspectId = null;
        foreach ($this->table->getMasterAspects() as $currentAspect) {
            $aspectField = $currentAspect->master().'.'.$currentAspect->field();
            if (array_key_exists($aspectField, $values)) {
                $aspect = $currentAspect;
                $aspectId = $values[$aspectField];
                break;
            }
        }

        return new Create(
            $this->queryFactory, 
            $this->table,
            $orderNumber,
            $masterId,
            $aspect,
            $aspectId
        );
        
    }
    
    public function remove(\Application\api\rest\IRequest $request) {
        
        $pathParts = $request->getApiPathParts();
        if (count($pathParts) < 2) {
            throw new \Exception('Id not supplied in request');
        }
        $id = $pathParts[1];

        return new Remove(
            $this->queryFactory, 
            $this->table, 
            $id
        );
        
    }
    
    public function reorder(\Application\api\rest\IRequest $request, $id, $from, $to) {

        return new Reorder(
            $this->queryFactory, 
            $this->table,
            $id,
            $from,
            $to
        );
        
    }
    
    public function change(\Application\api\rest\IRequest $request, $id) {

        $values = $this->valueFactory->requestToValues($request);

        return  new Change(
            $this->queryFactory, 
            $this->table, 
            $id,
            $values
        );
        
    }
    
    public function copy(\Application\api\rest\IRequest $request, $from, $to) {

        return new Copy(
            $this->queryFactory,
            $this->valueFactory,
            $this->table,
            $from,
            $to
        );
        
    }
    
    public function fetch(\Application\api\rest\IRequest $request) {
        
        $pathParts = $request->getApiPathParts();
        if (count($pathParts) < 2) {
            throw new \Exception('Id not supplied in request');
        }
        $id = $pathParts[1];

        return new Fetch(
            $this->queryFactory, 
            $this->hookFactory->fetch($pathParts[0]), 
            $this->cache,
            $this->table, 
            $id
        );
        
    }

    public function filter(\Application\api\rest\IRequest $request) {

        return new Filter($this->queryFactory, $this->table);
        
    }
    
}