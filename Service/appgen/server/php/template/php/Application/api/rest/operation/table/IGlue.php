<?php
namespace Application\api\rest\operation\table;
interface IGlue {

    public function table();
    public function detail();
    public function master();
    public function isMasterField($field);
    
}