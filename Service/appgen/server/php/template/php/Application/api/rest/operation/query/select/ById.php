<?php
namespace Application\api\rest\operation\query\select;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ById implements IQuery {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $fieldStatement;
    private $joinStatement;
    private $id;
    private $types;
    
    public function __construct(
        $database, 
        $valueWrapper, 
        $table, 
        $fieldStatement, 
        $joinStatement, 
        $id, 
        array $types
    ) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->fieldStatement = $fieldStatement;
        $this->joinStatement = $joinStatement;
        $this->id = $id;
        $this->types = $types;
        
    }
    
    public function query() {
        
        $query = '
            SELECT 
                '.$this->fieldStatement->state().'
            FROM 
                `'.$this->table.'` 
                '.$this->joinStatement->state().'
            WHERE
                `'.$this->table.'`.`id` = '.$this->valueWrapper->wrapValue('integer', $this->id).'
            ;
        ';

        $rows = $this->database->select($query, $this->types);
        
        if (empty($rows)) {
            return null;
        }

        return $rows[0];
        
    }

    
}