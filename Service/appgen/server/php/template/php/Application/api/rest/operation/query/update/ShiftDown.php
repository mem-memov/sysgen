<?php
namespace Application\api\rest\operation\query\update;
/**
 * Query Object: http://martinfowler.com/eaaCatalog/queryObject.html
 */
class ShiftDown {
    
    private $database;
    private $valueWrapper;
    private $table;
    private $orderNumber;
    
    public function __construct($database, $valueWrapper, $table, $orderNumber) {
        
        $this->database = $database;
        $this->valueWrapper = $valueWrapper;
        $this->table = $table;
        $this->orderNumber = $orderNumber;
        
    }
    
    public function query() {
        
        $query = '
            UPDATE 
                `'.$this->table.'` 
            SET 
                `orderNumber` = `orderNumber` + 1 
            WHERE 
                `orderNumber` > '.$this->valueWrapper->wrapValue('integer', $this->orderNumber).' 
            ;
        ';

        $affectedRows = $this->database->update($query);
        
    }
    
}