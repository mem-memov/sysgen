<?php
namespace Service\appgen;
class Renderer implements IRenderer {
    
    public function render($template, array $data) {
        
        ob_start();
        require $template;
        $result = ob_get_clean();
        
        return $result;
        
    }
    
}