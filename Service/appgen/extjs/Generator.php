<?php
namespace Service\appgen\extjs;
class Generator {
    
    private $configuration;
    private $applicationDirectory;
    private $renderer;
    private $fileSystem;
    
    private $typeMap;
    private $views;

    
    public function __construct(
        array $configuration, 
        $applicationDirectory, 
        \Service\appgen\IRenderer $renderer, 
        \Service\appgen\IFileSystem $fileSystem
    ) {

        $this->configuration = $configuration;
        $this->applicationDirectory = $applicationDirectory;
        $this->renderer = $renderer;
        $this->fileSystem = $fileSystem;

        $this->typeMap = array(
            'string' => 'string',
            'integer' => 'integer',
            'boolean' => 'boolean',
            'text' => 'string',
            'time' => 'date',
            'reference' => 'integer'
        );
        $this->views = array();

    }
    
    public function generate() {
        /*
        $pathToSdk = __DIR__.'/sdk/ext-5.1.0';
        $generateCommand = 'sencha -sdk '.$pathToSdk.' generate app -ext '.$this->configuration['name'].' '.$this->applicationDirectory;
        $commandOutput = array();
        $commandResult;
        exec($generateCommand, $commandOutput, $commandResult);
        echo $generateCommand."<br>\n";
        echo implode("<br>\n", $commandOutput)."<br>\n";
        echo $commandResult."<br>\n";
        */
 
        $this->fileSystem->copyDirectory(
            __DIR__.'/template', 
            $this->applicationDirectory
        );
        
        $this->fileSystem->copyDirectory(
            __DIR__.'/../task/'.$this->configuration['name'].'/resources', 
            $this->applicationDirectory.'/resources'
        );
        
        $this->fileSystem->copyDirectory(
            __DIR__.'/template/resources', 
            $this->applicationDirectory.'/resources'
        );
        
        $this->fileSystem->copyDirectory(
            __DIR__.'/../task/'.$this->configuration['name'].'/app', 
            $this->applicationDirectory.'/app'
        );
       
        $this->fileSystem->writeFile(
            $this->applicationDirectory.'/configuration.js',
            json_encode(array(
                'tables' => $this->configuration['tables'],
                'api' => $this->configuration['webServer']['baseUrl'].'api/'
            ))
        );

        $this->index();
        
    }
            
    public function index() {
        
        if (isset($this->configuration['webServer']['index'])) {
            
            $indexFile = $this->fileSystem->readFile(__DIR__.'/template/index.html');
            
            if (isset($this->configuration['webServer']['index']['styles'])) {

                foreach ($this->configuration['webServer']['index']['styles'] as $style) {
                    $styleTag = '<link href="'.$style.'" rel="stylesheet"  type="text/css">';
                    $indexFile = str_replace('</head>', "\t".$styleTag."\n".'</head>', $indexFile);
                }
                
            }
            
            if (isset($this->configuration['webServer']['index']['scripts'])) {

                foreach ($this->configuration['webServer']['index']['scripts'] as $script) {
                    $scriptTag = '<script src="'.$script.'" type="text/javascript"></script>';
                    $indexFile = str_replace('</head>', $scriptTag."\n".'</head>', $indexFile);
                }
                
            }
            
        }
        
        $this->fileSystem->writeFile(
            $this->applicationDirectory.'/index.html',
            $indexFile
        );
        
    }

}