Ext.define('System.Application', {
    extend: 'Ext.app.Application',
    
    requires:['System.view.main.View'],
    
    name: 'System',
    
    defaultToken : 'home',

    controllers: [
        'System.controller.main.Controller',
        'System.controller.gridEditor.Controller'
    ],

    launch: function () {
        Ext.create('System.view.main.View', {
            renderTo: Ext.getBody()
        });
    },
    
    init: function() {
        
        Ext.override(Ext.ClassManager.get('Ext.grid.filters.Filters'), {
            
            /**
             * Column filter problem in trees
             * http://www.sencha.com/forum/showthread.php?292910-Broken-column-menu-for-tree-panel-with-filters-plugin
             */
            onMenuBeforeShow: function (menu) {
                var me = this,
                    menuItem, filter, ownerGrid, ownerGridId;

                if (me.showMenu) {
                    // In the case of a locked grid, we need to cache the 'Filters' menuItem for each grid since
                    // there's only one Filters instance. Both grids/menus can't share the same menuItem!
                    if (!me.menuItems) {
                        me.menuItems = {};
                    }

                    // Don't get the owner panel if in a locking grid since we need to get the unique menuItems key.
                    //ownerGrid = me.grid; <-- old
                    ownerGrid = menu.up('grid') || menu.up('treepanel'); // <-- new
                    ownerGridId = ownerGrid.id;

                    menuItem = me.menuItems[ownerGridId];

                    if (!menuItem || menuItem.isDestroyed) {
                        menuItem = me.createMenuItem(menu, ownerGridId);
                    }

                    // Save a ref to the root "Filters" menu item, column filters make use of it.
                    me.activeFilterMenuItem = menuItem;

                    filter = me.getMenuFilter(ownerGrid.headerCt);
                    if (filter) {
                        filter.showMenu(menuItem);
                    }

                    menuItem.setVisible(!!filter);
                    me.sep.setVisible(!!filter);
                }
            }
        });
        
    }
    
});
