Ext.define('System.controller.gridEditor.Controller', {
    extend: 'Ext.app.Controller',
    
    requires: [
        'System.controller.gridEditor.factory.Grid',
        'System.controller.gridEditor.factory.Tree',
        'System.controller.gridEditor.factory.Field',
        'System.controller.gridEditor.factory.Column'
    ],
    
    config: {
        treeFactory: null,
        gridFactory: null,
        listen: {
            controller: {
                '*': {
                   makeTable: 'onMakeTable'
                }
            }
        }
    },
    
    init: function() {
        
        var fieldFactory = Ext.create('System.controller.gridEditor.factory.Field');
        var columnFactory = Ext.create('System.controller.gridEditor.factory.Column');

        this.setTreeFactory(Ext.create('System.controller.gridEditor.factory.Tree', {
            fieldFactory: fieldFactory,
            columnFactory: columnFactory
        }));
        
        this.setGridFactory(Ext.create('System.controller.gridEditor.factory.Grid', {
            fieldFactory: fieldFactory,
            columnFactory: columnFactory
        }));
       
    },
    
    onMakeTable: function(configuration) {

        var component;

        if (configuration.tree) {
            component = this.getTreeFactory().make(configuration);
        } else {
            component = this.getGridFactory().make(configuration);
        }
        
        this.fireEvent('tableMade', component);

    }
    
});