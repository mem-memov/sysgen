Ext.define('System.controller.gridEditor.factory.field.String', {

    make: function(fields, configuration) {
        
        var field = Ext.create('System.model.gridEditor.field.String', {
            name: configuration.name
        });
        
        fields.push(field);
                
    }
    
});