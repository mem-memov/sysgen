Ext.define('System.controller.gridEditor.factory.field.Time', {

    make: function(fields, configuration) {
        
        var field = Ext.create('System.model.gridEditor.field.Time', {
            name: configuration.name
        });
        
        fields.push(field);
                
    }
    
});