Ext.define('System.controller.gridEditor.factory.column.Time', {
    
    make: function(columns, configuration, table) {
        
        var column = Ext.create('Ext.grid.column.Date', {
            text: configuration.title,
            dataIndex: configuration.name,
            format: 'd.m.Y',
            filter: {
                type: 'date'
            },
            editor: {
                xtype: 'datefield',
                format: 'd.m.Y'
            }
        });
        
        columns.push(column);
                
    }
    
});