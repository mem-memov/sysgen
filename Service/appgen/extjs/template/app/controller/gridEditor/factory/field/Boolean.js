Ext.define('System.controller.gridEditor.factory.field.Boolean', {

    make: function(fields, configuration) {
        
        var field = Ext.create('System.model.gridEditor.field.Boolean', {
            name: configuration.name
        });
        
        fields.push(field);
                
    }
    
});