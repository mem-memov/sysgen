Ext.define('System.controller.gridEditor.factory.column.Integer', {
    
    make: function(columns, configuration, table) {
        
        var column = Ext.create('Ext.grid.column.Number', {
            text: configuration.title,
            dataIndex: configuration.name,
            format: '0',
            filter: {
                type: 'number'
            }, 
            editor: {
                xtype: 'numberfield' 
            }
        });
        
        columns.push(column);
                
    }
    
});