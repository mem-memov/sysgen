Ext.define('System.controller.gridEditor.factory.column.Boolean', {
    
    make: function(columns, configuration, table) {
        
        var column = Ext.create('Ext.grid.column.Boolean', {
            text: configuration.title,
            dataIndex: configuration.name,
            trueText: '+',
            falseText: '-',
            filter: {
                type: 'boolean'
            },
            editor: {
                xtype: 'checkboxfield'
            }
        });
        
        columns.push(column);
                
    }
    
});