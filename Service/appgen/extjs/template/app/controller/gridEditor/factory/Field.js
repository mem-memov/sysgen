Ext.define('System.controller.gridEditor.factory.Field', {
    
    requires: [
        'System.controller.gridEditor.factory.field.Id',
        'System.controller.gridEditor.factory.field.ParentId',
        'System.controller.gridEditor.factory.field.MasterId',
        'System.controller.gridEditor.factory.field.OrderNumber',
        'System.controller.gridEditor.factory.field.Boolean',
        'System.controller.gridEditor.factory.field.Integer',
        'System.controller.gridEditor.factory.field.Reference',
        'System.controller.gridEditor.factory.field.String',
        'System.controller.gridEditor.factory.field.Text',
        'System.controller.gridEditor.factory.field.Time',
        'System.controller.gridEditor.factory.field.Aspect'
    ],
    
    config: {
        id: null,
        parentId: null,
        masterId: null,
        orderNumber: null,
        boolean: null,
        integer: null,
        reference: null,
        string: null,
        text: null,
        time: null,
        aspect: null
    },
    
    constructor: function(config) {
        this.initConfig({
            id: Ext.create('System.controller.gridEditor.factory.field.Id'),
            parentId: Ext.create('System.controller.gridEditor.factory.field.ParentId'),
            masterId: Ext.create('System.controller.gridEditor.factory.field.MasterId'),
            orderNumber: Ext.create('System.controller.gridEditor.factory.field.OrderNumber'),
            boolean: Ext.create('System.controller.gridEditor.factory.field.Boolean'),
            integer: Ext.create('System.controller.gridEditor.factory.field.Integer'),
            reference: Ext.create('System.controller.gridEditor.factory.field.Reference'),
            string: Ext.create('System.controller.gridEditor.factory.field.String'),
            text: Ext.create('System.controller.gridEditor.factory.field.Text'),
            time: Ext.create('System.controller.gridEditor.factory.field.Time'),
            aspect: Ext.create('System.controller.gridEditor.factory.field.Aspect')
        });
    },
    
    make: function(configuration) {
        
        var fields = [];
        
        Ext.Object.each(configuration.columns, function(name, column) {

            var factory = this.getConfig(column.type);
            
            factory.make(fields, column);
            
        }, this);
        
        return fields;
        
    }
    
});