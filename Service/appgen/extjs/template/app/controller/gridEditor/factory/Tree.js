Ext.define('System.controller.gridEditor.factory.Tree', {
    
    requires: [
        'System.view.gridEditor.tree.View'
    ],
    
    config: {
        fieldFactory: null,
        columnFactory: null,
    },
    
    constructor: function(config) {
        this.initConfig(config);
    },
    
    make: function(configuration) {
        
        var viewClass = 'System.view.gridEditor.' + configuration.name + '.View';
        
        var columns = [{
            text: 'tree',
            xtype: 'treecolumn',
            renderer: function(value, metaDate, record) {
                return '-'; //record.get('id');
            }
        }];
    
        columns = columns.concat(this.getColumnFactory().make(configuration));
        
        Ext.define(viewClass, {
            extend: 'System.view.gridEditor.tree.View',
            controller: this.makeViewController(configuration),
            title: configuration.title,
            store: this.makeStore(configuration),
            columns: columns
        });

        var component = Ext.create(viewClass);
        
        return component;

    },
    
    makeViewController: function(configuration) {

        var viewControllerClass = 'System.view.gridEditor.' + configuration.name + '.Controller';

        var routes = {};
        routes[configuration.name] = 'onRoute';
        routes[configuration.name + '/'] = 'onRoute';
        routes[configuration.name + '/:field/:value'] = 'onRoute';
        
        Ext.define(viewControllerClass, {
            extend: 'System.view.gridEditor.tree.Controller',
            alias: 'controller.' + configuration.name,
            name: configuration.name,
            masterTable: configuration.masterTable ? configuration.masterTable : false,
            aspectMasterTables: configuration.aspectMasterTables ? configuration.aspectMasterTables : [],
            routes: routes
        });
        
        return configuration.name;
        
    },
    
    makeStore: function(configuration) {
        
        var storeId = configuration.name;
        var store = 'System.store.gridEditor.' + Ext.String.capitalize(configuration.name);

        var config = {
            extend: 'System.store.gridEditor.TreeStore',
            storeId: storeId,
            model: this.makeModel(configuration),
        };
        
        Ext.define(store,  config);
        
        Ext.create(store);

        return storeId;
        
    },
    
    makeModel: function(configuration) {

        var model = 'System.model.gridEditor.' + Ext.String.capitalize(configuration.name);

        Ext.define(
            model, 
            {
                extend: 'System.model.gridEditor.TreeModel',
                fields: this.getFieldFactory().make(configuration)
            },
            function () {
                this.getProxy().setUrl(configuration.api + configuration.name + '/');
            }
        );

        return model;
        
    }
    
});