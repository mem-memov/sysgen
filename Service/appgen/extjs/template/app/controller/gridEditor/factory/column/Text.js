Ext.define('System.controller.gridEditor.factory.column.Text', {
    
    make: function(columns, configuration, table) {
        
        var column = Ext.create('Ext.grid.column.Column', {
            text: configuration.title,
            dataIndex: configuration.name,
            flex: 1,
            filter: {
                type: 'string'
            },
            editor: {
                xtype: 'textfield'
            }
        });
        
        columns.push(column);
                
    }
    
});