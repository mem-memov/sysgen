Ext.define('System.controller.gridEditor.factory.field.ParentId', {

    make: function(fields, configuration) {
        
        var field = Ext.create('System.model.gridEditor.field.Integer', {
            name: configuration.name
        });
        
        fields.push(field);
                
    }
    
});