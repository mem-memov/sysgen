Ext.define('System.controller.gridEditor.factory.field.MasterId', {

    make: function(fields, configuration) {

        var field = Ext.create('System.model.gridEditor.field.Integer', {
            name: configuration.name,
            reference: {
                parent: Ext.String.capitalize(configuration.name),
                inverse: configuration.detailTable
            }
        });
        
        fields.push(field);
                
    }
    
});