Ext.define('System.controller.gridEditor.factory.Column', {
    
    requires: [
        'System.controller.gridEditor.factory.column.Id',
        'System.controller.gridEditor.factory.column.ParentId',
        'System.controller.gridEditor.factory.column.MasterId',
        'System.controller.gridEditor.factory.column.OrderNumber',
        'System.controller.gridEditor.factory.column.Boolean',
        'System.controller.gridEditor.factory.column.Integer',
        'System.controller.gridEditor.factory.column.Reference',
        'System.controller.gridEditor.factory.column.String',
        'System.controller.gridEditor.factory.column.Text',
        'System.controller.gridEditor.factory.column.Time',
        'System.controller.gridEditor.factory.column.Aspect'
    ],
    
    config: {
        id: null,
        parentId: null,
        masterId: null,
        orderNumber: null,
        boolean: null,
        integer: null,
        reference: null,
        string: null,
        text: null,
        time: null,
        aspect: null
    },
    
    constructor: function(config) {
        this.initConfig({
            id: Ext.create('System.controller.gridEditor.factory.column.Id'),
            parentId: Ext.create('System.controller.gridEditor.factory.column.ParentId'),
            masterId: Ext.create('System.controller.gridEditor.factory.column.MasterId'),
            orderNumber: Ext.create('System.controller.gridEditor.factory.column.OrderNumber'),
            boolean: Ext.create('System.controller.gridEditor.factory.column.Boolean'),
            integer: Ext.create('System.controller.gridEditor.factory.column.Integer'),
            reference: Ext.create('System.controller.gridEditor.factory.column.Reference'),
            string: Ext.create('System.controller.gridEditor.factory.column.String'),
            text: Ext.create('System.controller.gridEditor.factory.column.Text'),
            time: Ext.create('System.controller.gridEditor.factory.column.Time'),
            aspect: Ext.create('System.controller.gridEditor.factory.column.Aspect')
        });
    },
    
    
    make: function(configuration, table) {

        var columns = [];
        
        Ext.Object.each(configuration.columns, function(name, column) {

            var factory = this.getConfig(column.type);

            factory.make(columns, column, configuration.name);
            
        }, this);

        this.addActionColumn(configuration, columns);
        
        return columns;
        
    },
    
    addActionColumn: function(configuration, columns) {
        
        var actionItems = [{
            icon: 'resources/icons/magnifying_glass.png',
            tooltip: 'Подробнее',
            handler: function(view, rowIndex, colIndex) {
                var record = view.getStore().getAt(rowIndex);
                view.getSelectionModel().select(record, false, true);
                view.getController().onReadButtonClick(record);
            }
        }];
        
        Ext.Array.each(configuration.detailTables, function(detailTable) {
            actionItems.push({
                icon: 'resources/icons/table.png',
                tooltip: detailTable,
                handler: function(view, rowIndex, colIndex, item) {
                    var record = view.getStore().getAt(rowIndex);
                    view.getSelectionModel().select(record, false, true);
                    view.ownerGrid.getController().onAssociationButtonClick(record, detailTable);
                }
            });
        });
        
        columns.push({
            xtype: 'actioncolumn',
            width: 25 * (actionItems.length + 1),
            items: actionItems
        });
        
    }
    
});