Ext.define('System.controller.gridEditor.factory.field.Aspect', {

    make: function(fields, configuration) {
        
        var field = Ext.create('System.model.gridEditor.field.Aspect', {
            name: configuration.name
        });
        
        fields.push(field);
                
    }
    
});