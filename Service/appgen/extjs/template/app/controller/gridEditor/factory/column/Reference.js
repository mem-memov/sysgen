Ext.define('System.controller.gridEditor.factory.column.Reference', {
    
    make: function(columns, configuration, table) {
        
        var referencedColumns = [
            Ext.create('Ext.grid.column.Number', {
                text: configuration.name,
                dataIndex: configuration.name,
                hidden: true,
                format: '0',
                filter: {
                    type: 'number'
                }, 
                editor: {
                    xtype: 'numberfield' 
                }
            })
        ];
        
        Ext.Array.each(configuration.columns, function(column) {
            
            referencedColumns.push(this.makeReferencedColumn(column, configuration));
            
        }, this);
        
        var column = Ext.create('Ext.grid.column.Column', {
            text: configuration.title,
            columns: referencedColumns
        });
        
        columns.push(column);
                
    },
    
    makeReferencedColumn: function(column, configuration) {
        
        var referencedColumn = Ext.create('Ext.grid.column.Column', {
            text: configuration[configuration.table].columns[column].title,
            dataIndex: configuration.name + '.' + column,
            editor: {
                xtype: 'combobox',
                name: configuration.name + '.' + column,
                store: configuration.table,
                displayField: column,
                valueField: column,
                matchFieldWidth: false,
                multiSelect: false,
                editable: false,
                listeners: {
                    select: function(field, record) {
                        var form = field.up('grid').getPlugin('editing').editor.form;
                        form.findField(configuration.name).setValue(record.get('id'));
                        Ext.Array.each(configuration.columns, function(column) {
                            form.findField(configuration.name + '.' + column).setValue(record.get(column))
                        });
                    }
                }
            }
        });
        
        return referencedColumn;
        
    }
    
});