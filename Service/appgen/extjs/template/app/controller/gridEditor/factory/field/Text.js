Ext.define('System.controller.gridEditor.factory.field.Text', {

    make: function(fields, configuration) {
        
        var field = Ext.create('System.model.gridEditor.field.Text', {
            name: configuration.name
        });
        
        fields.push(field);
                
    }
    
});