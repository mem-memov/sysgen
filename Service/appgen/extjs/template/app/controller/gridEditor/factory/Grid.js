Ext.define('System.controller.gridEditor.factory.Grid', {
    
    requires: [
        'System.view.gridEditor.grid.View'
    ],
    
    config: {
        fieldFactory: null,
        columnFactory: null,
    },
    
    constructor: function(config) {
        this.initConfig(config);
    },
    
    make: function(configuration) {
        
        //this.makeDisplayView(configuration);
        
        var viewClass = 'System.view.gridEditor.' + configuration.name + '.View';
        
        Ext.define(viewClass, {
            extend: 'System.view.gridEditor.grid.View',
            controller: this.makeViewController(configuration),
            title: configuration.title,
            store: this.makeStore(configuration),
            columns: this.getColumnFactory().make(configuration)
        });

        var component = Ext.create(viewClass);

        return component;
        
    },
    
    makeViewController: function(configuration) {

        var viewControllerClass = 'System.view.gridEditor.' + configuration.name + '.Controller';

        var routes = {};
        routes[configuration.name] = 'onRoute';
        routes[configuration.name + '/'] = 'onRoute';
        routes[configuration.name + '/:field/:value'] = 'onRoute';
        
        Ext.define(viewControllerClass, {
            extend: 'System.view.gridEditor.grid.Controller',
            alias: 'controller.' + configuration.name,
            name: configuration.name,
            masterTable: configuration.masterTable ? configuration.masterTable : false,
            aspectMasterTables: configuration.aspectMasterTables ? configuration.aspectMasterTables : [],
            routes: routes
        });
        
        return configuration.name;
        
    },
    
    makeStore: function(configuration) {
        
        var storeId = configuration.name;
        var store = 'System.store.gridEditor.' + Ext.String.capitalize(configuration.name);

        Ext.define(store, {
            extend: 'System.store.gridEditor.Store',
            storeId: storeId,
            model: this.makeModel(configuration)
        });
        
        Ext.create(store);

        return storeId;
        
    },
    
    makeModel: function(configuration) {

        var model = 'System.model.gridEditor.' + Ext.String.capitalize(configuration.name);

        Ext.define(model, {
            extend: 'System.model.gridEditor.Model',
            fields: this.getFieldFactory().make(configuration),
            proxy: {
                type: 'rest',
                url: configuration.api + configuration.name + '/',
                reader: {
                    type: 'json',
                    rootProperty: 'data',
                    totalProperty: 'totalCount',
                    messageProperty : 'error',
                    implicitIncludes: true
                }
            }
        });

        return model;
        
    },
    
    makeDisplayView: function(configuration) {
        
        var viewClass = 'System.view.gridEditor.' + configuration.name + '.display.View';
        var alias = configuration.name + 'Display';
        
        Ext.define(viewClass, {
            extend: 'System.view.gridEditor.grid.display.View',
            alias: 'widget' + alias,
            controller: this.makeDisplayController(configuration),
            title: configuration.title,
            items: this.getFieldFactory().make(configuration)
        });

        var component = Ext.create(viewClass);

        return component;
        
    },
    
    makeDisplayController: function(configuration) {
        
        var viewControllerClass = 'System.view.gridEditor.' + configuration.name + '.display.Controller';
        var alias = configuration.name + 'Display';
        
        Ext.define(viewControllerClass, {
            extend: 'System.view.gridEditor.grid.display.Controller',
            alias: 'controller.' + alias
        });
        
        return alias;
        
    }

    
});