Ext.define('System.controller.gridEditor.factory.column.Aspect', {
    
    make: function(columns, configuration, table) {
        
        var data = [];
        
        Ext.Array.each(configuration.tables, function(aspectTable) {
            var aspect = {
                table: aspectTable,
                title: configuration.aspectDetailTables[aspectTable].title
            };
            data.push(aspect);
        });

        var aspectStore = Ext.create('Ext.data.Store', {
            fields: [
                { name: 'table', type: 'string'},
                { name: 'title', type: 'string'}
            ],
            data : data
        });
        
        var column = Ext.create('Ext.grid.column.Column', {
            text: configuration.title,
            dataIndex: configuration.name,
            flex: 1,
            filter: {
                type: 'string'
            },
            renderer: function(value, metaData, record) {
                var aspect = aspectStore.findRecord('table', value);
                if (aspect) {
                    var href = '#' + value + '/' + table + '_' + configuration.name + '/' + record.get('id'); // dot would break the router
                    return '<a href="'+href+'"><img src="resources/icons/table.png" style="margin-right: 5px;"/></a>' + aspect.get('title');
                } else {
                    return '';
                }
            },
            editor: {
                xtype: 'combobox',
                store: aspectStore,
                displayField: 'title',
                valueField: 'table',
                matchFieldWidth: false,
                multiSelect: false,
                editable: false
            }
        });
        
        columns.push(column);
                
    }
    
});