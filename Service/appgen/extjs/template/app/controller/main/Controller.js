Ext.define('System.controller.main.Controller', {
    extend: 'Ext.app.Controller',
    
    requires: [
        'System.view.main.View'
    ],
    
    config: {
        listen: {
            controller: {
                '*': {
                   tableMade: 'onTableMade'
                }
            }
        },
        refs: [{
            ref: 'main',
            selector: 'main'
        }]
    },
    
    tableCount: 0,
    widget: null,    

    onLaunch: function() {
        Ext.Ajax.request({
            url: 'configuration.js',
            success: function(response) {
                var configuration = Ext.decode(response.responseText);
                this.widget = configuration.widget;
                this.makeTables(configuration);
            },
            scope: this
        });
        
    },

    makeTables: function(configuration) {
        
        this.tableCount = Ext.Object.getSize(configuration.tables);
        
        Ext.Object.each(configuration.tables, function(name, table) {
            table.name = name;
            table.api = configuration.api;
            this.fireEvent('makeTable', table);
        }, this);
        
    },
    
    onTableMade: function(component) {

        this.getMain().down('tabpanel').add(component);
        this.tableCount--;
        
        if (this.tableCount == 0) {
            this.onAllTablesLoaded();
        }
        
    },
    
    onAllTablesLoaded: function() {
      
        var token = Ext.util.History.getToken();
        this.redirectTo(token, token, true);
        var panel = Ext.create('System.view.Start');
        var container = this.getMain().down('[reference="startContainer"]');
        container.add(panel);

    }
    
});