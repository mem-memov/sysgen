Ext.define('System.store.gridEditor.Store', {
    extend: 'Ext.data.Store',
    requires: [
        'System.model.gridEditor.Model'
    ],
    pageSize: 50,
    remoteFilter: true,
    remoteSort: true,
    autoSync: true,
    sorters: 'orderNumber',
    autoLoad: false
});