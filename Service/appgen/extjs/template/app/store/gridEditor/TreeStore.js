Ext.define('System.store.gridEditor.TreeStore', {
    extend: 'Ext.data.TreeStore',
    requires: [
        'System.model.gridEditor.TreeModel'
    ],
    remoteFilter: true,
    remoteSort: true,
    autoSync: true,
    sorters: 'orderNumber',
    autoLoad: false,
    clearOnLoad: true,
    defaultRootProperty: 'children', // child nodes are under this key. This is under the reader rootProperty
    defaultRootId: 'root', // send {root: true} from the server to mark the root node
    //parnetIdProperty: 'parentId',
    rootVisible: false,
    root: {
        id: 0,
        expanded: true // autoload enabled
    }/*,
    listeners: {
        load: function(store, records) {
            console.log(records);
        }
    }*/
});