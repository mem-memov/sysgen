Ext.define('System.model.gridEditor.Base', {
    extend: 'Ext.data.Model',
    requires: [
        'System.model.gridEditor.field.Boolean',
        'System.model.gridEditor.field.Integer',
        'System.model.gridEditor.field.Reference',
        'System.model.gridEditor.field.String',
        'System.model.gridEditor.field.Text',
        'System.model.gridEditor.field.Time'
    ],
    idProperty: 'id',
    schema: {
        namespace: 'System.model.gridEditor'
    }
});