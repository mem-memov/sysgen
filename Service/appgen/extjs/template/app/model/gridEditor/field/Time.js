Ext.define('System.model.gridEditor.field.Time', {
    extend: 'Ext.data.field.Date',
    dateFormat: 'Y-m-d H:i:s'
});