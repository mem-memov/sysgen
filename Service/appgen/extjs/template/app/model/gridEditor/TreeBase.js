Ext.define('System.model.gridEditor.TreeBase', { // see Ext.data.TreeModel source code
    extend: 'System.model.gridEditor.Base',
    requires: [
        'Ext.data.NodeInterface'
    ],

    mixins: [
        'Ext.mixin.Queryable'
    ],

    getRefItems: function() {
        return this.childNodes;
    },

    getRefOwner: function() {
        return this.parentNode;
    },
},
function () {
    Ext.data.NodeInterface.decorate(this);
});