Ext.define('System.model.gridEditor.TreeModel', { 
    extend: 'System.model.gridEditor.TreeBase',
    proxy: {
        type: 'rest',
        url: 'is/to/be/set',
        reader: {
            type: 'json',
            rootProperty: 'children', 
            messageProperty : 'error',
            implicitIncludes: true,
            useSimpleAccessors: false, // look at the rootProperty having a dot
            transform: function(data) {
                if (data.data) { // deep fetching
                    data.data.success = data.success; // extjs trees like only this structure
                    return data.data;
                } else { // shallow CRUD operation
                    return data;
                }
            }
        },
        writer: {
            writeRecordId: true // negative id values needed when copying
        }
    }
});
