Ext.define('System.view.gridEditor.Controller', {
    extend: 'Ext.app.ViewController',
    requires: [
        'Ext.window.MessageBox'
    ],

    onDeleteRequested: function(record) {
        
        var view = this.getView();
        var cardContainer = view.down('#cardContainer');
        var companyForm = view.down('companyForm');
        var companyDisplay = view.down('companyDisplay');
        
        if (record.phantom) {
            cardContainer.hide();
            companyForm.reset(true);
            companyDisplay.reset(true);
            return;
        }

        Ext.Msg.show({
            title:'Удаление',
            message: 'Действительно удалить запись?',
            buttons: Ext.Msg.YESNO,
            buttonText: {
                yes: 'Да',
                no: 'Нет'
            },
            icon: Ext.Msg.QUESTION,
            fn: function(btn) {
                if (btn === 'yes') {
                    //var store = Ext.StoreManager.lookup('companyStore');
                    record.erase();
                }
            }
        });
        
    },
    
    onCreateRequested: function() {
        
        var view = this.getView();
        var cardContainer = view.down('#cardContainer');
        var companyNew = view.down('companyNew');
        var companyGrid = view.down('companyGrid');
        
        companyGrid.getSelectionModel().deselectAll();
        var record = Ext.create('TechDoc.model.dictionary.CompanyModel');
        
        companyNew.loadRecord(record);
        companyNew.reset();

        cardContainer.setActiveItem(companyNew);
        cardContainer.show();
        
    },
    
    onNewSubmitted: function(record) {
        
        record.save({
            success: function() {
                var view = this.getView();
                var companyGrid = view.down('companyGrid');

                var store = Ext.StoreManager.lookup('companyStore');
                store.add(record);

                companyGrid.getSelectionModel().select(record);

            },
            scope: this
        });

    },
    
    onNewCanseled: function() {
        
        var view = this.getView();
        var cardContainer = view.down('#cardContainer');
        var companyNew = view.down('companyNew');
        
        var record = companyNew.getRecord();
        record.erase();
        companyNew.reset(true);
        
        cardContainer.hide();
        
    },
    
    onChangeRequested: function() {
        
        var view = this.getView();
        var companyForm = view.down('companyForm');
        var cardContainer = view.down('#cardContainer');
        
        cardContainer.setActiveItem(companyForm);
        
    },
    
    onChangeCanceled: function() {
        
        var view = this.getView();
        var companyForm = view.down('companyForm');
        var companyDisplay = view.down('companyDisplay');
        var cardContainer = view.down('#cardContainer');

        cardContainer.setActiveItem(companyDisplay);

        var record = companyForm.getRecord();
        
        if (record.phantom) {
            cardContainer.hide();
        }
        
        companyForm.reset(true);
        companyDisplay.reset(true);

    },
    
    onChangeSubmitted: function(record) {
        
        var view = this.getView();
        var companyDisplay = view.down('companyDisplay');
        var cardContainer = view.down('#cardContainer');
        var companyForm = view.down('companyForm');

        record.save({
            success: function() {
                companyForm.reset();
                companyDisplay.loadRecord(record);
                cardContainer.setActiveItem(companyDisplay);
            }
        });

    },
    
    onItemSelected: function(selectedRecord) {

        var view = this.getView();
        var cardContainer = view.down('#cardContainer');
        var companyDisplay = view.down('companyDisplay');
        var companyForm = view.down('companyForm');
        
        companyDisplay.reset(true);
        companyDisplay.loadRecord(selectedRecord);
        companyForm.reset(true);
        companyForm.loadRecord(selectedRecord);
        cardContainer.setActiveItem(companyDisplay);
        
    },
    
    onItemRequested: function(requestedRecord) {
        
        var view = this.getView();
        var cardContainer = view.down('#cardContainer');
        var companyDisplay = view.down('companyDisplay');
        var companyForm = view.down('companyForm');
        
        companyDisplay.reset(true);
        companyDisplay.loadRecord(requestedRecord);
        companyForm.reset(true);
        companyForm.loadRecord(requestedRecord);
        cardContainer.setActiveItem(companyDisplay);
        cardContainer.show();
        
    },
    
    onItemsDeselected: function() {
        
        var view = this.getView();
        var cardContainer = view.down('#cardContainer');
        
        cardContainer.hide();
        
    },
    
    onCardContainerShow: function() {
        
        var view = this.getView();
        var splitter = view.down('splitter');
        var companyGrid = view.down('companyGrid');
        
        companyGrid.getController().hideColumns(['okpo']);
        splitter.show();
        
    },
    
    onCardContainerHide: function() {
        
        var view = this.getView();
        var splitter = view.down('splitter');
        var companyGrid = view.down('companyGrid');
        
        companyGrid.getController().showColumns(['okpo']);
        splitter.hide();
        
    }
});