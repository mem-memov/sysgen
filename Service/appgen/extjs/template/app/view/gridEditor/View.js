Ext.define('System.view.gridEditor.View', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.gridTreeEditor',
    requires: [
        'System.view.gridEditor.tree.Controller',
        'System.store.gridEditor.TreeStore'
    ],
    controller: 'company',
    title: 'Организации-разработчики',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'companyGrid',
            flex: 1,
            listeners: {
                itemRequested: 'onItemRequested',
                itemSelected: 'onItemSelected',
                itemsDeselected: 'onItemsDeselected',
                createRequested: 'onCreateRequested'
            }
        }, 
        {
            xtype: 'splitter',
            hidden: true
        },
        {
            xtype: 'panel',
            title: 'Организация-разработчик',
            itemId: 'cardContainer',
            flex: 1,
            hidden: true,
            layout: 'card',
            closable: true,
            closeAction: 'hide',
            items: [
                {
                    xtype: 'companyDisplay',
                    listeners: {
                        changeRequested: 'onChangeRequested'
                    }
                },
                {
                    xtype: 'companyNew',
                    listeners: {
                        newSubmitted: 'onNewSubmitted',
                        newCanceled: 'onNewCanseled'
                    }
                },
                {
                    xtype: 'companyForm',
                    listeners: {
                        changeCanceled: 'onChangeCanceled',
                        changeSubmitted: 'onChangeSubmitted',
                        deleteRequested: 'onDeleteRequested'
                    }
                }
            ],
            listeners: {
                show: 'onCardContainerShow',
                hide: 'onCardContainerHide'
            }
        }
    ]
    
});