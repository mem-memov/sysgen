Ext.define('System.view.gridEditor.grid.new.View', {
    extend: 'Ext.form.Panel',
    alias: 'widget.gridEditorGridNew',
    requires: [
        'System.view.gridEditor.grid.new.Controller'
    ],
    controller: 'gridEditorGridNew',

    bodyPadding: 5,
    layout: 'anchor',
    trackResetOnLoad: true,
    
    defaults: {
        labelAlign: 'top',
        anchor: '100%',
        labelWidth: 200
    },
    
    items: [
        {
            xtype: 'textfield',
            fieldLabel: 'Код по кодификатору',
            name: 'developerCode'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Код по ОКПО',
            name: 'okpo'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Название организации',
            name: 'company'
        }
    ],
    
    buttons: [
        '->',
        {
            text: 'Отменить',
            listeners: {
                click: 'onCancelButtonClick'
            }
        },
        {
            text: 'Создать',
            listeners: {
                click: 'onSubmitButtonClick'
            }
        }
    ],
    
    listeners: {
        dirtychange: function(form, isDirty) {
            if (isDirty) {
                form.owner.setBodyStyle('background-color', 'pink');
            } else {
                form.owner.setBodyStyle('background-color', 'white');
            }
        }
    }
});