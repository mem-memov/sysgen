Ext.define('System.view.gridEditor.grid.display.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.gridEditorGridDisplay',
    
    onChangeButtonClick: function() {
        var view = this.getView();
        var record = view.getRecord();
        view.fireEvent('changeRequested', record);
    }
    
});