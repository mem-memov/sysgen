Ext.define('System.view.gridEditor.grid.form.View', {
    extend: 'Ext.form.Panel',
    alias: 'widget.gridEditorGridForm',
    requires: [
        'System.view.gridEditor.grid.form.Controller'
    ],
    controller: 'gridEditorGridForm',

    bodyPadding: 5,
    layout: 'anchor',
    trackResetOnLoad: true,
    
    defaults: {
        labelAlign: 'top',
        anchor: '100%',
        labelWidth: 200
    },
    
    items: [
        {
            xtype: 'textfield',
            fieldLabel: 'Код по кодификатору',
            name: 'developerCode'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Код по ОКПО',
            name: 'okpo'
        },
        {
            xtype: 'textfield',
            fieldLabel: 'Название организации',
            name: 'company'
        }
    ],
    
    buttons: [
        {
            itemId: 'deleteButton',
            text: 'Удалить',
            listeners: {
                click: 'onDeleteButtonClick'
            }
        },
        '->',
        {
            text: 'Отменить',
            listeners: {
                click: 'onCancelButtonClick'
            }
        },
        {
            text: 'Сохранить',
            listeners: {
                click: 'onSubmitButtonClick'
            }
        }
    ],
    
    listeners: {
        dirtychange: function(form, isDirty) {
            if (isDirty) {
                form.owner.setBodyStyle('background-color', 'pink');
            } else {
                form.owner.setBodyStyle('background-color', 'white');
            }
        }
    }
});