Ext.define('System.view.gridEditor.grid.new.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.gridEditorGridNew',
    
    onCancelButtonClick: function() {
        this.getView().fireEvent('newCanceled');
    },
    
    onSubmitButtonClick: function() {
        
        var view = this.getView();
        view.updateRecord();
        var record = view.getRecord();
        view.fireEvent('newSubmitted', record);

    }
    
});