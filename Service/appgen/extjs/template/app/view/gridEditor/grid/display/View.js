Ext.define('System.view.gridEditor.grid.display.View', {
    extend: 'Ext.form.Panel',
    alias: 'widget.gridEditorGridDisplay',
    requires: [
        'System.view.gridEditor.grid.display.Controller'
    ],
    controller: 'gridEditorGridDisplay',
    
    bodyPadding: 5,
    layout: 'anchor',
    trackResetOnLoad: true,
    
    defaults: {
        labelAlign: 'top',
        anchor: '100%',
        labelWidth: 200
    },
    
    items: [
        {
            xtype: 'displayfield',
            fieldLabel: '<b>Код по кодификатору</b>',
            name: 'developerCode'
        },
        {
            xtype: 'displayfield',
            fieldLabel: '<b>Код по ОКПО</b>',
            name: 'okpo'
        },
        {
            xtype: 'displayfield',
            fieldLabel: '<b>Название организации</b>',
            name: 'company'
        }
    ],
    
    buttons: [
        {
            text: 'Изменить',
            listeners: {
                click: 'onChangeButtonClick'
            }
        }
    ]
});