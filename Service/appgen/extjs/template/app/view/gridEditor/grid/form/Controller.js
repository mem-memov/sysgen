Ext.define('System.view.gridEditor.grid.form.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.gridEditorGridForm',
    
    onCancelButtonClick: function() {
        this.getView().fireEvent('changeCanceled');
    },
    
    onSubmitButtonClick: function() {
        
        var view = this.getView();
        view.updateRecord();
        var record = view.getRecord();
        view.fireEvent('changeSubmitted', record);

    },
    
    onDeleteButtonClick: function() {
        var view = this.getView();
        var record = view.getRecord();
        view.fireEvent('deleteRequested', record);
    }
    
});