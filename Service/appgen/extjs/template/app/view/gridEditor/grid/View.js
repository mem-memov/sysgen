Ext.define('System.view.gridEditor.grid.View', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.gridEditorGrid',
    requires: [
        'System.view.gridEditor.grid.Controller',
        'System.store.gridEditor.Store'
    ],

    rootVisible: false,
    loadMask: true,
    selModel: {
        type: 'rowmodel',
        mode: 'MULTI'
    },
    features: [
        {
            ftype:'grouping'
        }
    ],
    plugins: [
        {
            ptype: 'gridfilters'
        },
        {
            pluginId: 'editing',
            ptype: 'rowediting',
            clicksToEdit: 2
        }
    ],
    viewConfig: {
        plugins: {
            ptype: 'gridviewdragdrop',
            dragText: 'Drag and drop to reorganize'
        },
        listeners: {
            drop: 'onGridRowDrop'
        }
    },
    allowDeselect: true,

    bbar: [
        {
            reference: 'pageField',
            xtype: 'numberfield',
            width: 50,
            enableKeyEvents: true,
            minValue: 1,
            value: 1,
            listeners: {
                change: 'onPageFieldChange'
            }
        },
        '->',
        {
            reference: 'deleteButton',
            text: 'Удалить',
            xtype: 'button',
            disabled: true,
            listeners: {
                click: 'onDeleteButtonClick'
            }
        },
        {
            reference: 'copyButton',
            text: 'Копировать',
            xtype: 'button',
            disabled: true,
            listeners: {
                click: 'onCopyButtonClick'
            }
        },
        {
            reference: 'createButton',
            text: 'Добавить',
            xtype: 'button',
            listeners: {
                click: 'onCreateButtonClick'
            }
        }
    ],

    listeners: {
        selectionchange: 'onSelectionChange',
        afterrender: 'afterGridRender',
        filterchange: 'onGridFilterChange',
        edit: 'onGridEdit',
        activate: 'onGridActivate'
    }
    

});