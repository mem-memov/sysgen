Ext.define('System.view.gridEditor.tree.Controller', {
    extend: 'Ext.app.ViewController',

    config: {
        name: null,
        masterTable: null,
        masterId: 0,
        aspectMasterTables: [],
        aspectField: null,
        aspectId: 0
    },

    onRoute: function(field, value) {

        var view = this.getView();
        var store = view.getStore();

        view.fireEvent('needBeShown', view);
        
        //this.masterId = 0;
        //this.aspectId = 0;
        if (typeof field !== 'undefined' && typeof value !== 'undefined') {
        
            if (this.getMasterTable() && field == this.getMasterTable()) {
                this.setMasterId(value);
            }
            
            Ext.Array.each(this.getAspectMasterTables(), function(aspectMasterTable) {
                var aspectField = aspectMasterTable.table + '_' + aspectMasterTable.column; // dot would break the router
                if (aspectField == field) {
                    field = aspectMasterTable.table + '.' + aspectMasterTable.column;
                    this.setAspectField(field);
                    this.setAspectId(value);
                }
            }, this);

            store.clearFilter(true);
            store.addFilter([{
                property: field,
                operator: 'eq',
                value: value
            }], true);
            
        }
        
        store.setRoot({
            id: 0,
            expanded: true
        });

    },

    onAssociationButtonClick: function(record, detailTable) {
        this.redirectTo(detailTable +  '/' + this.getName() + '/' + record.get('id'));
    },
    
    onGridActivate: function() {

        this.redirectTo(this.getName());
        
    },
    
    onSelectionChange: function(grid, selectedRecords) {
        
        var view = this.getView();
        var deleteButton = this.lookupReference('deleteButton');
        var copyButton = this.lookupReference('copyButton');
        
        var editor = view.getPlugin('editing');
        
        
        if (selectedRecords.length === 0) {
            deleteButton.disable();
            copyButton.disable();
        } else {
            if (selectedRecords.length === 1) {
                if (editor.editing) {
                    editor.startEdit(selectedRecords[0], 0);
                }
            } else {
                editor.cancelEdit();
            }
            deleteButton.enable();
            copyButton.enable();
        }
        
    },
    
    onCopyButtonClick: function() {
    
        var view = this.getView();
        var store = view.getStore();
        var selectionModel = view.getSelectionModel();
        
        if (selectionModel.hasSelection()) {
            var selectedRecords = selectionModel.getSelection();

            Ext.Array.each(selectedRecords, function(selectedRecord) {
                var newId = -selectedRecord.get('id');
                var record = selectedRecord.copy(newId);
                record.phantom = true;
                selectedRecord.parentNode.appendChild(record);
            });

        }
    
    },

    onCreateButtonClick: function() {
        
        var view = this.getView();
        var store = view.getStore();
        var selectionModel = view.getSelectionModel();
        
        var record = Ext.create(store.getModel());

        if (this.getMasterTable()) {
            record.set(this.getMasterTable(), this.getMasterId());
        }
        if (this.getAspectId()) {
            record.set(this.getAspectField(), this.getAspectId());
        }
       
        if (selectionModel.hasSelection()) {
        
            var selectedRecords = selectionModel.getSelection();
            var selectedRecord = selectedRecords[0];
            var selectedRecordOrderNumber = selectedRecord.get('orderNumber');

            record.set('orderNumber', selectedRecordOrderNumber);
            var insertPosition = store.indexOf(selectedRecord) + 1;
            store.insert(insertPosition, [record]);
            store.mon(store, 'write', function() {
                var editor = view.getPlugin('editing');
                var ok = editor.startEdit(record, 0);
            }, this, { single: true, delay: 300 }); // TODO: remove delay
            
        }

    },
    
    onDeleteButtonClick: function() {
    
        var view = this.getView();
        var store = view.getStore();
        var selectionModel = view.getSelectionModel();
        var selectedRecords = selectionModel.getSelection();
        var count = selectedRecords.length;
        
        if (count == 0) {
            return;
        }
        
        Ext.Msg.show({
            title:'Delete Rows?',
            message: 'You are going to delete rows (' + count + '). Please, confirm.',
            buttons: Ext.Msg.YESNO,
            icon: Ext.Msg.WARNING,
            fn: function(btn) {
                if (btn === 'yes') {
                    store.beginUpdate();
                    Ext.Array.each(selectedRecords, function(record) {
                        record.drop();
                    });
                    //store.reload();
                    store.endUpdate();
                }
            }
        });
        
    
    },
    
    onReadButtonClick: function(record) {
        
        var view = this.getView();
        view.fireEvent('itemRequested', record);
        this.fireEvent('dictionaryItemSelected', record);
        
    },

    afterGridRender: function() {
    

        
    },
    
    onGridFilterChange: function() {
    

    
    },
    
    onGridEdit: function(editor, context, eOpts ) {

        context.record.commit();
    
    },
    
    onGridRowDrop: function(node, data, overModel, dropPosition, eOpts) {
    
        var view = this.getView();
        var store = view.getStore();
        var orderNumber = overModel.get('orderNumber');
        
        var callbacks = []; // TODO: problems with multiselect
        Ext.Array.each(data.records, function(record, index) {
            var callback = (function(record, orderNumber,  dropPosition, index, store, callbacks) {
            
                var record = record;
                var orderNumber = orderNumber;
                var dropPosition = dropPosition;
                var index = index;
                //var store = store;
                //var callbacks = callbacks;
                
                return function() {
                
                    
                    var newOrderNumber;
                    switch (dropPosition) {
                        case 'after':
                            newOrderNumber = orderNumber;
                            break;
                        case 'before':
                            newOrderNumber = -orderNumber;
                            break;
                    }

                    record.set('orderNumber', newOrderNumber);
                    
                    store.mon(store, 'write', function() {
                        if (index < callbacks.length-1) {
                            callbacks[index+1]();
                        }
                    }, this, { single: true });
                    
                }
            })(record, orderNumber,  dropPosition, index, store, callbacks);
            callbacks.push(callback);
        }, this);
        
        callbacks[0]();

    }
    
});