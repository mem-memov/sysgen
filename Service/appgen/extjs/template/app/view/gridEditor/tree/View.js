Ext.define('System.view.gridEditor.tree.View', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.gridTreeEditor',
    requires: [
        'System.view.gridEditor.tree.Controller',
        'System.store.gridEditor.TreeStore'
    ],
    //rootVisible: false,
    loadMask: true,
    selModel: {
        type: 'rowmodel',
        mode: 'MULTI'
    },
    features: [
        {
            ftype:'grouping'
        }
    ],
    plugins: [
        {
            ptype: 'gridfilters'
        },
        {
            pluginId: 'editing',
            ptype: 'rowediting',
            clicksToEdit: 2
        }
    ],
    viewConfig: {
        plugins: {
            ptype: 'gridviewdragdrop',
            dragText: 'Drag and drop to reorganize'
        },
        listeners: {
            drop: 'onGridRowDrop'
        }
    },
    allowDeselect: true,

    bbar: [
        '->',
        {
            reference: 'deleteButton',
            text: 'Удалить',
            xtype: 'button',
            disabled: true,
            listeners: {
                click: 'onDeleteButtonClick'
            }
        },
        {
            reference: 'copyButton',
            text: 'Копировать',
            xtype: 'button',
            disabled: true,
            listeners: {
                click: 'onCopyButtonClick'
            }
        },
        {
            reference: 'createButton',
            text: 'Добавить',
            xtype: 'button',
            listeners: {
                click: 'onCreateButtonClick'
            }
        }
    ],

    listeners: {
        selectionchange: 'onSelectionChange',
        afterrender: 'afterGridRender',
        filterchange: 'onGridFilterChange',
        edit: 'onGridEdit',
        activate: 'onGridActivate'
    }
    

});