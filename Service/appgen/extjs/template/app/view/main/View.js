Ext.define('System.view.main.View', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.main',
    requires: [
        'System.view.main.Controller',
        'System.view.Start'
    ],
    controller: 'main',

    layout: {
        type: 'fit'
    },

    items: [
        {
            reference: 'tabpanel',
            xtype: 'tabpanel',
            defaults: {
                listeners: {
                    needBeShown: 'onItemNeedBeShown'
                }
            },
            items: [
                {
                    title: 'Start',
                    reference: 'startContainer',
                    xtype: 'container',
                    layout: 'fit',
                    listeners: {
                        activate: 'onStartContainerActivate'
                    }
                }
            ]
        }
    ]


});