Ext.define('System.view.main.Controller', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    
    routes: {
        'home': 'onRoute'
    },
    
    onRoute: function() {

        var tabpanel = this.lookupReference('tabpanel');
        var startContainer = this.lookupReference('startContainer');
        tabpanel.setActiveTab(startContainer);
        this.redirectTo('home');
    },

    onItemNeedBeShown: function(item) {
        var tabpanel = this.lookupReference('tabpanel');
        var activeItem = tabpanel.getActiveTab();
        if (activeItem !== item) {
            tabpanel.setActiveTab(item);
        }
    },
    
    onStartContainerActivate: function() {
        this.redirectTo('home');
    }
    
});