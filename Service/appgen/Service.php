<?php
namespace Service\appgen;
class Service {
    
    private $configuration;
    
    public function __construct() {
        
        $app = $_GET['app'];
        $path = __DIR__.'/task/'.$app.'/configuration.json';
        $this->configuration = json_decode(file_get_contents($path), true, 100, JSON_UNESCAPED_UNICODE);
        
    }
    
    public function generate() {
        
        foreach ($this->configuration as $applicationKey => $applicationConfiguration) {
            $applicationConfiguration = array_merge(array(
                'name' => $applicationKey
            ), $applicationConfiguration);
            $renderer = new Renderer();
            $fileSystem = new FileSystem();
            $class = __NAMESPACE__ . '\\' . $applicationConfiguration['type'];
            $application = new $class($applicationConfiguration, $renderer, $fileSystem);
            $application->generate();
        }
        
    }
    
}