<?php
namespace Service\appgen;
class FileSystem implements IFileSystem {
    
    public function makeDirectory($directory) {

        mkdir($directory, 0777, true);
        
    }
    
    public function deleteDirectory($directory) {
        
        if (!file_exists($directory)) {
            return true;
        }

        if (!is_dir($directory)) {
            return unlink($directory);
        }

        foreach (scandir($directory) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($directory . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($directory);
        
    }
    
    public function copyDirectory($source, $desination) {
        
        if (!file_exists($source) || !is_dir($source)) {
            throw new \Exception('Source directory missing '.$source);
        }
        
        if (!file_exists($desination)) {
            mkdir($desination, 0777, true);
        }

        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST
        );
        
        foreach ($iterator as $item) {
            if ($item->isDir()) {
                $directory = $desination . DIRECTORY_SEPARATOR . $iterator->getSubPathName();
                if (!file_exists($directory)) {
                    mkdir($directory);
                }
            } else {
                $file = $desination . DIRECTORY_SEPARATOR . $iterator->getSubPathName();
                copy($item, $file);
            }
        }
        
    }
    
    public function writeFile($file, $contents) {
        
        file_put_contents($file, $contents);
        
    }
    
    public function readFile($file) {
        
        return file_get_contents($file);
        
    }
    
    public function copyFile($source, $desination) {
        
        copy($source, $desination);
        
    }
    
}