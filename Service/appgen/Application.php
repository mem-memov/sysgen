<?php
namespace Service\appgen;
class Application {
    
    private $configuration;
    private $renderer;
    private $fileSystem;
    
    public function __construct(array $configuration, IRenderer $renderer, IFileSystem $fileSystem) {
        
        $this->configuration = $configuration;
        $this->renderer = $renderer;
        $this->fileSystem = $fileSystem;
        
        $this->addTablePrefix();
        $this->addDetailTables();
        $this->setTrees();
        $this->addColumns();
        $this->setNames(); // follow columns
        $this->addColumnReference();
        $this->addAspectMasters();

    }
    
    public function generate() {
        
        $applicationDirectory = __DIR__.'/'.$this->configuration['directory'].'/'.$this->configuration['name'];        
        $this->fileSystem->deleteDirectory($applicationDirectory);
        $this->fileSystem->makeDirectory($applicationDirectory);

        $clientGenerator = new extjs\Generator($this->configuration, $applicationDirectory, $this->renderer, $this->fileSystem);
        $clientGenerator->generate();

        $databaseClass = __NAMESPACE__ . '\database\\' . $this->configuration['database']['type'];
        $database = new $databaseClass($this->configuration);
        $database->generate();
        
        $serverGeneratorClass = __NAMESPACE__.'\server\\'.$this->configuration['webServer']['programmingLanguage'].'\Generator';
        $serverGenerator = new  $serverGeneratorClass($this->configuration, $applicationDirectory, $this->fileSystem);
        $serverGenerator->generate();

    }
    
    private function addDetailTables() {
        
        $associationMap = array(
            'masterDetail' => array(),
            'detailMaster' => array()
        );
        
        foreach ($this->configuration['tables'] as $tableKey => $tableConfiguration) {
            if (array_key_exists('masterTable', $tableConfiguration)) {
                $masterTable = $tableConfiguration['masterTable'];
                if (!array_key_exists($masterTable, $this->configuration['tables'])) {
                    throw new \Exception('Master table is missing in the configuration '.$masterTable);
                }
                $this->configuration['tables'][$masterTable]['detailTables'][] = $tableKey;
            }
        }

    }
    
    public function addTablePrefix() {
        
        if (isset($this->configuration['database']['tablePrefix'])) {
            $tablePrefix = $this->configuration['database']['tablePrefix'];
        } else {
            $tablePrefix = '';
        }
        
        foreach ($this->configuration['tables'] as $tableKey => $tableConfiguration) {
            $this->configuration['tables'][$tableKey]['tablePrefix'] = $tablePrefix;
        }
        
    }
    
    private function addColumnReference() {
        foreach ($this->configuration['tables'] as $tableKey => $table) {
            foreach ($table['columns'] as $columnKey => $column) {
                if ($column['type'] == 'reference') {
                    $this->configuration['tables'][$tableKey]['columns'][$columnKey][$column['table']] = $this->configuration['tables'][$column['table']];
                }
            }
        }
    }
    
    private function addAspectMasters() {
        foreach ($this->configuration['tables'] as $tableKey => $table) {
            foreach ($table['columns'] as $columnKey => $column) {
                if ($column['type'] == 'aspect') {
                    foreach ($column['tables'] as $aspectDetailTable) {
                        if (!array_key_exists($aspectDetailTable, $this->configuration['tables'])) {
                            throw new \Exception('Unspecified aspect table ' . $aspectDetailTable);
                        }
                        $this->configuration['tables'][$aspectDetailTable]['aspectMasterTables'][] = array( 'table' => $tableKey, 'column' => $columnKey);
                        $this->configuration['tables'][$tableKey]['columns'][$columnKey]['aspectDetailTables'][$aspectDetailTable] = $this->configuration['tables'][$aspectDetailTable];
                    }
                }
            }
        }
    }
    
    private function setTrees() {
        
        foreach ($this->configuration['tables'] as $tableKey => $table) {
            
            if (!array_key_exists('tree', $table) || $table['tree'] != true) {
                
                $this->configuration['tables'][$tableKey]['tree'] = false;
                
            }
            
        }
        
    }
    
    private function addColumns() {
        
        foreach ($this->configuration['tables'] as $tableKey => $table) {
            
            $this->configuration['tables'][$tableKey]['columns']['id'] = array('type' => 'id', 'title' => 'id');

            $this->configuration['tables'][$tableKey]['columns']['orderNumber'] = array('type' => 'orderNumber', 'title' => 'orderNumber');
            
            if (array_key_exists('tree', $table) && $table['tree'] == true) {
                
                $this->configuration['tables'][$tableKey]['columns']['parentId'] = array('type' => 'parentId', 'title' => 'parentId');
                
            }

            if (array_key_exists('masterTable', $table)) {
                
                $masterTable = $table['masterTable'];
                
                $this->configuration['tables'][$tableKey]['columns'][$masterTable] = array(
                    'type' => 'masterId', 
                    'masterTable' => $masterTable, 
                    'detailTable' => $tableKey,
                    'title' => $masterTable
                );
                
            }

        }
 
    }
    
    private function setNames() {
        
        foreach ($this->configuration['tables'] as $tableKey => $table) {
            
            $this->configuration['tables'][$tableKey]['name'] = $tableKey;
            
            foreach ($table['columns'] as $columnKey => $column) {
                
                $this->configuration['tables'][$tableKey]['columns'][$columnKey]['name'] = $columnKey;
                
            }
            
        }
        
    }
    
}